package com.dyj.common.enums;

/**
 * 交易接口accessToken类型
 */
public enum TransactionMerchantTokenTypeEnum {
    /**
     * 服务商身份获取token链接
     */
    TP_THIRD_V2_TOKEN,

    /**
     * 非服务商身份获取token链接
     */
    CLIENT_TOKEN;


    TransactionMerchantTokenTypeEnum() {
    }



}
