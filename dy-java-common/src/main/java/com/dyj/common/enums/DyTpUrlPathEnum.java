package com.dyj.common.enums;

/**
 * @author danmo
 * @date 2024-06-24 17:20
 **/
public enum DyTpUrlPathEnum {

    /**
     * 获取第三方小程序接口调用凭据V2
     */
    TP_THIRD_V2_TOKEN("tpThirdV2Token", "/openapi/v2/auth/tp/token/"),

    /**
     *获取授权小程序接口调用凭据V2
     */
    TP_V2_AUTH_TOKEN("tpV2AuthToken", "/api/tpapp/v2/auth/get_auth_token/"),
    ;

    private String key;
    private String value;

    DyTpUrlPathEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static String getValueByKey(String key) {
        for (DyTpUrlPathEnum e : DyTpUrlPathEnum.values()) {
            if (e.getKey().equals(key)) {
                return e.getValue();
            }
        }
        return null;
    }


}
