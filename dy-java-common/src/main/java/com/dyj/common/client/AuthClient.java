package com.dyj.common.client;

import com.dtflys.forest.annotation.*;
import com.dtflys.forest.backend.ContentType;
import com.dyj.common.domain.DyAppletResult;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.*;
import com.dyj.common.domain.vo.*;
import com.dyj.common.interceptor.NoTokenInterceptor;
import com.dyj.common.interceptor.TpThirdV2TokenHeaderInterceptor;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-03 11:03
 **/

@BaseRequest(baseURL = "${domain}",interceptor = NoTokenInterceptor.class)
public interface AuthClient {


    /** 获取accessToken */
    @Post(value = "${oauthAccessToken}")
    DyResult<AccessTokenVo> getAccessToken(@JSONBody AccessTokenQuery query);

    /** 刷新 refresh_token */
    @Post(value = "${oauthRefreshToken}")
    DyResult<RefreshTokenVo> refreshToken(@JSONBody RefreshTokenQuery query);

    /** 获取client_token */
    @Post(value = "${oauthClientToken}")
    DyResult<ClientTokenVo> getClientToken(@JSONBody ClientTokenQuery query);

    /** 刷新 access_token */
    @Post(value = "${accessTokenRefresh}")
    DyResult<RefreshAccessTokenVo> refreshAccessToken(@JSONBody RefreshAccessTokenQuery query);

    /** BusinessToken 生成 */
    @Post(value = "${bizAccessToken}")
    DyAppletResult<BizTokenVo> getBizToken(@JSONBody BizTokenQuery query);

    /**
     * BusinessToken 生成刷新
     */
    @Post(value = "${bizTokenRefresh}")
    DyAppletResult<BizTokenVo> refreshBizToken(@JSONBody BizRefreshTokenQuery query);

    /**
     * 小程序 getAccessToken
     */
    @Post(value = "${ttDomain}${appsV2Token}")
    DySimpleResult<AppsV2TokenVo> appsV2Token(@JSONBody AppsV2TokenQuery query);


    /**
     * 获取第三方小程序接口调用凭据V2
     * @param appid 第三方小程序应用 appid
     * @param secret 第三方小程序应用 appsecret
     * @param ticket 推送的 component_ticket
     * @return TpThirdV2TokenVo
     */
    @Get(value = "${tpThirdV2Token}")
    TpThirdV2TokenVo tpThirdV2Token(@Query("component_appid") String appid, @Query("component_appsecret") String secret, @Query("component_ticket") String ticket);

    /**
     * 获取授权小程序接口调用凭据V2
     * @param query 应用信息
     * @param grantType 获取方式通过授权码方式获取填写固定字符串：
     *                  app_to_tp_authorization_code
     *                  通过刷新令牌方式获取填写固定字符串：
     *                  app_to_tp_refresh_token
     * @param authorizationCode 授权码通过授权码方式获取必填
     * @param authorizerRefreshToken 刷新令牌 authorizer_refresh_token通过刷新令牌方式获取必填
     * @return
     */
    @Get(value = "${tpV2AuthToken}",interceptor = TpThirdV2TokenHeaderInterceptor.class)
    DySimpleResult<TpV2AuthTokenVo> tpV2AuthToken(@Var("query") BaseQuery query, @Query("grant_type") String grantType, @Query("authorization_code") String authorizationCode, @Query("authorizer_refresh_token") String authorizerRefreshToken);

}
