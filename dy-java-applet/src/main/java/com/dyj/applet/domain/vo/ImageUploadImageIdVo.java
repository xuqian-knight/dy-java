package com.dyj.applet.domain.vo;

/**
 * 图片上传图片id
 */
public class ImageUploadImageIdVo {

    /**
     * 图片在文件存储平台的标识
     */
    private String image_id;

    public String getImage_id() {
        return image_id;
    }

    public ImageUploadImageIdVo setImage_id(String image_id) {
        this.image_id = image_id;
        return this;
    }
}
