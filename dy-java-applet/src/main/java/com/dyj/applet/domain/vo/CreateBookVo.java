package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.BookMarkupOrderInfo;

public class CreateBookVo {

    /**
     * 预约单id
     */
    private String book_id;

    /**
     * 加价单信息，详见 MarkupOrderInfo
     */
    private BookMarkupOrderInfo markup_order_info;

    public String getBook_id() {
        return book_id;
    }

    public CreateBookVo setBook_id(String book_id) {
        this.book_id = book_id;
        return this;
    }

    public BookMarkupOrderInfo getMarkup_order_info() {
        return markup_order_info;
    }

    public CreateBookVo setMarkup_order_info(BookMarkupOrderInfo markup_order_info) {
        this.markup_order_info = markup_order_info;
        return this;
    }
}
