package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class LiveWithShortIdQuery extends BaseQuery {

    /**
     * 开始时间，必须是昨天以前的某个时间戳
     */
    private Long start_time;
    /**
     * 结束时间，必须是昨天以前的某个时间戳
     */
    private Long end_time;
    /**
     * 分页
     */
    private Integer page_no;
    /**
     * 分页大小，最大50
     */
    private Integer page_size;
    /**
     * 选择的抖音号列表
     */
    private List<String> aweme_short_id_list;
    /**
     * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     */
    private String host_name;

    public static LiveWithShortIdQueryBuilder builder() {
        return new LiveWithShortIdQueryBuilder();
    }

    public static class LiveWithShortIdQueryBuilder {
        /**
         * 开始时间，必须是昨天以前的某个时间戳
         */
        private Long startTime;
        /**
         * 结束时间，必须是昨天以前的某个时间戳
         */
        private Long endTime;
        /**
         * 分页
         */
        private Integer pageNo;
        /**
         * 分页大小，最大50
         */
        private Integer pageSize;
        /**
         * 选择的抖音号列表
         */
        private List<String> awemeShortIdList;
        /**
         * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
         */
        private String hostNme;

        private Integer tenantId;

        private String clientKey;

        public LiveWithShortIdQueryBuilder startTime(Long startTime) {
            this.startTime = startTime;
            return this;
        }
        public LiveWithShortIdQueryBuilder endTime(Long endTime) {
            this.endTime = endTime;
            return this;
        }
        public LiveWithShortIdQueryBuilder pageNo(Integer pageNo) {
            this.pageNo = pageNo;
            return this;
        }
        public LiveWithShortIdQueryBuilder pageSize(Integer pageSize) {
            this.pageSize = pageSize;
            return this;
        }
        public LiveWithShortIdQueryBuilder awemeShortIdList(List<String> awemeShortIdList) {
            this.awemeShortIdList = awemeShortIdList;
            return this;
        }
        public LiveWithShortIdQueryBuilder hostNme(String hostNme) {
            this.hostNme = hostNme;
            return this;
        }
        public LiveWithShortIdQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public LiveWithShortIdQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public LiveWithShortIdQuery build() {
            LiveWithShortIdQuery liveWithShortIdQuery = new LiveWithShortIdQuery();
            liveWithShortIdQuery.setStart_time(startTime);
            liveWithShortIdQuery.setEnd_time(endTime);
            liveWithShortIdQuery.setPage_no(pageNo);
            liveWithShortIdQuery.setPage_size(pageSize);
            liveWithShortIdQuery.setAweme_short_id_list(awemeShortIdList);
            liveWithShortIdQuery.setHost_name(hostNme);
            liveWithShortIdQuery.setTenantId(tenantId);
            liveWithShortIdQuery.setClientKey(clientKey);
            return liveWithShortIdQuery;
        }
    }


    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public Integer getPage_no() {
        return page_no;
    }

    public void setPage_no(Integer page_no) {
        this.page_no = page_no;
    }

    public Integer getPage_size() {
        return page_size;
    }

    public void setPage_size(Integer page_size) {
        this.page_size = page_size;
    }

    public List<String> getAweme_short_id_list() {
        return aweme_short_id_list;
    }

    public void setAweme_short_id_list(List<String> aweme_short_id_list) {
        this.aweme_short_id_list = aweme_short_id_list;
    }

    public String getHost_name() {
        return host_name;
    }

    public void setHost_name(String host_name) {
        this.host_name = host_name;
    }
}
