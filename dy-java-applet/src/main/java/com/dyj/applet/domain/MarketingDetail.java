package com.dyj.applet.domain;

import java.util.Map;

/**
 * 营销明细
 */
public class MarketingDetail {

    /**
     * 营销 id(用户身份 id，优惠券 id，积分 id 或者活动 id)
     */
    private String id;

    /**
     * 营销类型，
     * 1：用户身份
     * 2：优惠券
     * 3：积分
     * 4：活动
     */
    private Integer type;

    /**
     * 该营销策略优惠金额，单位分
     */
    private Long discount_amount;

    /**
     * 营销名称
     */
    private String title;

    /**
     * 营销备注
     */
    private String note;

    /**
     * 子营销类型 选填
     */
    private String subtype;

    /**
     * 营销分值，某些类型的营销会有，积分和 discount_amount 有一定的关系 选填
     */
    private Long value;

    /**
     * 营销适用维度：
     * 1：订单维度
     * 2：商品维度
     */
    private Integer discount_range;

    /**
     * 优惠券编码
     */
    private String code;

    /**
     * 营销类别
     * 1：商家
     * 2：平台
     */
    private Integer kind;

    /**
     * 创建人类型
     * 1：抖音平台
     * 2：抖音来客—商家
     * 3：小程序商家
     */
    private Integer creator_type;

    /**
     * 扩展字段
     */
    private Map<Object,Object> marketing_extend;

    public String getId() {
        return id;
    }

    public MarketingDetail setId(String id) {
        this.id = id;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public MarketingDetail setType(Integer type) {
        this.type = type;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public MarketingDetail setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public MarketingDetail setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getNote() {
        return note;
    }

    public MarketingDetail setNote(String note) {
        this.note = note;
        return this;
    }

    public String getSubtype() {
        return subtype;
    }

    public MarketingDetail setSubtype(String subtype) {
        this.subtype = subtype;
        return this;
    }

    public Long getValue() {
        return value;
    }

    public MarketingDetail setValue(Long value) {
        this.value = value;
        return this;
    }

    public Integer getDiscount_range() {
        return discount_range;
    }

    public MarketingDetail setDiscount_range(Integer discount_range) {
        this.discount_range = discount_range;
        return this;
    }

    public String getCode() {
        return code;
    }

    public MarketingDetail setCode(String code) {
        this.code = code;
        return this;
    }

    public Integer getKind() {
        return kind;
    }

    public MarketingDetail setKind(Integer kind) {
        this.kind = kind;
        return this;
    }

    public Integer getCreator_type() {
        return creator_type;
    }

    public MarketingDetail setCreator_type(Integer creator_type) {
        this.creator_type = creator_type;
        return this;
    }

    public Map<Object, Object> getMarketing_extend() {
        return marketing_extend;
    }

    public MarketingDetail setMarketing_extend(Map<Object, Object> marketing_extend) {
        this.marketing_extend = marketing_extend;
        return this;
    }
}
