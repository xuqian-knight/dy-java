package com.dyj.applet.domain;

/**
 * 查询券状态信息返回值
 */
public class QueryIndustryItemOrderInfo {

    /**
     * 商品单的核销时间，毫秒级的 Unix 时间戳
     */
    private Long delivery_time;
    /**
     * 交易系统商品单号
     */
    private String item_order_id;
    /**
     * 商品单的状态：
     */
    private Integer item_order_status;
    /**
     * 商品单的有效截止时间，毫秒级的 Unix 时间戳
     */
    private Long valid_end_time;
    /**
     * 商品单的有效开始时间，毫秒级的 Unix 时间戳
     */
    private Long valid_start_time;
    /**
     * 次卡信息，仅次卡类型会返回 选填
     */
    private QueryIndustryItemOrderInfoTimesCardInfo times_card_info;

    public Long getDelivery_time() {
        return delivery_time;
    }

    public QueryIndustryItemOrderInfo setDelivery_time(Long delivery_time) {
        this.delivery_time = delivery_time;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public QueryIndustryItemOrderInfo setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public Integer getItem_order_status() {
        return item_order_status;
    }

    public QueryIndustryItemOrderInfo setItem_order_status(Integer item_order_status) {
        this.item_order_status = item_order_status;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public QueryIndustryItemOrderInfo setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }

    public Long getValid_start_time() {
        return valid_start_time;
    }

    public QueryIndustryItemOrderInfo setValid_start_time(Long valid_start_time) {
        this.valid_start_time = valid_start_time;
        return this;
    }

    public QueryIndustryItemOrderInfoTimesCardInfo getTimes_card_info() {
        return times_card_info;
    }

    public QueryIndustryItemOrderInfo setTimes_card_info(QueryIndustryItemOrderInfoTimesCardInfo times_card_info) {
        this.times_card_info = times_card_info;
        return this;
    }
}
