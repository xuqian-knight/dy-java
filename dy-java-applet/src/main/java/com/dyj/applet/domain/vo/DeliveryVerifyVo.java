package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.DeliveryVerifyVerifyResult;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * 生活服务交易系统->核销->抖音码->验券
 */
public class DeliveryVerifyVo extends BaseVo {

    /**
     * 数组，每个券的验券结果
     */
    private List<DeliveryVerifyVerifyResult> verify_results;

    public List<DeliveryVerifyVerifyResult> getVerify_results() {
        return verify_results;
    }

    public DeliveryVerifyVo setVerify_results(List<DeliveryVerifyVerifyResult> verify_results) {
        this.verify_results = verify_results;
        return this;
    }
}
