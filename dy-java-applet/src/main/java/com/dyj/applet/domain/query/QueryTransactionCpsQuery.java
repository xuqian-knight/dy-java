package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询CPS信息请求值
 * @author ws
 **/
public class QueryTransactionCpsQuery extends BaseQuery {

    /**
     * <p>交易订单号，order_id 与 out_order_no 二选一</p> 选填
     */
    private String order_id;
    /**
     * <p>开发者的单号，order_id 与 out_order_no 二选一</p> 选填
     */
    private String out_order_no;

    public String getOrder_id() {
        return order_id;
    }

    public QueryTransactionCpsQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryTransactionCpsQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public static QueryTransactionCpsQueryBuilder builder() {
        return new QueryTransactionCpsQueryBuilder();
    }

    public static final class QueryTransactionCpsQueryBuilder {
        private String order_id;
        private String out_order_no;
        private Integer tenantId;
        private String clientKey;

        private QueryTransactionCpsQueryBuilder() {
        }

        public QueryTransactionCpsQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public QueryTransactionCpsQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public QueryTransactionCpsQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryTransactionCpsQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryTransactionCpsQuery build() {
            QueryTransactionCpsQuery queryTransactionCpsQuery = new QueryTransactionCpsQuery();
            queryTransactionCpsQuery.setOrder_id(order_id);
            queryTransactionCpsQuery.setOut_order_no(out_order_no);
            queryTransactionCpsQuery.setTenantId(tenantId);
            queryTransactionCpsQuery.setClientKey(clientKey);
            return queryTransactionCpsQuery;
        }
    }
}
