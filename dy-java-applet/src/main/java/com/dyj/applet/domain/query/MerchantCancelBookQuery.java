package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class MerchantCancelBookQuery extends BaseQuery {

    /**
     * 预约单id，len(book_id) <= 64 byte
     */
    private String book_id;

    /**
     * 取消原因
     */
    private String cancel_reason;

    public String getBook_id() {
        return book_id;
    }

    public MerchantCancelBookQuery setBook_id(String book_id) {
        this.book_id = book_id;
        return this;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public MerchantCancelBookQuery setCancel_reason(String cancel_reason) {
        this.cancel_reason = cancel_reason;
        return this;
    }

    public static MerchantCancelBookQueryBuilder builder() {
        return new MerchantCancelBookQueryBuilder();
    }

    public static final class MerchantCancelBookQueryBuilder {
        private String book_id;
        private String cancel_reason;
        private Integer tenantId;
        private String clientKey;

        private MerchantCancelBookQueryBuilder() {
        }

        public MerchantCancelBookQueryBuilder bookId(String bookId) {
            this.book_id = bookId;
            return this;
        }

        public MerchantCancelBookQueryBuilder cancelReason(String cancelReason) {
            this.cancel_reason = cancelReason;
            return this;
        }

        public MerchantCancelBookQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public MerchantCancelBookQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public MerchantCancelBookQuery build() {
            MerchantCancelBookQuery merchantCancelBookQuery = new MerchantCancelBookQuery();
            merchantCancelBookQuery.setBook_id(book_id);
            merchantCancelBookQuery.setCancel_reason(cancel_reason);
            merchantCancelBookQuery.setTenantId(tenantId);
            merchantCancelBookQuery.setClientKey(clientKey);
            return merchantCancelBookQuery;
        }
    }
}
