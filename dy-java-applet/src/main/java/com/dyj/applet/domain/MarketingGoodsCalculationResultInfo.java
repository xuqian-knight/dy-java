package com.dyj.applet.domain;

import java.util.List;

/**
 * 商品算价结果信息
 */
public class MarketingGoodsCalculationResultInfo {

    /**
     * 商品id
     */
    private String goods_id;

    /**
     * 购买数量
     */
    private Long quantity;

    /**
     * 商品总价，单位分
     */
    private Long total_amount;

    /**
     * 该商品总优惠金额，该商品的实付金额 = total_amount - total_discount_amount
     */
    private Long total_discount_amount;

    /**
     * 营销明细，包含该商品订单层和商品层所有优惠信息 选填
     */
    private List<MarketingDetail> marketing_detail_info;

    public String getGoods_id() {
        return goods_id;
    }

    public MarketingGoodsCalculationResultInfo setGoods_id(String goods_id) {
        this.goods_id = goods_id;
        return this;
    }

    public Long getQuantity() {
        return quantity;
    }

    public MarketingGoodsCalculationResultInfo setQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public MarketingGoodsCalculationResultInfo setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public Long getTotal_discount_amount() {
        return total_discount_amount;
    }

    public MarketingGoodsCalculationResultInfo setTotal_discount_amount(Long total_discount_amount) {
        this.total_discount_amount = total_discount_amount;
        return this;
    }

    public List<MarketingDetail> getMarketing_detail_info() {
        return marketing_detail_info;
    }

    public MarketingGoodsCalculationResultInfo setMarketing_detail_info(List<MarketingDetail> marketing_detail_info) {
        this.marketing_detail_info = marketing_detail_info;
        return this;
    }
}
