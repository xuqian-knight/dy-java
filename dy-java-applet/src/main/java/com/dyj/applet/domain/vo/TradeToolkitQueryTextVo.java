package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.TradeToolkitTextInfo;

import java.util.List;

public class TradeToolkitQueryTextVo {

    /**
     * 返回的文案信息列表
     */
    private List<TradeToolkitTextInfo> text_list;

    public List<TradeToolkitTextInfo> getText_list() {
        return text_list;
    }

    public TradeToolkitQueryTextVo setText_list(List<TradeToolkitTextInfo> text_list) {
        this.text_list = text_list;
        return this;
    }
}
