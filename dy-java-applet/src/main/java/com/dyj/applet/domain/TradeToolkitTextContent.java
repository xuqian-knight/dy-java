package com.dyj.applet.domain;

public class TradeToolkitTextContent {

    /**
     * 文案Id：设置文案时传入该值
     */
    private String text_id;

    /**
     * 文案内容
     */
    private String text_content;

    public String getText_id() {
        return text_id;
    }

    public TradeToolkitTextContent setText_id(String text_id) {
        this.text_id = text_id;
        return this;
    }

    public String getText_content() {
        return text_content;
    }

    public TradeToolkitTextContent setText_content(String text_content) {
        this.text_content = text_content;
        return this;
    }
}
