package com.dyj.applet.domain;

import java.util.List;

/**
 * 查询订单信息返回值
 */
public class QueryTransactionOrder {

    /**
     * <p>小程序id</p>
     */
    private String app_id;
    /**
     * <p>渠道支付单号，如：微信的支付单号、支付宝支付单号。</p><p>只有在支付成功时才会有值。</p> 选填
     */
    private String channel_pay_id;
    /**
     * <p>订单优惠金额，单位：分，接入营销时请关注这个字段</p> 选填
     */
    private Long discount_amount;
    /**
     * <p>开发者自定义收款商户号，小程序在抖音开平商户进件时会绑定一个收款账号，当用户交易时，资金会默认收款到此账号，如果某笔交易开发者希望收款到其他账号则需指定希望收款的账号id，此账号必须与默认账号一样属于同一开发者</p> 选填
     */
    private String merchant_uid;
    /**
     * <p>抖音开平侧订单号</p>
     */
    private String order_id;
    /**
     * <p>开发者侧订单号，与 order_id 一一对应</p>
     */
    private String out_order_no;
    /**
     * <p>支付渠道枚举</p><ul><li>1：微信</li><li>2：支付宝</li><li>10：抖音支付</li></ul><p>只有在支付成功时才会有值。</p> 选填
     */
    private Byte pay_channel;
    /**
     * <p>订单支付状态</p><ul><li>PROCESS： 订单处理中 支付处理中</li><li>SUCCESS：成功 支付成功</li><li>FAIL：失败 支付失败 暂无该情况会支付失败</li><li>TIMEOUT：用户超时未支付</li></ul>
     */
    private String pay_status;
    /**
     * <p>支付成功时间，精度：毫秒。</p><p>只有在支付成功时才会有值。</p> 选填
     */
    private Long pay_time;
    /**
     * <p>订单总金额，单位：分，支付金额 = total_amount - discount_amount</p>
     */
    private Long total_amount;
    /**
     * <p>交易下单时间，精度：毫秒</p>
     */
    private Long trade_time;
    /**
     * <p>item单信息</p> 选填
     */
    private List<QueryTransactionOrderItemOrder> item_order_list;

    public String getApp_id() {
        return app_id;
    }

    public QueryTransactionOrder setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getChannel_pay_id() {
        return channel_pay_id;
    }

    public QueryTransactionOrder setChannel_pay_id(String channel_pay_id) {
        this.channel_pay_id = channel_pay_id;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public QueryTransactionOrder setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public QueryTransactionOrder setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public QueryTransactionOrder setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryTransactionOrder setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public Byte getPay_channel() {
        return pay_channel;
    }

    public QueryTransactionOrder setPay_channel(Byte pay_channel) {
        this.pay_channel = pay_channel;
        return this;
    }

    public String getPay_status() {
        return pay_status;
    }

    public QueryTransactionOrder setPay_status(String pay_status) {
        this.pay_status = pay_status;
        return this;
    }

    public Long getPay_time() {
        return pay_time;
    }

    public QueryTransactionOrder setPay_time(Long pay_time) {
        this.pay_time = pay_time;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public QueryTransactionOrder setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public Long getTrade_time() {
        return trade_time;
    }

    public QueryTransactionOrder setTrade_time(Long trade_time) {
        this.trade_time = trade_time;
        return this;
    }

    public List<QueryTransactionOrderItemOrder> getItem_order_list() {
        return item_order_list;
    }

    public QueryTransactionOrder setItem_order_list(List<QueryTransactionOrderItemOrder> item_order_list) {
        this.item_order_list = item_order_list;
        return this;
    }
}
