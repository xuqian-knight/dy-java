package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.LiveRoomData;

import java.util.List;

public class LiveRoomDataVo {

    /**
     * 当前直播间数据
     */
    private List<LiveRoomData> current_live_room;
    /**
     * 历史直播间数据
     */
    private List<LiveRoomData> history_live_room;

    public List<LiveRoomData> getCurrent_live_room() {
        return current_live_room;
    }

    public void setCurrent_live_room(List<LiveRoomData> current_live_room) {
        this.current_live_room = current_live_room;
    }

    public List<LiveRoomData> getHistory_live_room() {
        return history_live_room;
    }

    public void setHistory_live_room(List<LiveRoomData> history_live_room) {
        this.history_live_room = history_live_room;
    }
}
