package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class AnalysisVideoDataQuery extends BaseQuery {

    /**
     * 开始时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     */
    private Long start_time;
    /**
     * 结束时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
     */
    private Long end_time;

    /**
     * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；不传表示查全部端数据
     */
    private String host_name;

    /**
     * 查询的抖音号列表，限制长度在200以内，即一次性最多查询 200个抖音号所对应的自挂载，达人挂载视频所产生的数据
     */
    private List<String> aweme_short_id_list;

    /**
     * 查询的短视频列表，该参数和open_item_id_list参数长度合限制在200以内，即一次性最多查询200个自挂载，达人挂载该小程序的短视频所产生的数据
     */
    private List<String> item_id_list;

    /**
     * 查询的加密短视频列表，该参数和item_id_list参数长度合限制在200以内，即一次性最多查询200个自挂载，达人挂载该小程序的短视频所产生的数据
     */
    private List<String> open_item_id_list;

    /**
     * 查询数据的挂载类型，1-自挂载数据，0-达人挂载+自挂载数据，4-达人挂载+自挂载+通用挂载数据，目前调用此open_api必须传此参数，可以传0或1或4，不要不传，不传可能返回非预期的结果
     */
    private Integer query_bind_type;

    public AnalysisVideoDataQueryBuilder builder() {
        return new AnalysisVideoDataQueryBuilder();
    }

    public static class AnalysisVideoDataQueryBuilder {
        /**
         * 开始时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
         */
        private Long startTime;
        /**
         * 结束时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
         */
        private Long endTime;

        /**
         * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；不传表示查全部端数据
         */
        private String hostName;

        /**
         * 查询的抖音号列表，限制长度在200以内，即一次性最多查询 200个抖音号所对应的自挂载，达人挂载视频所产生的数据
         */
        private List<String> awemeShortIdList;

        /**
         * 查询的短视频列表，该参数和open_item_id_list参数长度合限制在200以内，即一次性最多查询200个自挂载，达人挂载该小程序的短视频所产生的数据
         */
        private List<String> itemIdList;

        /**
         * 查询的加密短视频列表，该参数和item_id_list参数长度合限制在200以内，即一次性最多查询200个自挂载，达人挂载该小程序的短视频所产生的数据
         */
        private List<String> openTtemIdList;

        /**
         * 查询数据的挂载类型，1-自挂载数据，0-达人挂载+自挂载数据，4-达人挂载+自挂载+通用挂载数据，目前调用此open_api必须传此参数，可以传0或1或4，不要不传，不传可能返回非预期的结果
         */
        private Integer queryBindType;

        private Integer tenantId;

        private String clientKey;

        public AnalysisVideoDataQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public AnalysisVideoDataQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public AnalysisVideoDataQueryBuilder startTime(Long startTime) {
            this.startTime = startTime;
            return this;
        }

        public AnalysisVideoDataQueryBuilder endTime(Long endTime) {
            this.endTime = endTime;
            return this;
        }

        public AnalysisVideoDataQueryBuilder hostName(String hostName) {
            this.hostName = hostName;
            return this;
        }

        public AnalysisVideoDataQueryBuilder awemeShortIdList(List<String> awemeShortIdList) {
            this.awemeShortIdList = awemeShortIdList;
            return this;
        }

        public AnalysisVideoDataQueryBuilder itemIdList(List<String> itemIdList) {
            this.itemIdList = itemIdList;
            return this;
        }

        public AnalysisVideoDataQueryBuilder openTtemIdList(List<String> openTtemIdList) {
            this.openTtemIdList = openTtemIdList;
            return this;
        }

        public AnalysisVideoDataQueryBuilder queryBindType(Integer queryBindType) {
            this.queryBindType = queryBindType;
            return this;
        }

        public AnalysisVideoDataQuery build() {
            AnalysisVideoDataQuery analysisVideoDataQuery = new AnalysisVideoDataQuery();
            analysisVideoDataQuery.setClientKey(clientKey);
            analysisVideoDataQuery.setTenantId(tenantId);
            analysisVideoDataQuery.setStart_time(startTime);
            analysisVideoDataQuery.setEnd_time(endTime);
            analysisVideoDataQuery.setHost_name(hostName);
            analysisVideoDataQuery.setAweme_short_id_list(awemeShortIdList);
            analysisVideoDataQuery.setItem_id_list(itemIdList);
            analysisVideoDataQuery.setOpen_item_id_list(openTtemIdList);
            analysisVideoDataQuery.setQuery_bind_type(queryBindType);
            return analysisVideoDataQuery;
        }
    }


    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public String getHost_name() {
        return host_name;
    }

    public void setHost_name(String host_name) {
        this.host_name = host_name;
    }

    public List<String> getAweme_short_id_list() {
        return aweme_short_id_list;
    }

    public void setAweme_short_id_list(List<String> aweme_short_id_list) {
        this.aweme_short_id_list = aweme_short_id_list;
    }

    public List<String> getItem_id_list() {
        return item_id_list;
    }

    public void setItem_id_list(List<String> item_id_list) {
        this.item_id_list = item_id_list;
    }

    public List<String> getOpen_item_id_list() {
        return open_item_id_list;
    }

    public void setOpen_item_id_list(List<String> open_item_id_list) {
        this.open_item_id_list = open_item_id_list;
    }

    public Integer getQuery_bind_type() {
        return query_bind_type;
    }

    public void setQuery_bind_type(Integer query_bind_type) {
        this.query_bind_type = query_bind_type;
    }
}
