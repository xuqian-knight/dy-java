package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AuditTemplateInfo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-26 17:39
 **/
public class AwemeRelationListVo {
    /**
     * 绑定抖音号列表记录总数
     */
    private Long total_count;

    /**
     * 绑定抖音号列表
     */
    private List<AwemeRelationInfo> list;


    public static class AwemeRelationInfo {
        /**
         * 抖音号类型1：企业号2：个人号
         */
        private Integer account_type;
        private Long AwemeUID;
        /**
         * 绑定状态
         * 251：绑定失败
         * 281：审核拒绝
         * 291：审核中
         * 301：待C端同意
         * 302：绑定成功
         */
        private Integer bind_status;
        /**
         * 绑定失败原因
         */
        private String reason;
        /**
         * 抖音号昵称
         */
        private String user_name;
        /**
         * 绑定能力列表
         */
        private List<AwemeRelationCapacity> capacity_list;
        /**
         * 解绑频次信息
         */
        private List<UnbindFrequencyInfo> unbind_frequency_info;


        public Integer getAccount_type() {
            return account_type;
        }

        public void setAccount_type(Integer account_type) {
            this.account_type = account_type;
        }

        public Long getAwemeUID() {
            return AwemeUID;
        }

        public void setAwemeUID(Long awemeUID) {
            AwemeUID = awemeUID;
        }

        public Integer getBind_status() {
            return bind_status;
        }

        public void setBind_status(Integer bind_status) {
            this.bind_status = bind_status;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public List<AwemeRelationCapacity> getCapacity_list() {
            return capacity_list;
        }

        public void setCapacity_list(List<AwemeRelationCapacity> capacity_list) {
            this.capacity_list = capacity_list;
        }

        public List<UnbindFrequencyInfo> getUnbind_frequency_info() {
            return unbind_frequency_info;
        }

        public void setUnbind_frequency_info(List<UnbindFrequencyInfo> unbind_frequency_info) {
            this.unbind_frequency_info = unbind_frequency_info;
        }
    }


    public static class AwemeRelationCapacity {
        /**
         * 绑定状态1：已绑定2：未绑定3：待确认4：审核中5：审核失败
         */
        private Integer status;
        /**
         * 绑定失败原因
         */
        private String fail_reason;
        /**
         * 能力
         */
        private String capacity_key;
        /**
         * 资质信息，当审核失败时返回
         */
        private AuditTemplateInfo audit_template_info;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getFail_reason() {
            return fail_reason;
        }

        public void setFail_reason(String fail_reason) {
            this.fail_reason = fail_reason;
        }

        public String getCapacity_key() {
            return capacity_key;
        }

        public void setCapacity_key(String capacity_key) {
            this.capacity_key = capacity_key;
        }

        public AuditTemplateInfo getAudit_template_info() {
            return audit_template_info;
        }

        public void setAudit_template_info(AuditTemplateInfo audit_template_info) {
            this.audit_template_info = audit_template_info;
        }
    }

    public static class UnbindFrequencyInfo {
        /**
         * 最早一次解绑时间戳，单位秒
         */
        private Long first_unbind_time;
        /**
         * 限制次数
         */
        private Long limit_frequency;
        /**
         * 限制周期，单位：天
         */
        private Long limit_period;
        /**
         * 已解绑次数
         */
        private Long unbound_count;

        public Long getFirst_unbind_time() {
            return first_unbind_time;
        }

        public void setFirst_unbind_time(Long first_unbind_time) {
            this.first_unbind_time = first_unbind_time;
        }

        public Long getLimit_frequency() {
            return limit_frequency;
        }

        public void setLimit_frequency(Long limit_frequency) {
            this.limit_frequency = limit_frequency;
        }

        public Long getLimit_period() {
            return limit_period;
        }

        public void setLimit_period(Long limit_period) {
            this.limit_period = limit_period;
        }

        public Long getUnbound_count() {
            return unbound_count;
        }

        public void setUnbound_count(Long unbound_count) {
            this.unbound_count = unbound_count;
        }
    }

    public Long getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Long total_count) {
        this.total_count = total_count;
    }

    public List<AwemeRelationInfo> getList() {
        return list;
    }

    public void setList(List<AwemeRelationInfo> list) {
        this.list = list;
    }
}
