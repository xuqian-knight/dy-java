package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 13:37
 **/
public class ProductOnline {
    /**
     * 在线状态 1-在线 2-下线 3-封禁
     */
    private Integer online_status;
    /**
     * 商品
     */
    private ProductStruct product;
    /**
     * 售卖单元
     */
    private SkuStruct sku;

    public Integer getOnline_status() {
        return online_status;
    }

    public void setOnline_status(Integer online_status) {
        this.online_status = online_status;
    }

    public ProductStruct getProduct() {
        return product;
    }

    public void setProduct(ProductStruct product) {
        this.product = product;
    }

    public SkuStruct getSku() {
        return sku;
    }

    public void setSku(SkuStruct sku) {
        this.sku = sku;
    }
}
