package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询订单基本信息。请求值
 */
public class QueryIndustryOrderQuery extends BaseQuery {

    /**
     * 抖音开放平台内部交易订单号，通过预下单回调传给开发者服务，长度 < 64byte 选填
     */
    private String order_id;
    /**
     * 开发者系统生成的订单号，与唯一order_id关联，长度 < 64byte 选填
     */
    private String out_order_no;

    public String getOrder_id() {
        return order_id;
    }

    public QueryIndustryOrderQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryIndustryOrderQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public static TradeOrderCommonQueryBuilder builder() {
        return new TradeOrderCommonQueryBuilder();
    }

    public static final class TradeOrderCommonQueryBuilder {
        private String order_id;
        private String out_order_no;
        private Integer tenantId;
        private String clientKey;

        private TradeOrderCommonQueryBuilder() {
        }

        public TradeOrderCommonQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public TradeOrderCommonQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public TradeOrderCommonQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public TradeOrderCommonQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryIndustryOrderQuery build() {
            QueryIndustryOrderQuery queryIndustryOrderQuery = new QueryIndustryOrderQuery();
            queryIndustryOrderQuery.setOrder_id(order_id);
            queryIndustryOrderQuery.setOut_order_no(out_order_no);
            queryIndustryOrderQuery.setTenantId(tenantId);
            queryIndustryOrderQuery.setClientKey(clientKey);
            return queryIndustryOrderQuery;
        }
    }
}
