package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisUserSceneData;

import java.util.List;

public class AnalysisUserSceneDataVo {

    /**
     * 来源分析数据
     */
    private List<AnalysisUserSceneData> scene_list;

    public List<AnalysisUserSceneData> getScene_list() {
        return scene_list;
    }

    public void setScene_list(List<AnalysisUserSceneData> scene_list) {
        this.scene_list = scene_list;
    }
}
