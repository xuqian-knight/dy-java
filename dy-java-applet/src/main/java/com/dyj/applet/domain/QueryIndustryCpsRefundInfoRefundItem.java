package com.dyj.applet.domain;

/**
 * 订单已退款详细信息
 */
public class QueryIndustryCpsRefundInfoRefundItem {

    /**
     * 抖音开平侧的商品单号，只存在交易系统中 
     */
    private String item_order_id;
    /**
     * 开发者系统生成的退款单号，与抖音开平退款单号 refund_id 唯一关联 
     */
    private String out_refund_no;
    /**
     * 退款金额，单位分 
     */
    private Long refund_amount;
    /**
     * 退款时间，13 位毫秒时间戳，只有已退款才有退款时间 
     */
    private Long refund_at;
    /**
     * 显示退款状态，默认“已退款” 
     */
    private String refund_status;

    public String getItem_order_id() {
        return item_order_id;
    }

    public QueryIndustryCpsRefundInfoRefundItem setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public QueryIndustryCpsRefundInfoRefundItem setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public Long getRefund_amount() {
        return refund_amount;
    }

    public QueryIndustryCpsRefundInfoRefundItem setRefund_amount(Long refund_amount) {
        this.refund_amount = refund_amount;
        return this;
    }

    public Long getRefund_at() {
        return refund_at;
    }

    public QueryIndustryCpsRefundInfoRefundItem setRefund_at(Long refund_at) {
        this.refund_at = refund_at;
        return this;
    }

    public String getRefund_status() {
        return refund_status;
    }

    public QueryIndustryCpsRefundInfoRefundItem setRefund_status(String refund_status) {
        this.refund_status = refund_status;
        return this;
    }
}
