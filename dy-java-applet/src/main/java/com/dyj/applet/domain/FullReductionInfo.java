package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 16:32
 **/
public class FullReductionInfo {

    /**
     * 折扣门槛（分为单位）
     */
    private Long least_cost;
    /**
     * 折扣金额（分为单位）
     */
    private Long reduct_cost;

    public static FullReductionInfoBuilder builder() {
        return new FullReductionInfoBuilder();
    }

    public static class FullReductionInfoBuilder {
        private Long leastCost;
        private Long reductCost;

        public FullReductionInfoBuilder leastCost(Long leastCost) {
            this.leastCost = leastCost;
            return this;
        }

        public FullReductionInfoBuilder reductCost(Long reductCost) {
            this.reductCost = reductCost;
            return this;
        }

        public FullReductionInfo build() {
            FullReductionInfo fullReductionInfo = new FullReductionInfo();
            fullReductionInfo.setLeast_cost(leastCost);
            fullReductionInfo.setReduct_cost(reductCost);
            return fullReductionInfo;
        }
    }

    public Long getLeast_cost() {
        return least_cost;
    }

    public void setLeast_cost(Long least_cost) {
        this.least_cost = least_cost;
    }

    public Long getReduct_cost() {
        return reduct_cost;
    }

    public void setReduct_cost(Long reduct_cost) {
        this.reduct_cost = reduct_cost;
    }
}
