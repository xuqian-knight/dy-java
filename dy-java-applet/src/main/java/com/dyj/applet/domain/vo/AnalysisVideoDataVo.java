package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisVideoDealOverviewData;

public class AnalysisVideoDataVo {

    private AnalysisVideoDealOverviewData video_deal_overview_data;

    public AnalysisVideoDealOverviewData getVideo_deal_overview_data() {
        return video_deal_overview_data;
    }

    public void setVideo_deal_overview_data(AnalysisVideoDealOverviewData video_deal_overview_data) {
        this.video_deal_overview_data = video_deal_overview_data;
    }
}
