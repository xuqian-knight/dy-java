package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class ShortLiveIdWithAwemeIdQuery extends BaseQuery {

    /**
     * 开始时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     */
    private Long start_time;
    /**
     * 结束时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
     */
    private Long end_time;
    /**
     * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；不传表示查全部端数据
     */
    private String host_name;
    /**
     * 查询数据的挂载类型，0-自挂载+达人挂载数据，1-自挂载数据，4-自挂载+达人挂载+UGC挂载数据，目前该参数只能传0或1或4，传其他参数会产生非预期的结果
     */
    private Long query_bind_type;
    /**
     *
     * 没有抖音号列表，需要传递 空的字符串数组，如果查询抖音号列表，限制长度在200以内，即一次性最多查询 200个抖音号 所对应的自挂载视频所产生的数据
     */
    private List<String> aweme_short_id_list;
    /**
     * 查询的列表类型，传4表示查询短视频发布数据，传5表示查询直播开播数据，不传表示同时查询两者的信息
     */
    private Long query_data_type;

    public static ShortLiveIdWithAwemeIdQueryBuilder builder() {
        return new ShortLiveIdWithAwemeIdQueryBuilder();
    }

    public static class ShortLiveIdWithAwemeIdQueryBuilder {
        /**
         * 开始时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
         */
        private Long startTime;
        /**
         * 结束时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
         */
        private Long endTime;
        /**
         * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；不传表示查全部端数据
         */
        private String hostName;
        /**
         * 查询数据的挂载类型，0-自挂载+达人挂载数据，1-自挂载数据，4-自挂载+达人挂载+UGC挂载数据，目前该参数只能传0或1或4，传其他参数会产生非预期的结果
         */
        private Long queryBindType;
        /**
         *
         * 没有抖音号列表，需要传递 空的字符串数组，如果查询抖音号列表，限制长度在200以内，即一次性最多查询 200个抖音号 所对应的自挂载视频所产生的数据
         */
        private List<String> awemeShortIdList;
        /**
         * 查询的列表类型，传4表示查询短视频发布数据，传5表示查询直播开播数据，不传表示同时查询两者的信息
         */
        private Long queryDataType;

        /**
         * 租户ID
         */
        private Integer tenantId;
        /**
         * 应用Key
         */
        private String clientKey;

        public ShortLiveIdWithAwemeIdQueryBuilder startTime(Long startTime) {
            this.startTime = startTime;
            return this;
        }

        public ShortLiveIdWithAwemeIdQueryBuilder endTime(Long endTime) {
            this.endTime = endTime;
            return this;
        }

        public ShortLiveIdWithAwemeIdQueryBuilder hostName(String hostName) {
            this.hostName = hostName;
            return this;
        }

        public ShortLiveIdWithAwemeIdQueryBuilder queryBindType(Long queryBindType) {
            this.queryBindType = queryBindType;
            return this;
        }

        public ShortLiveIdWithAwemeIdQueryBuilder awemeShortIdList(List<String> awemeShortIdList) {
            this.awemeShortIdList = awemeShortIdList;
            return this;
        }

        public ShortLiveIdWithAwemeIdQueryBuilder queryDataType(Long queryDataType) {
            this.queryDataType = queryDataType;
            return this;
        }

        public ShortLiveIdWithAwemeIdQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ShortLiveIdWithAwemeIdQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public ShortLiveIdWithAwemeIdQuery build() {
            ShortLiveIdWithAwemeIdQuery shortLiveIdWithAwemeIdQuery = new ShortLiveIdWithAwemeIdQuery();
            shortLiveIdWithAwemeIdQuery.setStart_time(startTime);
            shortLiveIdWithAwemeIdQuery.setEnd_time(endTime);
            shortLiveIdWithAwemeIdQuery.setHost_name(hostName);
            shortLiveIdWithAwemeIdQuery.setQuery_bind_type(queryBindType);
            shortLiveIdWithAwemeIdQuery.setAweme_short_id_list(awemeShortIdList);
            shortLiveIdWithAwemeIdQuery.setQuery_data_type(queryDataType);

            shortLiveIdWithAwemeIdQuery.setTenantId(tenantId);
            shortLiveIdWithAwemeIdQuery.setClientKey(clientKey);
            return shortLiveIdWithAwemeIdQuery;
        }
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public String getHost_name() {
        return host_name;
    }

    public void setHost_name(String host_name) {
        this.host_name = host_name;
    }

    public Long getQuery_bind_type() {
        return query_bind_type;
    }

    public void setQuery_bind_type(Long query_bind_type) {
        this.query_bind_type = query_bind_type;
    }

    public List<String> getAweme_short_id_list() {
        return aweme_short_id_list;
    }

    public void setAweme_short_id_list(List<String> aweme_short_id_list) {
        this.aweme_short_id_list = aweme_short_id_list;
    }

    public Long getQuery_data_type() {
        return query_data_type;
    }

    public void setQuery_data_type(Long query_data_type) {
        this.query_data_type = query_data_type;
    }
}
