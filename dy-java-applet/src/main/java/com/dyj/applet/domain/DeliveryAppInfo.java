package com.dyj.applet.domain;

public class DeliveryAppInfo {

    /**
     * 引导文案: 通过query_text查询接口设置
     * 选填
     */
    private String guidance_text_id;

    /**
     * 核销/预约按钮文案: 通过query_text查询接口取其id
     * 选填
     */
    private String button_text_id;

    /**
     * 详情页按钮文案: 通过query_text查询接口取其id，仅到综进家场景可以配置
     * 选填
     */
    private String detail_page_text_id;

    /**
     * 二维码展示方式，枚举值（1，2，3）
     */
    private Integer display_mode;

    public String getGuidance_text_id() {
        return guidance_text_id;
    }

    public DeliveryAppInfo setGuidance_text_id(String guidance_text_id) {
        this.guidance_text_id = guidance_text_id;
        return this;
    }

    public String getButton_text_id() {
        return button_text_id;
    }

    public DeliveryAppInfo setButton_text_id(String button_text_id) {
        this.button_text_id = button_text_id;
        return this;
    }

    public String getDetail_page_text_id() {
        return detail_page_text_id;
    }

    public DeliveryAppInfo setDetail_page_text_id(String detail_page_text_id) {
        this.detail_page_text_id = detail_page_text_id;
        return this;
    }

    public Integer getDisplay_mode() {
        return display_mode;
    }

    public DeliveryAppInfo setDisplay_mode(Integer display_mode) {
        this.display_mode = display_mode;
        return this;
    }
}
