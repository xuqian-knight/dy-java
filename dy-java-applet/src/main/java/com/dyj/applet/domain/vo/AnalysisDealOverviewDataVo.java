package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisDealData;

import java.util.List;

public class AnalysisDealOverviewDataVo {

    /**
     * 当前交易分析总览数据
     */
    private AnalysisDealData deal_overview_data;

    /**
     * 交易分析每日总览数据
     */
    private List<AnalysisDealData> deal_data_list;

    public AnalysisDealData getDeal_overview_data() {
        return deal_overview_data;
    }

    public void setDeal_overview_data(AnalysisDealData deal_overview_data) {
        this.deal_overview_data = deal_overview_data;
    }

    public List<AnalysisDealData> getDeal_data_list() {
        return deal_data_list;
    }

    public void setDeal_data_list(List<AnalysisDealData> deal_data_list) {
        this.deal_data_list = deal_data_list;
    }
}
