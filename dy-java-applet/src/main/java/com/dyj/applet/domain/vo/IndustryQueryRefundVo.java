package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryRefundResult;
import com.dyj.applet.domain.RefundItemOrderDetail;
import com.dyj.applet.domain.RefundMerchantAuditDetail;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

public class IndustryQueryRefundVo extends BaseVo {

    /**
     * 退款创建时间，13位毫秒时间戳
     */
    private Long create_at;
    /**
     * 退款结果信息，可以通过该字段了解退款失败原因 选填
     */
    private String message;
    /**
     * 系统订单信息，开放平台生成的订单号
     */
    private String order_id;
    /**
     * 开发者系统生成的退款单号，与抖音开平退款单号唯一关联 选填
     */
    private String out_refund_no;
    /**
     * 退款时间，13位毫秒时间戳，只有已退款才有退款时间 选填
     */
    private Long refund_at;
    /**
     * 系统退款单号，开放平台生成的退款单号
     */
    private String refund_id;

    /**
     * <ul><li>退款状态:退款中-PROCESSING</li><li>已退款-SUCCESS</li><li>退款失败-FAIL</li></ul>
     */
    private String refund_status;
    /**
     * 退款金额，单位[分]
     */
    private Long refund_total_amount;
    /**
     * 系统订单信息，开放平台生成的订单号 选填
     */
    private List<RefundItemOrderDetail> item_order_detail;
    /**
     * 退款审核信息 选填
     */
    private RefundMerchantAuditDetail merchant_audit_detail;

    /**
     * 退款查询结果列表，最多返回50条
     * (建议开发者用这个字段)
     */
    private List<QueryRefundResult> refund_list;

    public Long getCreate_at() {
        return create_at;
    }

    public IndustryQueryRefundVo setCreate_at(Long create_at) {
        this.create_at = create_at;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public IndustryQueryRefundVo setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public IndustryQueryRefundVo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public IndustryQueryRefundVo setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public Long getRefund_at() {
        return refund_at;
    }

    public IndustryQueryRefundVo setRefund_at(Long refund_at) {
        this.refund_at = refund_at;
        return this;
    }

    public String getRefund_id() {
        return refund_id;
    }

    public IndustryQueryRefundVo setRefund_id(String refund_id) {
        this.refund_id = refund_id;
        return this;
    }

    public String getRefund_status() {
        return refund_status;
    }

    public IndustryQueryRefundVo setRefund_status(String refund_status) {
        this.refund_status = refund_status;
        return this;
    }

    public Long getRefund_total_amount() {
        return refund_total_amount;
    }

    public IndustryQueryRefundVo setRefund_total_amount(Long refund_total_amount) {
        this.refund_total_amount = refund_total_amount;
        return this;
    }

    public List<RefundItemOrderDetail> getItem_order_detail() {
        return item_order_detail;
    }

    public IndustryQueryRefundVo setItem_order_detail(List<RefundItemOrderDetail> item_order_detail) {
        this.item_order_detail = item_order_detail;
        return this;
    }

    public RefundMerchantAuditDetail getMerchant_audit_detail() {
        return merchant_audit_detail;
    }

    public IndustryQueryRefundVo setMerchant_audit_detail(RefundMerchantAuditDetail merchant_audit_detail) {
        this.merchant_audit_detail = merchant_audit_detail;
        return this;
    }

    public List<QueryRefundResult> getRefund_list() {
        return refund_list;
    }

    public IndustryQueryRefundVo setRefund_list(List<QueryRefundResult> refund_list) {
        this.refund_list = refund_list;
        return this;
    }
}
