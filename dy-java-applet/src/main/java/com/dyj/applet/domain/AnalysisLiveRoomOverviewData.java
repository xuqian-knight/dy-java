package com.dyj.applet.domain;

public class AnalysisLiveRoomOverviewData {

    /**
     * 评论次数，挂载了小程序的主播, 直播开播开始，在直播间发评论的次数
     */
    private String comment_count;
    /**
     * 累计看播人数，挂载了小程序的主播, 在直播被打开观看的人数
     */
    private String cumulative_audience_count;
    /**
     * 新增粉丝数，挂载了小程序的主播, 直播开播开始，在直播间关注主播的粉丝数
     */
    private String increased_fans_count;
    /**
     * 点赞次数，挂载了小程序的主播, 直播开播开始，点赞的次数
     */
    private String like_times;
    /**
     * 最高在线人数，挂载了小程序的主播, 直播同时在线用户最高的人数
     */
    private String online_user_count;
    /**
     * 人均观看时长（单位：秒），总看播时长/看播人数
     */
    private String per_capita_time;
    /**
     * 分享次数，挂载了小程序的主播, 直播开播开始，分享成功的人数
     */
    private String share_count;

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getCumulative_audience_count() {
        return cumulative_audience_count;
    }

    public void setCumulative_audience_count(String cumulative_audience_count) {
        this.cumulative_audience_count = cumulative_audience_count;
    }

    public String getIncreased_fans_count() {
        return increased_fans_count;
    }

    public void setIncreased_fans_count(String increased_fans_count) {
        this.increased_fans_count = increased_fans_count;
    }

    public String getLike_times() {
        return like_times;
    }

    public void setLike_times(String like_times) {
        this.like_times = like_times;
    }

    public String getOnline_user_count() {
        return online_user_count;
    }

    public void setOnline_user_count(String online_user_count) {
        this.online_user_count = online_user_count;
    }

    public String getPer_capita_time() {
        return per_capita_time;
    }

    public void setPer_capita_time(String per_capita_time) {
        this.per_capita_time = per_capita_time;
    }

    public String getShare_count() {
        return share_count;
    }

    public void setShare_count(String share_count) {
        this.share_count = share_count;
    }
}
