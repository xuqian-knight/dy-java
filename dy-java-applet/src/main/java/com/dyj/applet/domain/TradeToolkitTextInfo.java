package com.dyj.applet.domain;

import java.util.List;

public class TradeToolkitTextInfo {

    /**
     * 文案类型
     */
    private Integer text_type;

    /**
     * 该类型下，文案信息列表
     */
    private List<TradeToolkitTextContent> text_content_list;

    public Integer getText_type() {
        return text_type;
    }

    public TradeToolkitTextInfo setText_type(Integer text_type) {
        this.text_type = text_type;
        return this;
    }

    public List<TradeToolkitTextContent> getText_content_list() {
        return text_content_list;
    }

    public TradeToolkitTextInfo setText_content_list(List<TradeToolkitTextContent> text_content_list) {
        this.text_content_list = text_content_list;
        return this;
    }
}
