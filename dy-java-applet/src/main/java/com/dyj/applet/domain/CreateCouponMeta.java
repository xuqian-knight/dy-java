package com.dyj.applet.domain;

/**
 * 创建券模板请求值
 */
public class CreateCouponMeta {


    /**
     * <p>发券确认的回调url</p><p>用户在直播间领券后平台会生成券记录且处理库存扣减等逻辑，完成后会通过该url通知开发者，具体实现请参考 <a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/merchant-coupon/coupon-mgmt/coupon-callback" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">用户领券结果回调通知</a></p><p>特别注意：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">若开发者无响应，则用户无法完成最终的领券（用户处于等待状态），故需要确保接口的时效性，建议 pct99 耗时 &lt; 200ms</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">当小程序的新用户领券进入时，回调接口会传递新用户的openid，此时在开发者的用户系统中可能尚不存在，需考虑兼容问题，确保新用户仍可发放成功</li></ul> 选填
     */
    private String callback_url;
    /**
     * <p>使用须知（长度15以内）</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">优惠券使用描述文案，用于在卡券包，直播领券等位置对优惠券的解释说明</li></ul>
     */
    private String consume_desc;
    /**
     * <p>当前券模版的去使用链接，用户领取后可在直播间、卡包等场景点击「去使用」后跳转到「该链接」</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">只能传path和query，不能以"/"开头</li></ul>
     */
    private String consume_path;
    /**
     * <p>券名称，长度15以内（汉字、英文、符号分别算一个字），不能包含敏感字符和特殊字符，若触发敏感字符接口会有错误码提示</p><p>注意：</p><p>针对「优惠券」玩法，平台会在展示券的地方结构化展示金额和满减条件，故可在券名称中突出优惠对象本身</p>
     */
    private String coupon_name;
    /**
     * <p>优惠金额，单位分：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">立减、满减券（discount_type = 1 || discount_type =2 ）必填 ，需小于等于100000分。</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">权益券（discount_type = 3 ）不校验金额</li></ul> 选填
     */
    private Long discount_amount;
    /**
     * <p>优惠类型</p>
     */
    private Integer discount_type;
    /**
     * <p>小程序券模板号（开发者内部系统的券模版自增id或uuid，抖音开放平台会校验该值的唯一性）</p>
     */
    private String merchant_meta_no;
    /**
     * <p>满减门槛金额，单位分：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">满减券（discount_type = 2 ）必填</li></ul> 选填
     */
    private Long min_pay_amount;
    /**
     * <p>具体的映射ID</p><ul><li>当discount_type=3且related_type=1或2时必填</li></ul> 选填
     */
    private String origin_id;
    /**
     * <p>券模板生效时间（即可领取开始时间），单位秒，和当前时间最大时间差为30天，小于当前时间，立即生效</p>
     */
    private Long receive_begin_time;
    /**
     * <p>券的领取须知（长度20以内）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">用于在卡包等场景展示当前优惠券的领取规则说明</li></ul> 选填
     */
    private String receive_desc;
    /**
     * <p>券模板过期时间（即可领取结束时间），单位秒，必须大于receive_begin_time，大于当前时间，和当前时间最大时间差为60天</p>
     */
    private Long receive_end_time;
    /**
     * <p>券模板的关联内容ID：</p><ul><li>当discount_type=3时，必填</li></ul> 选填
     */
    private Integer related_type;
    /**
     * 密钥类型
     密钥来源 选填
     */
    private Integer secret_source;
    /**
     * <p>券模板最大库存，最大值 1000000000；此库存是该券可发放的总库存，所有主播共享。</p>
     */
    private Long stock_number;
    /**
     * <p>用户领取的「券记录」的有效期开始时间（单位秒）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">当valid_type=1时，必填</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">和当前时间最大时间差为30天（即最大相差值为2592000）</li></ul> 选填
     */
    private Long valid_begin_time;
    /**
     * <p>用户领取的「券记录」的有效时长（单位秒）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">当valid_type=2时，必填</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">最大日期为30天（即最大值为2592000）</li></ul> 选填
     */
    private Long valid_duration;
    /**
     * <p>用户领取的「券记录」的有效期结束时间（单位秒）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">当valid_type=1时，必填</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">valid_end_time必须满足：</li><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">大于当前时间</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">与当前时间最大时间差为60天</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">与valid_begin_time最大时间差为30天</li></ul></ul> 选填
     */
    private Long valid_end_time;
    /**
     * <p>用户领券后「该用户所领取的券记录」的有效期类型</p>
     */
    private Integer valid_type;

    public String getCallback_url() {
        return callback_url;
    }

    public CreateCouponMeta setCallback_url(String callback_url) {
        this.callback_url = callback_url;
        return this;
    }

    public String getConsume_desc() {
        return consume_desc;
    }

    public CreateCouponMeta setConsume_desc(String consume_desc) {
        this.consume_desc = consume_desc;
        return this;
    }

    public String getConsume_path() {
        return consume_path;
    }

    public CreateCouponMeta setConsume_path(String consume_path) {
        this.consume_path = consume_path;
        return this;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public CreateCouponMeta setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public CreateCouponMeta setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public Integer getDiscount_type() {
        return discount_type;
    }

    public CreateCouponMeta setDiscount_type(Integer discount_type) {
        this.discount_type = discount_type;
        return this;
    }

    public String getMerchant_meta_no() {
        return merchant_meta_no;
    }

    public CreateCouponMeta setMerchant_meta_no(String merchant_meta_no) {
        this.merchant_meta_no = merchant_meta_no;
        return this;
    }

    public Long getMin_pay_amount() {
        return min_pay_amount;
    }

    public CreateCouponMeta setMin_pay_amount(Long min_pay_amount) {
        this.min_pay_amount = min_pay_amount;
        return this;
    }

    public String getOrigin_id() {
        return origin_id;
    }

    public CreateCouponMeta setOrigin_id(String origin_id) {
        this.origin_id = origin_id;
        return this;
    }

    public Long getReceive_begin_time() {
        return receive_begin_time;
    }

    public CreateCouponMeta setReceive_begin_time(Long receive_begin_time) {
        this.receive_begin_time = receive_begin_time;
        return this;
    }

    public String getReceive_desc() {
        return receive_desc;
    }

    public CreateCouponMeta setReceive_desc(String receive_desc) {
        this.receive_desc = receive_desc;
        return this;
    }

    public Long getReceive_end_time() {
        return receive_end_time;
    }

    public CreateCouponMeta setReceive_end_time(Long receive_end_time) {
        this.receive_end_time = receive_end_time;
        return this;
    }

    public Integer getRelated_type() {
        return related_type;
    }

    public CreateCouponMeta setRelated_type(Integer related_type) {
        this.related_type = related_type;
        return this;
    }

    public Integer getSecret_source() {
        return secret_source;
    }

    public CreateCouponMeta setSecret_source(Integer secret_source) {
        this.secret_source = secret_source;
        return this;
    }

    public Long getStock_number() {
        return stock_number;
    }

    public CreateCouponMeta setStock_number(Long stock_number) {
        this.stock_number = stock_number;
        return this;
    }

    public Long getValid_begin_time() {
        return valid_begin_time;
    }

    public CreateCouponMeta setValid_begin_time(Long valid_begin_time) {
        this.valid_begin_time = valid_begin_time;
        return this;
    }

    public Long getValid_duration() {
        return valid_duration;
    }

    public CreateCouponMeta setValid_duration(Long valid_duration) {
        this.valid_duration = valid_duration;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public CreateCouponMeta setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }

    public Integer getValid_type() {
        return valid_type;
    }

    public CreateCouponMeta setValid_type(Integer valid_type) {
        this.valid_type = valid_type;
        return this;
    }

    public static CreateCouponMetaBuilder builder(){
        return new CreateCouponMetaBuilder();
    }

    public static final class CreateCouponMetaBuilder {
        private String callback_url;
        private String consume_desc;
        private String consume_path;
        private String coupon_name;
        private Long discount_amount;
        private Integer discount_type;
        private String merchant_meta_no;
        private Long min_pay_amount;
        private String origin_id;
        private Long receive_begin_time;
        private String receive_desc;
        private Long receive_end_time;
        private Integer related_type;
        private Integer secret_source;
        private Long stock_number;
        private Long valid_begin_time;
        private Long valid_duration;
        private Long valid_end_time;
        private Integer valid_type;

        private CreateCouponMetaBuilder() {
        }

        public CreateCouponMetaBuilder callbackUrl(String callbackUrl) {
            this.callback_url = callbackUrl;
            return this;
        }

        public CreateCouponMetaBuilder consumeDesc(String consumeDesc) {
            this.consume_desc = consumeDesc;
            return this;
        }

        public CreateCouponMetaBuilder consumePath(String consumePath) {
            this.consume_path = consumePath;
            return this;
        }

        public CreateCouponMetaBuilder couponName(String couponName) {
            this.coupon_name = couponName;
            return this;
        }

        public CreateCouponMetaBuilder discountAmount(Long discountAmount) {
            this.discount_amount = discountAmount;
            return this;
        }

        public CreateCouponMetaBuilder discountType(Integer discountType) {
            this.discount_type = discountType;
            return this;
        }

        public CreateCouponMetaBuilder merchantMetaNo(String merchantMetaNo) {
            this.merchant_meta_no = merchantMetaNo;
            return this;
        }

        public CreateCouponMetaBuilder minPayAmount(Long minPayAmount) {
            this.min_pay_amount = minPayAmount;
            return this;
        }

        public CreateCouponMetaBuilder originId(String originId) {
            this.origin_id = originId;
            return this;
        }

        public CreateCouponMetaBuilder receiveBeginTime(Long receiveBeginTime) {
            this.receive_begin_time = receiveBeginTime;
            return this;
        }

        public CreateCouponMetaBuilder receiveDesc(String receiveDesc) {
            this.receive_desc = receiveDesc;
            return this;
        }

        public CreateCouponMetaBuilder receiveEndTime(Long receiveEndTime) {
            this.receive_end_time = receiveEndTime;
            return this;
        }

        public CreateCouponMetaBuilder relatedType(Integer relatedType) {
            this.related_type = relatedType;
            return this;
        }

        public CreateCouponMetaBuilder secretSource(Integer secretSource) {
            this.secret_source = secretSource;
            return this;
        }

        public CreateCouponMetaBuilder stockNumber(Long stockNumber) {
            this.stock_number = stockNumber;
            return this;
        }

        public CreateCouponMetaBuilder validBeginTime(Long validBeginTime) {
            this.valid_begin_time = validBeginTime;
            return this;
        }

        public CreateCouponMetaBuilder validDuration(Long validDuration) {
            this.valid_duration = validDuration;
            return this;
        }

        public CreateCouponMetaBuilder validEndTime(Long validEndTime) {
            this.valid_end_time = validEndTime;
            return this;
        }

        public CreateCouponMetaBuilder validType(Integer validType) {
            this.valid_type = validType;
            return this;
        }

        public CreateCouponMeta build() {
            CreateCouponMeta createCouponMeta = new CreateCouponMeta();
            createCouponMeta.setCallback_url(callback_url);
            createCouponMeta.setConsume_desc(consume_desc);
            createCouponMeta.setConsume_path(consume_path);
            createCouponMeta.setCoupon_name(coupon_name);
            createCouponMeta.setDiscount_amount(discount_amount);
            createCouponMeta.setDiscount_type(discount_type);
            createCouponMeta.setMerchant_meta_no(merchant_meta_no);
            createCouponMeta.setMin_pay_amount(min_pay_amount);
            createCouponMeta.setOrigin_id(origin_id);
            createCouponMeta.setReceive_begin_time(receive_begin_time);
            createCouponMeta.setReceive_desc(receive_desc);
            createCouponMeta.setReceive_end_time(receive_end_time);
            createCouponMeta.setRelated_type(related_type);
            createCouponMeta.setSecret_source(secret_source);
            createCouponMeta.setStock_number(stock_number);
            createCouponMeta.setValid_begin_time(valid_begin_time);
            createCouponMeta.setValid_duration(valid_duration);
            createCouponMeta.setValid_end_time(valid_end_time);
            createCouponMeta.setValid_type(valid_type);
            return createCouponMeta;
        }
    }
}
