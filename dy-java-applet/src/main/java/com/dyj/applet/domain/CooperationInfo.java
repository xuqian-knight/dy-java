package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-24 16:38
 **/
public class CooperationInfo {

    /**
     * 合作类型
     * enterprise：企业
     * personal：个人
     */
    private String cooperation_type;

    /**
     * 关系过期时间，时间戳最长一年有效期
     */
    private Long relation_expire_date;

    /**
     * 营业执照编号，企业合作类型必传
     */
    private String business_license_code;
    /**
     * 公司名称，企业合作类型必传
     */
    private String company_name;
    /**
     * 合作合同照片，使用上传素材接口获取到的路径上传素材接口type传2
     * contract_image和contract_image_list必传一个
     */
    private String contract_image;

    /**
     * 合作合同照片列表，使用上传素材接口获取到的路径上传素材接口type传2
     * contract_image和contract_image_list必传一个
     */
    private List<String> contract_image_list;
    /**
     * 身份证号，个人合作类型必传
     */
    private String identity_number;
    /**
     * 姓名，个人合作类型必传
     */
    private String real_name;


    public static class CooperationInfoBuilder {
        /**
         * 合作类型
         * enterprise：企业
         * personal：个人
         */
        private String cooperationType;

        /**
         * 关系过期时间，时间戳最长一年有效期
         */
        private Long relationExpireDate;

        /**
         * 营业执照编号，企业合作类型必传
         */
        private String businessLicenseCode;
        /**
         * 公司名称，企业合作类型必传
         */
        private String companyName;
        /**
         * 合作合同照片，使用上传素材接口获取到的路径上传素材接口type传2
         * contract_image和contract_image_list必传一个
         */
        private String contractImage;

        /**
         * 合作合同照片列表，使用上传素材接口获取到的路径上传素材接口type传2
         * contract_image和contract_image_list必传一个
         */
        private List<String> contractImageList;
        /**
         * 身份证号，个人合作类型必传
         */
        private String identityNumber;
        /**
         * 姓名，个人合作类型必传
         */
        private String realName;

        public CooperationInfoBuilder cooperationType(String cooperationType) {
            this.cooperationType = cooperationType;
            return this;
        }
        public CooperationInfoBuilder relationExpireDate(Long relationExpireDate) {
            this.relationExpireDate = relationExpireDate;
            return this;
        }
        public CooperationInfoBuilder businessLicenseCode(String businessLicenseCode) {
            this.businessLicenseCode = businessLicenseCode;
            return this;
        }
        public CooperationInfoBuilder companyName(String companyName) {
            this.companyName = companyName;
            return this;
        }
        public CooperationInfoBuilder contractImage(String contractImage) {
            this.contractImage = contractImage;
            return this;
        }
        public CooperationInfoBuilder contractImageList(List<String> contractImageList) {
            this.contractImageList = contractImageList;
            return this;
        }
        public CooperationInfoBuilder identityNumber(String identityNumber) {
            this.identityNumber = identityNumber;
            return this;
        }
        public CooperationInfoBuilder realName(String realName) {
            this.realName = realName;
            return this;
        }
        public CooperationInfo build() {
            CooperationInfo cooperationInfo = new CooperationInfo();
            cooperationInfo.setCooperation_type(cooperationType);
            cooperationInfo.setRelation_expire_date(relationExpireDate);
            cooperationInfo.setBusiness_license_code(businessLicenseCode);
            cooperationInfo.setCompany_name(companyName);
            cooperationInfo.setContract_image(contractImage);
            cooperationInfo.setContract_image_list(contractImageList);
            cooperationInfo.setIdentity_number(identityNumber);
            cooperationInfo.setReal_name(realName);
            return cooperationInfo;
        }
    }

    public static CooperationInfoBuilder builder() {
       return new CooperationInfoBuilder();
    }

    public String getCooperation_type() {
        return cooperation_type;
    }

    public void setCooperation_type(String cooperation_type) {
        this.cooperation_type = cooperation_type;
    }

    public Long getRelation_expire_date() {
        return relation_expire_date;
    }

    public void setRelation_expire_date(Long relation_expire_date) {
        this.relation_expire_date = relation_expire_date;
    }

    public String getBusiness_license_code() {
        return business_license_code;
    }

    public void setBusiness_license_code(String business_license_code) {
        this.business_license_code = business_license_code;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getContract_image() {
        return contract_image;
    }

    public void setContract_image(String contract_image) {
        this.contract_image = contract_image;
    }

    public List<String> getContract_image_list() {
        return contract_image_list;
    }

    public void setContract_image_list(List<String> contract_image_list) {
        this.contract_image_list = contract_image_list;
    }

    public String getIdentity_number() {
        return identity_number;
    }

    public void setIdentity_number(String identity_number) {
        this.identity_number = identity_number;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }
}
