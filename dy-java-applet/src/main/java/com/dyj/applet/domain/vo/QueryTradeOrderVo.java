package com.dyj.applet.domain.vo;

public class QueryTradeOrderVo {

    /**
     * 渠道支付单号，如微信的支付单号
     */
    private String channel_pay_id;
    /**
     * 预下单时开发者定义的透传信息
     */
    private String cp_extra;
    /**
     * 订单核销类型：
     */
    private Integer delivery_type;
    /**
     * 视频id
     */
    private String item_id;
    /**
     * 结果描述信息，如失败原因
     */
    private String message;
    /**
     * 抖音开平侧订单号
     */
    private String order_id;
    /**
     * 订单状态，
     */
    private String order_status;
    /**
     * 开发者侧订单号，与 order_id 一一对应
     */
    private String out_order_no;
    /**
     * 支付渠道枚举
     */
    private Integer pay_channel;
    /**
     * 支付时间，格式：2021-12-12 00:00:00
     */
    private String pay_time;
    /**
     * 担保支付单 id
     */
    private String payment_order_id;
    /**
     * 已退款金额，单位分
     */
    private Long refund_amount;
    /**
     * 卖家商户号 id
     */
    private String seller_uid;
    /**
     * 已分账金额，单位分
     */
    private Long settle_amount;
    /**
     * 订单实际支付金额，单位[分]
     */
    private Long total_fee;

    public String getChannel_pay_id() {
        return channel_pay_id;
    }

    public QueryTradeOrderVo setChannel_pay_id(String channel_pay_id) {
        this.channel_pay_id = channel_pay_id;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public QueryTradeOrderVo setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public Integer getDelivery_type() {
        return delivery_type;
    }

    public QueryTradeOrderVo setDelivery_type(Integer delivery_type) {
        this.delivery_type = delivery_type;
        return this;
    }

    public String getItem_id() {
        return item_id;
    }

    public QueryTradeOrderVo setItem_id(String item_id) {
        this.item_id = item_id;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public QueryTradeOrderVo setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public QueryTradeOrderVo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOrder_status() {
        return order_status;
    }

    public QueryTradeOrderVo setOrder_status(String order_status) {
        this.order_status = order_status;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryTradeOrderVo setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public Integer getPay_channel() {
        return pay_channel;
    }

    public QueryTradeOrderVo setPay_channel(Integer pay_channel) {
        this.pay_channel = pay_channel;
        return this;
    }

    public String getPay_time() {
        return pay_time;
    }

    public QueryTradeOrderVo setPay_time(String pay_time) {
        this.pay_time = pay_time;
        return this;
    }

    public String getPayment_order_id() {
        return payment_order_id;
    }

    public QueryTradeOrderVo setPayment_order_id(String payment_order_id) {
        this.payment_order_id = payment_order_id;
        return this;
    }

    public Long getRefund_amount() {
        return refund_amount;
    }

    public QueryTradeOrderVo setRefund_amount(Long refund_amount) {
        this.refund_amount = refund_amount;
        return this;
    }

    public String getSeller_uid() {
        return seller_uid;
    }

    public QueryTradeOrderVo setSeller_uid(String seller_uid) {
        this.seller_uid = seller_uid;
        return this;
    }

    public Long getSettle_amount() {
        return settle_amount;
    }

    public QueryTradeOrderVo setSettle_amount(Long settle_amount) {
        this.settle_amount = settle_amount;
        return this;
    }

    public Long getTotal_fee() {
        return total_fee;
    }

    public QueryTradeOrderVo setTotal_fee(Long total_fee) {
        this.total_fee = total_fee;
        return this;
    }
}
