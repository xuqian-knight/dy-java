package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 15:03
 **/
public class SupplierSyncStatus {

    /**
     * 最近一次同步状态, 0 - 未处理; 1 - 通过; 2 - 未通过
     */
    private Integer last_sync_status;

    /**
     * 同步失败原因，抖音风控政策问题，该字段无法提供太多信息，目前审核不通过联系抖音运营做进一步处理
     */
    private String fail_reason;

    public Integer getLast_sync_status() {
        return last_sync_status;
    }

    public void setLast_sync_status(Integer last_sync_status) {
        this.last_sync_status = last_sync_status;
    }

    public String getFail_reason() {
        return fail_reason;
    }

    public void setFail_reason(String fail_reason) {
        this.fail_reason = fail_reason;
    }
}
