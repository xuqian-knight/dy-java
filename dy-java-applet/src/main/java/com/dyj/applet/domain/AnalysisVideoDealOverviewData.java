package com.dyj.applet.domain;

public class AnalysisVideoDealOverviewData {

    /**
     * 创建订单数，通过短视频直接挂载小程序，创建的订单数量
     */
    private Long create_order_cnt;
    /**
     * 客单价，单位：分
     */
    private Long customer_once_price;
    /**
     * 锚点点击次数，短视频直接挂载小程序的锚点点击次数
     */
    private Long mp_click_pv;
    /**
     * 进入小程序次数
     */
    private Long mp_drainage_pv;
    /**
     * 进入小程序人数
     */
    private Long mp_drainage_uv;
    /**
     * 锚点曝光次数，短视频直接挂载小程序的锚点曝光次数
     */
    private Long mp_show_pv;
    /**
     * 小程序曝光人数
     */
    private Long mp_show_uv;
    /**
     * 单均价，单位：分
     */
    private Long order_once_price;
    /**
     * 支付人数
     */
    private Long pay_customer_cnt;
    /**
     * 支付金额，单位：分
     */
    private Long pay_order_amount;
    /**
     * 支付订单数，通过短视频直接挂载小程序，支付成功的订单数量
     */
    private Long pay_order_cnt;
    /**
     * 退款金额，单位：分
     */
    private Long refund_amount;
    /**
     * 退款人数
     */
    private Long refund_customer_cnt;
    /**
     * 退款订单数
     */
    private Long refund_order_cnt;
    /**
     * 视频播放次数，直接挂载小程序的短视频累计播放量
     */
    private Long video_play_cnt;
    /**
     * 线索数
     */
    private Long clue_cnt;
    /**
     * 线索人数
     */
    private Long clue_ucnt;
    /**
     * 创建订单人数
     */
    private Long create_customer_cnt;
    /**
     * 视频播放人数
     */
    private Long item_uv;
    /**
     * 视频播放次数
     */
    private Long item_vv;
    /**
     * 有效线索数
     */
    private Long valid_clue_cnt;
    /**
     * 有效线索人数
     */
    private Long valid_clue_ucnt;

    public Long getCreate_order_cnt() {
        return create_order_cnt;
    }

    public void setCreate_order_cnt(Long create_order_cnt) {
        this.create_order_cnt = create_order_cnt;
    }

    public Long getCustomer_once_price() {
        return customer_once_price;
    }

    public void setCustomer_once_price(Long customer_once_price) {
        this.customer_once_price = customer_once_price;
    }

    public Long getMp_click_pv() {
        return mp_click_pv;
    }

    public void setMp_click_pv(Long mp_click_pv) {
        this.mp_click_pv = mp_click_pv;
    }

    public Long getMp_drainage_pv() {
        return mp_drainage_pv;
    }

    public void setMp_drainage_pv(Long mp_drainage_pv) {
        this.mp_drainage_pv = mp_drainage_pv;
    }

    public Long getMp_drainage_uv() {
        return mp_drainage_uv;
    }

    public void setMp_drainage_uv(Long mp_drainage_uv) {
        this.mp_drainage_uv = mp_drainage_uv;
    }

    public Long getMp_show_pv() {
        return mp_show_pv;
    }

    public void setMp_show_pv(Long mp_show_pv) {
        this.mp_show_pv = mp_show_pv;
    }

    public Long getMp_show_uv() {
        return mp_show_uv;
    }

    public void setMp_show_uv(Long mp_show_uv) {
        this.mp_show_uv = mp_show_uv;
    }

    public Long getOrder_once_price() {
        return order_once_price;
    }

    public void setOrder_once_price(Long order_once_price) {
        this.order_once_price = order_once_price;
    }

    public Long getPay_customer_cnt() {
        return pay_customer_cnt;
    }

    public void setPay_customer_cnt(Long pay_customer_cnt) {
        this.pay_customer_cnt = pay_customer_cnt;
    }

    public Long getPay_order_amount() {
        return pay_order_amount;
    }

    public void setPay_order_amount(Long pay_order_amount) {
        this.pay_order_amount = pay_order_amount;
    }

    public Long getPay_order_cnt() {
        return pay_order_cnt;
    }

    public void setPay_order_cnt(Long pay_order_cnt) {
        this.pay_order_cnt = pay_order_cnt;
    }

    public Long getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(Long refund_amount) {
        this.refund_amount = refund_amount;
    }

    public Long getRefund_customer_cnt() {
        return refund_customer_cnt;
    }

    public void setRefund_customer_cnt(Long refund_customer_cnt) {
        this.refund_customer_cnt = refund_customer_cnt;
    }

    public Long getRefund_order_cnt() {
        return refund_order_cnt;
    }

    public void setRefund_order_cnt(Long refund_order_cnt) {
        this.refund_order_cnt = refund_order_cnt;
    }

    public Long getVideo_play_cnt() {
        return video_play_cnt;
    }

    public void setVideo_play_cnt(Long video_play_cnt) {
        this.video_play_cnt = video_play_cnt;
    }

    public Long getClue_cnt() {
        return clue_cnt;
    }

    public void setClue_cnt(Long clue_cnt) {
        this.clue_cnt = clue_cnt;
    }

    public Long getClue_ucnt() {
        return clue_ucnt;
    }

    public void setClue_ucnt(Long clue_ucnt) {
        this.clue_ucnt = clue_ucnt;
    }

    public Long getCreate_customer_cnt() {
        return create_customer_cnt;
    }

    public void setCreate_customer_cnt(Long create_customer_cnt) {
        this.create_customer_cnt = create_customer_cnt;
    }

    public Long getItem_uv() {
        return item_uv;
    }

    public void setItem_uv(Long item_uv) {
        this.item_uv = item_uv;
    }

    public Long getItem_vv() {
        return item_vv;
    }

    public void setItem_vv(Long item_vv) {
        this.item_vv = item_vv;
    }

    public Long getValid_clue_cnt() {
        return valid_clue_cnt;
    }

    public void setValid_clue_cnt(Long valid_clue_cnt) {
        this.valid_clue_cnt = valid_clue_cnt;
    }

    public Long getValid_clue_ucnt() {
        return valid_clue_ucnt;
    }

    public void setValid_clue_ucnt(Long valid_clue_ucnt) {
        this.valid_clue_ucnt = valid_clue_ucnt;
    }
}
