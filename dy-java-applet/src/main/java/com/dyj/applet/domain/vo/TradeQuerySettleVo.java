package com.dyj.applet.domain.vo;

public class TradeQuerySettleVo {

    /**
     * 开发者侧交易订单 id，长度 <= 64 字节，由数字、ASCII 字符组成
     */
    private String out_order_no;

    /**
     * 开发者侧分账单 id，长度 <= 64字节，由数字、ASCII 字符组成
     */
    private String out_settle_no;

    /**
     * 抖音开平侧交易订单 id，长度 <= 64 字节，由数字、ASCII 字符组成
     */
    private String order_id;

    /**
     * 抖音开平侧分账单id，长度 <= 64 字节，由数字、ASCII 字符组成
     */
    private String settle_id;

    /**
     * 抖音开平侧item单 id，长度 <= 64 字节，由数字、ASCII 字符组成，按券分账时该字段不为空
     */
    private String item_order_id;

    /**
     * 分账金额，单位分
     */
    private Long settle_amount;

    /**
     *
     * 分账状态：
     * INIT：初始化
     * PROCESSING：处理中
     * SUCCESS：处理成功
     * FAIL：处理失败
     */
    private String settle_status;

    /**
     * 分账详情
     */
    private String settle_detail;

    /**
     * 分账时间，13 位时间戳，单位毫秒
     */
    private Long settle_time;

    /**
     * 手续费，单位分
     */
    private Long rake;

    /**
     * 佣金，单位分
     */
    private Long commission;

    /**
     * 开发者自定义透传字段，长度 <= 2048 字节，不支持二进制数据
     */
    private String cp_extra;

    /**
     * 用于退分账场景，对应退分账接口文档中settle_no，长度<= 64字节
     */
    private String inner_settle_id;

    public String getOut_order_no() {
        return out_order_no;
    }

    public TradeQuerySettleVo setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_settle_no() {
        return out_settle_no;
    }

    public TradeQuerySettleVo setOut_settle_no(String out_settle_no) {
        this.out_settle_no = out_settle_no;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public TradeQuerySettleVo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getSettle_id() {
        return settle_id;
    }

    public TradeQuerySettleVo setSettle_id(String settle_id) {
        this.settle_id = settle_id;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public TradeQuerySettleVo setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public Long getSettle_amount() {
        return settle_amount;
    }

    public TradeQuerySettleVo setSettle_amount(Long settle_amount) {
        this.settle_amount = settle_amount;
        return this;
    }

    public String getSettle_status() {
        return settle_status;
    }

    public TradeQuerySettleVo setSettle_status(String settle_status) {
        this.settle_status = settle_status;
        return this;
    }

    public String getSettle_detail() {
        return settle_detail;
    }

    public TradeQuerySettleVo setSettle_detail(String settle_detail) {
        this.settle_detail = settle_detail;
        return this;
    }

    public Long getSettle_time() {
        return settle_time;
    }

    public TradeQuerySettleVo setSettle_time(Long settle_time) {
        this.settle_time = settle_time;
        return this;
    }

    public Long getRake() {
        return rake;
    }

    public TradeQuerySettleVo setRake(Long rake) {
        this.rake = rake;
        return this;
    }

    public Long getCommission() {
        return commission;
    }

    public TradeQuerySettleVo setCommission(Long commission) {
        this.commission = commission;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public TradeQuerySettleVo setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public String getInner_settle_id() {
        return inner_settle_id;
    }

    public TradeQuerySettleVo setInner_settle_id(String inner_settle_id) {
        this.inner_settle_id = inner_settle_id;
        return this;
    }
}
