package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-06-12 14:02
 **/
public class BinDoudianAccountVo {

    /**
     * 抖店账号绑定状态
     * 1: 开发者账号未注册抖店账号
     * 2: 绑定失败，请重试
     * 3: 绑定成功
     */
    private Integer doudian_account_status;

    /**
     * 抖店开放平台注册地址
     * 在未注册抖店开放平台账号的时候返回
     */
    private String doudian_register_url;

    /**
     * 抖店账号
     */
    private String account;

    /**
     * 抖店用户名
     */
    private String user_name;
    /**
     * 抖店主体
     */
    private String subject;

    public Integer getDoudian_account_status() {
        return doudian_account_status;
    }

    public void setDoudian_account_status(Integer doudian_account_status) {
        this.doudian_account_status = doudian_account_status;
    }

    public String getDoudian_register_url() {
        return doudian_register_url;
    }

    public void setDoudian_register_url(String doudian_register_url) {
        this.doudian_register_url = doudian_register_url;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
