package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisComponentWithDetailLiveData;
import com.dyj.applet.domain.AnalysisComponentWithDetailVideoData;

public class AnalysisComponentWithDetailVo {

    private AnalysisComponentWithDetailLiveData LiveData;
    private AnalysisComponentWithDetailVideoData VideoData;

    public AnalysisComponentWithDetailLiveData getLiveData() {
        return LiveData;
    }

    public void setLiveData(AnalysisComponentWithDetailLiveData liveData) {
        LiveData = liveData;
    }

    public AnalysisComponentWithDetailVideoData getVideoData() {
        return VideoData;
    }

    public void setVideoData(AnalysisComponentWithDetailVideoData videoData) {
        VideoData = videoData;
    }
}
