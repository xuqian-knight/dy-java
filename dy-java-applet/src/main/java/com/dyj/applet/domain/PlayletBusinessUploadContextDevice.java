package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-21 10:24
 **/
public class PlayletBusinessUploadContextDevice {

    /**
     * 用户在当前小程序对应的openid
     */
    private String open_id;

    public PlayletBusinessUploadContextDevice(String openId) {
        this.open_id = openId;
    }

    public String getOpen_id() {
        return open_id;
    }

    public void setOpen_id(String open_id) {
        this.open_id = open_id;
    }
}
