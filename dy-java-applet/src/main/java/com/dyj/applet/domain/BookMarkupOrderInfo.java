package com.dyj.applet.domain;

public class BookMarkupOrderInfo {

    /**
     * 加价单id，抖音开平侧生成的订单号
     */
    private String markup_order_id;

    /**
     * 唤起收银台的支付订单号
     */
    private String pay_order_id;

    /**
     * 唤起收银台的token
     */
    private String pay_order_token;

    public String getMarkup_order_id() {
        return markup_order_id;
    }

    public BookMarkupOrderInfo setMarkup_order_id(String markup_order_id) {
        this.markup_order_id = markup_order_id;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public BookMarkupOrderInfo setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public String getPay_order_token() {
        return pay_order_token;
    }

    public BookMarkupOrderInfo setPay_order_token(String pay_order_token) {
        this.pay_order_token = pay_order_token;
        return this;
    }
}
