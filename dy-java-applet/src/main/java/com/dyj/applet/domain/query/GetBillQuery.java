package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 获取交易账单请求值
 */
public class GetBillQuery extends BaseQuery {


    /**
     * <p>和商户号绑定的 app_id</p>
     */
    private String app_id;
    /**
     * <p>交易完成时间，格式：yyyyMMdd/yyyyMM</p>
     */
    private String bill_date;
    /**
     * <p>账单类型</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">payment:支付账单</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">settle:分账账单</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">refund:退款账单</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">return:退分账账单</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">withdraw:提现账单</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">rebate:返佣账单</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">annual_rebate:年框返佣账单</li></ul>
     */
    private String bill_type;
    /**
     * 商户号
     */
    private String merchant_id;

    public String getApp_id() {
        return app_id;
    }

    public GetBillQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getBill_date() {
        return bill_date;
    }

    public GetBillQuery setBill_date(String bill_date) {
        this.bill_date = bill_date;
        return this;
    }

    public String getBill_type() {
        return bill_type;
    }

    public GetBillQuery setBill_type(String bill_type) {
        this.bill_type = bill_type;
        return this;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public GetBillQuery setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
        return this;
    }

    public static GetBillQueryBuilder builder() {
        return new GetBillQueryBuilder();
    }

    public static final class GetBillQueryBuilder {
        private String app_id;
        private String bill_date;
        private String bill_type;
        private String merchant_id;
        private Integer tenantId;
        private String clientKey;

        private GetBillQueryBuilder() {
        }

        public static GetBillQueryBuilder aGetBillQuery() {
            return new GetBillQueryBuilder();
        }

        public GetBillQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public GetBillQueryBuilder billDate(String billDate) {
            this.bill_date = billDate;
            return this;
        }

        public GetBillQueryBuilder billType(String billType) {
            this.bill_type = billType;
            return this;
        }

        public GetBillQueryBuilder merchantId(String merchantId) {
            this.merchant_id = merchantId;
            return this;
        }

        public GetBillQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public GetBillQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public GetBillQuery build() {
            GetBillQuery getBillQuery = new GetBillQuery();
            getBillQuery.setApp_id(app_id);
            getBillQuery.setBill_date(bill_date);
            getBillQuery.setBill_type(bill_type);
            getBillQuery.setMerchant_id(merchant_id);
            getBillQuery.setTenantId(tenantId);
            getBillQuery.setClientKey(clientKey);
            return getBillQuery;
        }
    }
}
