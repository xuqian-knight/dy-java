package com.dyj.applet.domain.vo;

import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

public class ShortLiveIdWithAwemeIdVo {

    //发布的短视频数据(仅支持自挂载查询，达人挂载数据暂不支持)
    // key:抖音号  value:该抖音号对应的短视频ID列表
    private Map<String, List<String>> item_data;

    //发布的直播间数据(仅支持自挂载查询，达人挂载数据暂不支持)
    private JSONObject live_data;

    //总览数据(自挂载，达人挂载，UGC挂载均可获取)
    private OverViewData  over_view_data;

    public Map<String, List<String>> getItem_data() {
        return item_data;
    }

    public void setItem_data(Map<String, List<String>> item_data) {
        this.item_data = item_data;
    }

    public JSONObject getLive_data() {
        return live_data;
    }

    public void setLive_data(JSONObject live_data) {
        this.live_data = live_data;
    }

    public OverViewData getOver_view_data() {
        return over_view_data;
    }

    public void setOver_view_data(OverViewData over_view_data) {
        this.over_view_data = over_view_data;
    }

    public static class OverViewData{
        //有播放视频数
        private Integer item_watched_cnt;
        //短视频投稿数
        private Integer item_contribute_cnt;

        public Integer getItem_watched_cnt() {
            return item_watched_cnt;
        }

        public void setItem_watched_cnt(Integer item_watched_cnt) {
            this.item_watched_cnt = item_watched_cnt;
        }

        public Integer getItem_contribute_cnt() {
            return item_contribute_cnt;
        }

        public void setItem_contribute_cnt(Integer item_contribute_cnt) {
            this.item_contribute_cnt = item_contribute_cnt;
        }
    }
}
