package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

public class QueryUserCertificatesQuery extends UserInfoQuery {

    /**
     * 商家团购账号id（抖音来客账号id）。需要先有此商家授权关系才能获取到。
     */
    private String account_id;

    /**
     * 适用门店poi_id
     * 选填
     */
    private String poi_id;

    /**
     * 分页展示用户券起始页，默认1
     * 选填
     */
    private Integer page;

    /**
     *
     * 分页查询订单数量 page_size <= 50，默认50
     * 选填
     */
    private Integer page_size;

    /**
     * 1： 核销工具(默认)； 2：混合双开
     * 选填
     */
    private Integer biz_type;

    public String getAccount_id() {
        return account_id;
    }

    public QueryUserCertificatesQuery setAccount_id(String account_id) {
        this.account_id = account_id;
        return this;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public QueryUserCertificatesQuery setPoi_id(String poi_id) {
        this.poi_id = poi_id;
        return this;
    }

    public Integer getPage() {
        return page;
    }

    public QueryUserCertificatesQuery setPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getPage_size() {
        return page_size;
    }

    public QueryUserCertificatesQuery setPage_size(Integer page_size) {
        this.page_size = page_size;
        return this;
    }

    public Integer getBiz_type() {
        return biz_type;
    }

    public QueryUserCertificatesQuery setBiz_type(Integer biz_type) {
        this.biz_type = biz_type;
        return this;
    }

    public static QueryUserCertificatesQueryBuilder builder() {
        return new QueryUserCertificatesQueryBuilder();
    }

    public static final class QueryUserCertificatesQueryBuilder {
        private String account_id;
        private String poi_id;
        private Integer page;
        private Integer page_size;
        private Integer biz_type;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private QueryUserCertificatesQueryBuilder() {
        }

        public QueryUserCertificatesQueryBuilder accountId(String accountId) {
            this.account_id = accountId;
            return this;
        }

        public QueryUserCertificatesQueryBuilder poiId(String poiId) {
            this.poi_id = poiId;
            return this;
        }

        public QueryUserCertificatesQueryBuilder page(Integer page) {
            this.page = page;
            return this;
        }

        public QueryUserCertificatesQueryBuilder pageSize(Integer pageSize) {
            this.page_size = pageSize;
            return this;
        }

        public QueryUserCertificatesQueryBuilder bizType(Integer bizType) {
            this.biz_type = bizType;
            return this;
        }

        public QueryUserCertificatesQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public QueryUserCertificatesQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryUserCertificatesQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryUserCertificatesQuery build() {
            QueryUserCertificatesQuery queryUserCertificatesQuery = new QueryUserCertificatesQuery();
            queryUserCertificatesQuery.setAccount_id(account_id);
            queryUserCertificatesQuery.setPoi_id(poi_id);
            queryUserCertificatesQuery.setPage(page);
            queryUserCertificatesQuery.setPage_size(page_size);
            queryUserCertificatesQuery.setBiz_type(biz_type);
            queryUserCertificatesQuery.setOpen_id(open_id);
            queryUserCertificatesQuery.setTenantId(tenantId);
            queryUserCertificatesQuery.setClientKey(clientKey);
            return queryUserCertificatesQuery;
        }
    }
}
