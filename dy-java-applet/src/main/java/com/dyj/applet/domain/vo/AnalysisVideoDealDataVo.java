package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisVideoDealOverviewData;

import java.util.List;

public class AnalysisVideoDealDataVo {

    private AnalysisVideoDealOverviewData video_deal_overview_data;

    private List<AnalysisVideoDealOverviewData> video_deal_data_list;

    public AnalysisVideoDealOverviewData getVideo_deal_overview_data() {
        return video_deal_overview_data;
    }

    public void setVideo_deal_overview_data(AnalysisVideoDealOverviewData video_deal_overview_data) {
        this.video_deal_overview_data = video_deal_overview_data;
    }

    public List<AnalysisVideoDealOverviewData> getVideo_deal_data_list() {
        return video_deal_data_list;
    }

    public void setVideo_deal_data_list(List<AnalysisVideoDealOverviewData> video_deal_data_list) {
        this.video_deal_data_list = video_deal_data_list;
    }
}
