package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-07-17 10:17
 **/
public class AwemeVideoKeyword {

    /**
     * 关键词id
     */
    private String keyword;
    /**
     * 关键词id
     */
    private String keyword_id;
    /**
     * 审核拒绝原因列表
     */
    private List<String> reject_reason_list;
    /**
     * 关键词状态 0:审核中 1:审核通过 2:审核拒绝
     */
    private Integer status;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword_id() {
        return keyword_id;
    }

    public void setKeyword_id(String keyword_id) {
        this.keyword_id = keyword_id;
    }

    public List<String> getReject_reason_list() {
        return reject_reason_list;
    }

    public void setReject_reason_list(List<String> reject_reason_list) {
        this.reject_reason_list = reject_reason_list;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
