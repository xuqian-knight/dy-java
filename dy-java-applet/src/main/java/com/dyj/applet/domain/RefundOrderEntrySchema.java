package com.dyj.applet.domain;

/**
 * <p>退款单的跳转的schema，参考<a href="/docs/resource/zh-CN/mini-app/develop/server/trade-system/general/common-param" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">通用参数-关于 xxx_entry_schema 的前置说明</a></p>
 * @author ws
 **/
public class RefundOrderEntrySchema {

    /**
     * <p>xx情页路径参数，自定义的json结构，<strong>内部为k-v结构</strong>，序列化成字符串存入该字段，平台不限制，但是写入的内容需要能够保证生成访问xx详情的schema能正确跳转到小程序内部的xx详情页，<strong>长度须&lt;=512byte</strong>，<strong>params内key不可重复</strong>。</p> 选填
     */
    private String params;
    /**
     * <p>小程序xxx详情页跳转路径，<strong>没有前导的“/”</strong>，<strong>路径后不可携带query参数</strong>，路<strong>径中不可携带『？: &amp; *』等特殊字符，路径只可以是『英文字符、数字、_、/ 』等组成，长度&lt;=512byte</strong></p>
     */
    private String path;

    public String getParams() {
        return params;
    }

    public RefundOrderEntrySchema setParams(String params) {
        this.params = params;
        return this;
    }

    public String getPath() {
        return path;
    }

    public RefundOrderEntrySchema setPath(String path) {
        this.path = path;
        return this;
    }
}
