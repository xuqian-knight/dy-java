package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-24 14:18
 **/
public class AptUserShare {

    /**
     * 新增分享
     */
    private Long new_share;

    /**
     * yyyy-MM-dd日期
     */
    private String date;

    public Long getNew_share() {
        return new_share;
    }

    public void setNew_share(Long new_share) {
        this.new_share = new_share;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
