package com.dyj.applet.domain;

/**
 * 商品 item_order 详细信息
 */
public class IndustryOrderItemOrderDetail {

    /**
     * item 单 id
     */
    private String item_order_id;

    /**
     * 商品优惠后价格
     */
    private Long price;

    public String getItem_order_id() {
        return item_order_id;
    }

    public IndustryOrderItemOrderDetail setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public Long getPrice() {
        return price;
    }

    public IndustryOrderItemOrderDetail setPrice(Long price) {
        this.price = price;
        return this;
    }
}
