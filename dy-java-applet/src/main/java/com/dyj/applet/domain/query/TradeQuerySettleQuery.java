package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class TradeQuerySettleQuery extends BaseQuery {

    /**
     * 抖音开平侧订单 id，长度 <= 64字节
     */
    private String order_id;
    /**
     * 开发者侧订单 id，长度 <= 64 字节
     */
    private String out_order_no;
    /**
     * 开发者侧分账单 id，长度 <= 64字节
     */
    private String out_settle_no;
    /**
     * 抖音开平侧分账单 id，长度 <= 64字节
     */
    private String settle_id;

    public String getOrder_id() {
        return order_id;
    }

    public TradeQuerySettleQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public TradeQuerySettleQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_settle_no() {
        return out_settle_no;
    }

    public TradeQuerySettleQuery setOut_settle_no(String out_settle_no) {
        this.out_settle_no = out_settle_no;
        return this;
    }

    public String getSettle_id() {
        return settle_id;
    }

    public TradeQuerySettleQuery setSettle_id(String settle_id) {
        this.settle_id = settle_id;
        return this;
    }

    public static TradeQuerySettleQueryBuilder builder() {
        return new TradeQuerySettleQueryBuilder();
    }

    public static final class TradeQuerySettleQueryBuilder {
        private String order_id;
        private String out_order_no;
        private String out_settle_no;
        private String settle_id;
        private Integer tenantId;
        private String clientKey;

        private TradeQuerySettleQueryBuilder() {
        }

        public TradeQuerySettleQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public TradeQuerySettleQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public TradeQuerySettleQueryBuilder outSettleNo(String outSettleNo) {
            this.out_settle_no = outSettleNo;
            return this;
        }

        public TradeQuerySettleQueryBuilder settleId(String settleId) {
            this.settle_id = settleId;
            return this;
        }

        public TradeQuerySettleQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public TradeQuerySettleQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public TradeQuerySettleQuery build() {
            TradeQuerySettleQuery tradeQuerySettleQuery = new TradeQuerySettleQuery();
            tradeQuerySettleQuery.setOrder_id(order_id);
            tradeQuerySettleQuery.setOut_order_no(out_order_no);
            tradeQuerySettleQuery.setOut_settle_no(out_settle_no);
            tradeQuerySettleQuery.setSettle_id(settle_id);
            tradeQuerySettleQuery.setTenantId(tenantId);
            tradeQuerySettleQuery.setClientKey(clientKey);
            return tradeQuerySettleQuery;
        }
    }
}
