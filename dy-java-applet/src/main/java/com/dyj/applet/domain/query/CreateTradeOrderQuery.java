package com.dyj.applet.domain.query;

import com.dyj.applet.domain.IndustryOrderOrderEntrySchema;
import com.dyj.applet.domain.TradeOrderGoodsInfo;
import com.dyj.applet.domain.vo.PriceCalculationDetail;
import com.dyj.common.domain.query.UserInfoQuery;

import java.util.List;

/**
 * 行业交易系统->预下单->开发者发起下单
 */
public class CreateTradeOrderQuery extends UserInfoQuery {

    /**
     * 商品信息
     */
    private List<TradeOrderGoodsInfo> goods_list;

    /**
     * 订单总价，单位分
     */
    private Long total_amount;

    /**
     * 用户手机号，长度 <= 128 byte
     */
    private String phone_num;

    /**
     * 用户姓名，长度 <= 64 byte
     */
    private String contact_name;

    /**
     * 下单备注信息，长度 <= 2048byte
     */
    private String extra;

    /**
     * 支付结果通知地址，必须是 HTTPS 类型。
     * 若不填，默认使用在行业模板配置-消息通知的支付结果通知地址。
     */
    private String pay_notify_url;

    /**
     * 开发者的单号，长度 <= 64 byte
     */
    private String out_order_no;

    /**
     * 支付超时时间，单位秒，例如 300 表示 300 秒后过期；不传或传 0 会使用默认值 300。
     */
    private Long pay_expire_seconds;

    /**
     * 订单详情页信息
     */
    private IndustryOrderOrderEntrySchema order_entry_schema;

    /**
     * 开发者自定义透传字段，不支持二进制，长度 <= 2048 byte
     */
    private String cp_extra;

    /**
     * 折扣金额，单位分
     */
    private Long discount_amount;

    /**
     * 营销算价结果信息
     * 选填
     */
    private PriceCalculationDetail price_calculation_detail;

    public List<TradeOrderGoodsInfo> getGoods_list() {
        return goods_list;
    }

    public CreateTradeOrderQuery setGoods_list(List<TradeOrderGoodsInfo> goods_list) {
        this.goods_list = goods_list;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public CreateTradeOrderQuery setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public CreateTradeOrderQuery setPhone_num(String phone_num) {
        this.phone_num = phone_num;
        return this;
    }

    public String getContact_name() {
        return contact_name;
    }

    public CreateTradeOrderQuery setContact_name(String contact_name) {
        this.contact_name = contact_name;
        return this;
    }

    public String getExtra() {
        return extra;
    }

    public CreateTradeOrderQuery setExtra(String extra) {
        this.extra = extra;
        return this;
    }

    public String getPay_notify_url() {
        return pay_notify_url;
    }

    public CreateTradeOrderQuery setPay_notify_url(String pay_notify_url) {
        this.pay_notify_url = pay_notify_url;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public CreateTradeOrderQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public Long getPay_expire_seconds() {
        return pay_expire_seconds;
    }

    public CreateTradeOrderQuery setPay_expire_seconds(Long pay_expire_seconds) {
        this.pay_expire_seconds = pay_expire_seconds;
        return this;
    }

    public IndustryOrderOrderEntrySchema getOrder_entry_schema() {
        return order_entry_schema;
    }

    public CreateTradeOrderQuery setOrder_entry_schema(IndustryOrderOrderEntrySchema order_entry_schema) {
        this.order_entry_schema = order_entry_schema;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public CreateTradeOrderQuery setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public CreateTradeOrderQuery setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public PriceCalculationDetail getPrice_calculation_detail() {
        return price_calculation_detail;
    }

    public CreateTradeOrderQuery setPrice_calculation_detail(PriceCalculationDetail price_calculation_detail) {
        this.price_calculation_detail = price_calculation_detail;
        return this;
    }

    public static CreateTradeOrderQueryBuilder builder() {
        return new CreateTradeOrderQueryBuilder();
    }

    public static final class CreateTradeOrderQueryBuilder {
        private List<TradeOrderGoodsInfo> goods_list;
        private Long total_amount;
        private String phone_num;
        private String contact_name;
        private String extra;
        private String pay_notify_url;
        private String out_order_no;
        private Long pay_expire_seconds;
        private IndustryOrderOrderEntrySchema order_entry_schema;
        private String cp_extra;
        private Long discount_amount;
        private PriceCalculationDetail price_calculation_detail;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private CreateTradeOrderQueryBuilder() {
        }

        public CreateTradeOrderQueryBuilder goodsList(List<TradeOrderGoodsInfo> goodsList) {
            this.goods_list = goodsList;
            return this;
        }

        public CreateTradeOrderQueryBuilder totalAmount(Long totalAmount) {
            this.total_amount = totalAmount;
            return this;
        }

        public CreateTradeOrderQueryBuilder phoneNum(String phoneNum) {
            this.phone_num = phoneNum;
            return this;
        }

        public CreateTradeOrderQueryBuilder contactName(String contactName) {
            this.contact_name = contactName;
            return this;
        }

        public CreateTradeOrderQueryBuilder extra(String extra) {
            this.extra = extra;
            return this;
        }

        public CreateTradeOrderQueryBuilder payNotifyUrl(String payNotifyUrl) {
            this.pay_notify_url = payNotifyUrl;
            return this;
        }

        public CreateTradeOrderQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public CreateTradeOrderQueryBuilder payExpireSeconds(Long payExpireSeconds) {
            this.pay_expire_seconds = payExpireSeconds;
            return this;
        }

        public CreateTradeOrderQueryBuilder orderEntrySchema(IndustryOrderOrderEntrySchema orderEntrySchema) {
            this.order_entry_schema = orderEntrySchema;
            return this;
        }

        public CreateTradeOrderQueryBuilder cpExtra(String cpExtra) {
            this.cp_extra = cpExtra;
            return this;
        }

        public CreateTradeOrderQueryBuilder discountAmount(Long discountAmount) {
            this.discount_amount = discountAmount;
            return this;
        }

        public CreateTradeOrderQueryBuilder priceCalculationDetail(PriceCalculationDetail priceCalculationDetail) {
            this.price_calculation_detail = priceCalculationDetail;
            return this;
        }

        public CreateTradeOrderQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public CreateTradeOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateTradeOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateTradeOrderQuery build() {
            CreateTradeOrderQuery createTradeOrderQuery = new CreateTradeOrderQuery();
            createTradeOrderQuery.setGoods_list(goods_list);
            createTradeOrderQuery.setTotal_amount(total_amount);
            createTradeOrderQuery.setPhone_num(phone_num);
            createTradeOrderQuery.setContact_name(contact_name);
            createTradeOrderQuery.setExtra(extra);
            createTradeOrderQuery.setPay_notify_url(pay_notify_url);
            createTradeOrderQuery.setOut_order_no(out_order_no);
            createTradeOrderQuery.setPay_expire_seconds(pay_expire_seconds);
            createTradeOrderQuery.setOrder_entry_schema(order_entry_schema);
            createTradeOrderQuery.setCp_extra(cp_extra);
            createTradeOrderQuery.setDiscount_amount(discount_amount);
            createTradeOrderQuery.setPrice_calculation_detail(price_calculation_detail);
            createTradeOrderQuery.setOpen_id(open_id);
            createTradeOrderQuery.setTenantId(tenantId);
            createTradeOrderQuery.setClientKey(clientKey);
            return createTradeOrderQuery;
        }
    }
}
