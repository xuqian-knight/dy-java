package com.dyj.applet.domain;

/**
 * 商户管理员信息
 */
public class CreateSubMerchantMerchantOperationInfo {

    /**
     * <p>支付宝账号 入驻支付宝必填</p> 选填
     */
    private String alipay_account_no;
    /**
     * <p>当所选渠道包含微信，且manage_person_type为2非法人的条件下必填</p><p><a href="https://kf.qq.com/faq/220509Y3Yvym220509fQvYR7.html" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">示例</a></p> 选填
     */
    private String business_authorization_letter;
    /**
     * <p>在manage_person_type为2非法人以及IdType 为身份证的条件下必填证件照国徽面电子版，图片id信息通过图片上传接口获取</p> 选填
     */
    private String id_back_pic_url;
    /**
     * <p>在manage_person_type为2非法人条件下必填 证件有效期开始时间，格式为yyyyMMdd</p> 选填
     */
    private String id_begin_date;
    /**
     * <p>在manage_person_type为2非法人条件下必填 证件有效期过期时间，格式为yyyyMMdd, 长期 99991231</p> 选填
     */
    private String id_exp_date;
    /**
     * <p>在manage_person_type为2非法人条件下必填证件照人脸面电子版，图片id信息通过图片上传接口获取</p> 选填
     */
    private String id_front_pic_url;
    /**
     * <p>在manage_person_type为2非法人条件下必填</p><p>证件类型枚举值:</p><p>1: 身份证</p><p>2: 户口本</p><p>3: 护照</p><p>4: 军官证</p><p>5: 士兵证</p><p>6: 香港居民来往内地通行证</p><p>7: 台湾同胞来往内地通行证</p><p>8: 临时身份证</p><p>9: 外国人居留证</p><p>10: 警官证</p><p>11: 澳门同胞来往内地通行证</p><p>12: 港澳居住证</p><p>13: 台湾居住证</p><p>100: 其他证件类型（上面枚举没有符合的）</p> 选填
     */
    private Integer id_type;
    /**
     * <p>联系人邮箱</p>
     */
    private String manage_email;
    /**
     * <p>身份证号</p>
     */
    private String manage_id_no;
    /**
     * <p>联系人手机号</p>
     */
    private String manage_mobile;
    /**
     * 联系人名称
     */
    private String manage_name;
    /**
     * <p>联系人类型枚举值：</p><p>1: 法人</p><p>2: 非法人</p>
     */
    private Integer manage_person_type;
    /**
     * <p>店铺名称 入驻支付宝、微信必填</p> 选填
     */
    private String shop_name;
    /**
     * <p>店铺链接 入驻支付宝、微信必填</p> 选填
     */
    private String shop_url;

    public String getAlipay_account_no() {
        return alipay_account_no;
    }

    public CreateSubMerchantMerchantOperationInfo setAlipay_account_no(String alipay_account_no) {
        this.alipay_account_no = alipay_account_no;
        return this;
    }

    public String getBusiness_authorization_letter() {
        return business_authorization_letter;
    }

    public CreateSubMerchantMerchantOperationInfo setBusiness_authorization_letter(String business_authorization_letter) {
        this.business_authorization_letter = business_authorization_letter;
        return this;
    }

    public String getId_back_pic_url() {
        return id_back_pic_url;
    }

    public CreateSubMerchantMerchantOperationInfo setId_back_pic_url(String id_back_pic_url) {
        this.id_back_pic_url = id_back_pic_url;
        return this;
    }

    public String getId_begin_date() {
        return id_begin_date;
    }

    public CreateSubMerchantMerchantOperationInfo setId_begin_date(String id_begin_date) {
        this.id_begin_date = id_begin_date;
        return this;
    }

    public String getId_exp_date() {
        return id_exp_date;
    }

    public CreateSubMerchantMerchantOperationInfo setId_exp_date(String id_exp_date) {
        this.id_exp_date = id_exp_date;
        return this;
    }

    public String getId_front_pic_url() {
        return id_front_pic_url;
    }

    public CreateSubMerchantMerchantOperationInfo setId_front_pic_url(String id_front_pic_url) {
        this.id_front_pic_url = id_front_pic_url;
        return this;
    }

    public Integer getId_type() {
        return id_type;
    }

    public CreateSubMerchantMerchantOperationInfo setId_type(Integer id_type) {
        this.id_type = id_type;
        return this;
    }

    public String getManage_email() {
        return manage_email;
    }

    public CreateSubMerchantMerchantOperationInfo setManage_email(String manage_email) {
        this.manage_email = manage_email;
        return this;
    }

    public String getManage_id_no() {
        return manage_id_no;
    }

    public CreateSubMerchantMerchantOperationInfo setManage_id_no(String manage_id_no) {
        this.manage_id_no = manage_id_no;
        return this;
    }

    public String getManage_mobile() {
        return manage_mobile;
    }

    public CreateSubMerchantMerchantOperationInfo setManage_mobile(String manage_mobile) {
        this.manage_mobile = manage_mobile;
        return this;
    }

    public String getManage_name() {
        return manage_name;
    }

    public CreateSubMerchantMerchantOperationInfo setManage_name(String manage_name) {
        this.manage_name = manage_name;
        return this;
    }

    public Integer getManage_person_type() {
        return manage_person_type;
    }

    public CreateSubMerchantMerchantOperationInfo setManage_person_type(Integer manage_person_type) {
        this.manage_person_type = manage_person_type;
        return this;
    }

    public String getShop_name() {
        return shop_name;
    }

    public CreateSubMerchantMerchantOperationInfo setShop_name(String shop_name) {
        this.shop_name = shop_name;
        return this;
    }

    public String getShop_url() {
        return shop_url;
    }

    public CreateSubMerchantMerchantOperationInfo setShop_url(String shop_url) {
        this.shop_url = shop_url;
        return this;
    }


    public static final class CreateSubMerchantMerchantOperationInfoBuilder {
        private String alipay_account_no;
        private String business_authorization_letter;
        private String id_back_pic_url;
        private String id_begin_date;
        private String id_exp_date;
        private String id_front_pic_url;
        private Integer id_type;
        private String manage_email;
        private String manage_id_no;
        private String manage_mobile;
        private String manage_name;
        private Integer manage_person_type;
        private String shop_name;
        private String shop_url;

        private CreateSubMerchantMerchantOperationInfoBuilder() {
        }

        public CreateSubMerchantMerchantOperationInfoBuilder alipayAccountNo(String alipayAccountNo) {
            this.alipay_account_no = alipayAccountNo;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder businessAuthorizationLetter(String businessAuthorizationLetter) {
            this.business_authorization_letter = businessAuthorizationLetter;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder idBackPicUrl(String idBackPicUrl) {
            this.id_back_pic_url = idBackPicUrl;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder idBeginDate(String idBeginDate) {
            this.id_begin_date = idBeginDate;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder idExpDate(String idExpDate) {
            this.id_exp_date = idExpDate;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder idFrontPicUrl(String idFrontPicUrl) {
            this.id_front_pic_url = idFrontPicUrl;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder idType(Integer idType) {
            this.id_type = idType;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder manageEmail(String manageEmail) {
            this.manage_email = manageEmail;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder manageIdNo(String manageIdNo) {
            this.manage_id_no = manageIdNo;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder manageMobile(String manageMobile) {
            this.manage_mobile = manageMobile;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder manageName(String manageName) {
            this.manage_name = manageName;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder managePersonType(Integer managePersonType) {
            this.manage_person_type = managePersonType;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder shopName(String shopName) {
            this.shop_name = shopName;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfoBuilder shopUrl(String shopUrl) {
            this.shop_url = shopUrl;
            return this;
        }

        public CreateSubMerchantMerchantOperationInfo build() {
            CreateSubMerchantMerchantOperationInfo createSubMerchantMerchantOperationInfo = new CreateSubMerchantMerchantOperationInfo();
            createSubMerchantMerchantOperationInfo.setAlipay_account_no(alipay_account_no);
            createSubMerchantMerchantOperationInfo.setBusiness_authorization_letter(business_authorization_letter);
            createSubMerchantMerchantOperationInfo.setId_back_pic_url(id_back_pic_url);
            createSubMerchantMerchantOperationInfo.setId_begin_date(id_begin_date);
            createSubMerchantMerchantOperationInfo.setId_exp_date(id_exp_date);
            createSubMerchantMerchantOperationInfo.setId_front_pic_url(id_front_pic_url);
            createSubMerchantMerchantOperationInfo.setId_type(id_type);
            createSubMerchantMerchantOperationInfo.setManage_email(manage_email);
            createSubMerchantMerchantOperationInfo.setManage_id_no(manage_id_no);
            createSubMerchantMerchantOperationInfo.setManage_mobile(manage_mobile);
            createSubMerchantMerchantOperationInfo.setManage_name(manage_name);
            createSubMerchantMerchantOperationInfo.setManage_person_type(manage_person_type);
            createSubMerchantMerchantOperationInfo.setShop_name(shop_name);
            createSubMerchantMerchantOperationInfo.setShop_url(shop_url);
            return createSubMerchantMerchantOperationInfo;
        }
    }
}
