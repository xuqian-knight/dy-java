package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-28 16:22
 **/
public class SupplierSubmitTaskVo extends BaseVo {

    /**
     * 任务提交状态
     * 1 - 成功
     * 2 - 失败
     */
    private Integer is_success;
    /**
     * 匹配任务 id，咨询问题时需提供
     */
    private String task_id;


    public Integer getIs_success() {
        return is_success;
    }

    public void setIs_success(Integer is_success) {
        this.is_success = is_success;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }
}
