package com.dyj.applet.domain;

/**
 * 退款审核信息
 */
public class RefundMerchantAuditDetail {

    /**
     * <p>退款审核状态：</p><ul><li>INIT：初始化</li><li>TOAUDIT：待审核</li><li>AGREE：同意</li><li>DENY：拒绝</li><li>OVERTIME：超时未审核自动同意</li></ul>
     */
    private String audit_status;
    /**
     * 不同意退款信息，长度 <= 512 byte 选填
     */
    private String deny_message;
    /**
     * <p>是否需要退款审核，</p><ul><li>1-需要审核</li><li>2-不需要审核</li></ul>
     */
    private Long need_refund_audit;
    /**
     * <p>退款审核的最后期限，过期无需审核，自动退款，13 位 unix 时间戳，精度：毫秒</p>
     */
    private Long refund_audit_deadline;

    public String getAudit_status() {
        return audit_status;
    }

    public RefundMerchantAuditDetail setAudit_status(String audit_status) {
        this.audit_status = audit_status;
        return this;
    }

    public String getDeny_message() {
        return deny_message;
    }

    public RefundMerchantAuditDetail setDeny_message(String deny_message) {
        this.deny_message = deny_message;
        return this;
    }

    public Long getNeed_refund_audit() {
        return need_refund_audit;
    }

    public RefundMerchantAuditDetail setNeed_refund_audit(Long need_refund_audit) {
        this.need_refund_audit = need_refund_audit;
        return this;
    }

    public Long getRefund_audit_deadline() {
        return refund_audit_deadline;
    }

    public RefundMerchantAuditDetail setRefund_audit_deadline(Long refund_audit_deadline) {
        this.refund_audit_deadline = refund_audit_deadline;
        return this;
    }
}
