package com.dyj.applet.domain;

import java.util.Map;

/**
 * @author danmo
 * @date 2024-04-11 14:12
 **/
public class UserTaskVideoInfo {

    /**
     * 视频真实id
     */
    private String video_id;


    /**
     * 视频状态。2:不适宜公开;4:审核中;5:公开视频
     */
    private Integer video_status;

    /**
     * 任务是否完成
     */
    private Integer completed;

    /**
     * 互动数据
     */
    private Map<String,InteractInfo> interact_info;

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public Integer getVideo_status() {
        return video_status;
    }

    public void setVideo_status(Integer video_status) {
        this.video_status = video_status;
    }

    public Integer getCompleted() {
        return completed;
    }

    public void setCompleted(Integer completed) {
        this.completed = completed;
    }

    public Map<String, InteractInfo> getInteract_info() {
        return interact_info;
    }

    public void setInteract_info(Map<String, InteractInfo> interact_info) {
        this.interact_info = interact_info;
    }
}
