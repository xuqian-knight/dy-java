package com.dyj.applet.domain.vo;

public class CreateSettleV2Vo {

    /**
     * 用于退分账场景，对应退分账接口文档中settle_no，长度<= 64字节
     */
    private String inner_settle_id;
    /**
     * 抖音开平侧分账 id，长度 <= 64 个字节
     */
    private String settle_id;

    public String getInner_settle_id() {
        return inner_settle_id;
    }

    public CreateSettleV2Vo setInner_settle_id(String inner_settle_id) {
        this.inner_settle_id = inner_settle_id;
        return this;
    }

    public String getSettle_id() {
        return settle_id;
    }

    public CreateSettleV2Vo setSettle_id(String settle_id) {
        this.settle_id = settle_id;
        return this;
    }
}
