package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AptUserItem;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-24 13:59
 **/
public class AptUserItemVo extends BaseVo {

    private List<AptUserItem> result_list;

    public List<AptUserItem> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<AptUserItem> result_list) {
        this.result_list = result_list;
    }
}
