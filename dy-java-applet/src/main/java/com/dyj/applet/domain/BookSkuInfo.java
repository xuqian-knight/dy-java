package com.dyj.applet.domain;

/**
 * 预约的商品SKU信息，需要加价时必填，详见BookSkuInfo
 */
public class BookSkuInfo {

    /**
     * sku金额
     * •加价的商品价格不能大于预售单的商品价格两倍
     * 选填
     */
    private Long price;

    /**
     * 预约的商品sku_id
     */
    private String sku_id;

    /**
     * sku_id类型 1-商品库skuId 2-非商品库skuId
     */
    private Integer sku_id_type;

    public Long getPrice() {
        return price;
    }

    public BookSkuInfo setPrice(Long price) {
        this.price = price;
        return this;
    }

    public String getSku_id() {
        return sku_id;
    }

    public BookSkuInfo setSku_id(String sku_id) {
        this.sku_id = sku_id;
        return this;
    }

    public Integer getSku_id_type() {
        return sku_id_type;
    }

    public BookSkuInfo setSku_id_type(Integer sku_id_type) {
        this.sku_id_type = sku_id_type;
        return this;
    }
}
