package com.dyj.applet.domain;

/**
 * 加价信息
 */
public class BookMarkupInfo {

    /**
     * 加价单订单总金额
     */
    private Long total_amount;

    /**
     * 支付结果通知地址，必须是https类型。若不填，默认使用在行业模板配置-消息通知页面设置的支付回调地址。加价单支付超时时间是300s。配置地址为：https://developer.open-douyin.com/microapp/${appid}/industry/template，注意将此地址中的${appid}更换为自己的appid
     * 选填
     */
    private String pay_notify_url;

    /**
     * 加价单的开发者单号，长度 <= 64 byte
     */
    private String out_markup_no;

    /**
     * 订单详情页信息，详见 OrderEntrySchema
     */
    private IndustryOrderOrderEntrySchema order_entry_schema;

    public Long getTotal_amount() {
        return total_amount;
    }

    public BookMarkupInfo setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public String getPay_notify_url() {
        return pay_notify_url;
    }

    public BookMarkupInfo setPay_notify_url(String pay_notify_url) {
        this.pay_notify_url = pay_notify_url;
        return this;
    }

    public String getOut_markup_no() {
        return out_markup_no;
    }

    public BookMarkupInfo setOut_markup_no(String out_markup_no) {
        this.out_markup_no = out_markup_no;
        return this;
    }

    public IndustryOrderOrderEntrySchema getOrder_entry_schema() {
        return order_entry_schema;
    }

    public BookMarkupInfo setOrder_entry_schema(IndustryOrderOrderEntrySchema order_entry_schema) {
        this.order_entry_schema = order_entry_schema;
        return this;
    }
}
