package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 修改券模板状态请求值
 */
public class UpdateCouponMetaStatusQuery extends BaseQuery {

    /**
     * 小程序appId
     */
    private String app_id;
    /**
     * 抖音开平券模板id
     */
    private String coupon_meta_id;
    /**
     * 期望变更的主播发券状态，1：上架，2：下架
     */
    private Integer coupon_meta_status;

    public String getApp_id() {
        return app_id;
    }

    public UpdateCouponMetaStatusQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public UpdateCouponMetaStatusQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public Integer getCoupon_meta_status() {
        return coupon_meta_status;
    }

    public UpdateCouponMetaStatusQuery setCoupon_meta_status(Integer coupon_meta_status) {
        this.coupon_meta_status = coupon_meta_status;
        return this;
    }

    public static UpdateCouponMetaStatusQueryBuilder builder() {
        return new UpdateCouponMetaStatusQueryBuilder();
    }

    public static final class UpdateCouponMetaStatusQueryBuilder {
        private String app_id;
        private String coupon_meta_id;
        private Integer coupon_meta_status;
        private Integer tenantId;
        private String clientKey;

        private UpdateCouponMetaStatusQueryBuilder() {
        }

        public UpdateCouponMetaStatusQueryBuilder appId(String app_id) {
            this.app_id = app_id;
            return this;
        }

        public UpdateCouponMetaStatusQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public UpdateCouponMetaStatusQueryBuilder couponMetaStatus(Integer couponMetaStatus) {
            this.coupon_meta_status = couponMetaStatus;
            return this;
        }

        public UpdateCouponMetaStatusQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UpdateCouponMetaStatusQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UpdateCouponMetaStatusQuery build() {
            UpdateCouponMetaStatusQuery updateCouponMetaStatusQuery = new UpdateCouponMetaStatusQuery();
            updateCouponMetaStatusQuery.setApp_id(app_id);
            updateCouponMetaStatusQuery.setCoupon_meta_id(coupon_meta_id);
            updateCouponMetaStatusQuery.setCoupon_meta_status(coupon_meta_status);
            updateCouponMetaStatusQuery.setTenantId(tenantId);
            updateCouponMetaStatusQuery.setClientKey(clientKey);
            return updateCouponMetaStatusQuery;
        }
    }
}
