package com.dyj.applet.domain;

/**
 * 订单 item单信息
 */
public class QueryTransactionOrderItemOrder {

    /**
     * <p>item单订单金额，单位分</p>
     */
    private Long item_order_amount;
    /**
     * <p>交易系统商品单号</p>
     */
    private String item_order_id;
    /**
     * <p>用户下单时传入的商品sku_id</p>
     */
    private String sku_id;

    public Long getItem_order_amount() {
        return item_order_amount;
    }

    public QueryTransactionOrderItemOrder setItem_order_amount(Long item_order_amount) {
        this.item_order_amount = item_order_amount;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public QueryTransactionOrderItemOrder setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public String getSku_id() {
        return sku_id;
    }

    public QueryTransactionOrderItemOrder setSku_id(String sku_id) {
        this.sku_id = sku_id;
        return this;
    }
}
