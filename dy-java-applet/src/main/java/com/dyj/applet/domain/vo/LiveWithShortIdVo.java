package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.LiveWithShortData;

import java.util.List;

public class LiveWithShortIdVo {

    /**
     * 总数
     */
    private Long Total;

    private List<LiveWithShortData> ShortDataList;

    public Long getTotal() {
        return Total;
    }

    public void setTotal(Long total) {
        Total = total;
    }

    public List<LiveWithShortData> getShortDataList() {
        return ShortDataList;
    }

    public void setShortDataList(List<LiveWithShortData> shortDataList) {
        ShortDataList = shortDataList;
    }
}
