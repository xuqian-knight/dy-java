package com.dyj.applet.domain.query;

import com.dyj.applet.domain.PushDeliveryItemOrderId;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * 推送核销状态请求值
 */
public class TradePushDeliveryQuery extends BaseQuery {

    /**
     * 开发者系统内的交易单号，称为外部单号。
     */
    private String out_order_no;
    /**
     * 核销的商铺 POI 信息，最多 1024 个字节。 注意：此字段为 string 类型，按下面结构填入所需字段后序列化成 string。 选填
     */
    private String poi_info;
    /**
     * 是否将整个订单核销。此参数设为 true 则将此订单的所有子单核销。 use_all 和 item_order_list 必须有一个有效值，要么指定 use_all=true 整单核销，要么使用 item_order_list 指定特定的商品单 id 核销。 注意：此参数设为 true 时只有 out_order_no 的所有商品单都为待使用状态才能成功核销。 选填
     */
    private Boolean use_all;
    /**
     * 指定需要核销的子单列表，此列表内的子单必须是上面 out_order_no 订单所对应的子单。 object 结构内的字段见下面介绍。 限制数组长度 <= 100
     */
    private List<PushDeliveryItemOrderId> item_order_list;

    /**
     * 签证交易以及泛知识接入交易规则必传：
     * 1：推送子单状态到“履约中”
     * 2：推送子单状态到“履约完成”
     * 签证交易中，字段含义为：
     * 1：子单状态变为“签证结果已出 ”
     * 2：子单状态变为“确认签证结果”
     * 注意，签证交易中：
     * •状态变为“签证结果已出”后，可调用tt.confirm让用户发起签证结果确认
     * •状态变为“签证结果已出”14天后开发者可传入"2: 确认签证结果”帮助用户确认。tt.confirm不限制。
     * 选填
     */
    private Integer delivery_status;

    public String getOut_order_no() {
        return out_order_no;
    }

    public TradePushDeliveryQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getPoi_info() {
        return poi_info;
    }

    public TradePushDeliveryQuery setPoi_info(String poi_info) {
        this.poi_info = poi_info;
        return this;
    }

    public Boolean getUse_all() {
        return use_all;
    }

    public TradePushDeliveryQuery setUse_all(Boolean use_all) {
        this.use_all = use_all;
        return this;
    }

    public List<PushDeliveryItemOrderId> getItem_order_list() {
        return item_order_list;
    }

    public TradePushDeliveryQuery setItem_order_list(List<PushDeliveryItemOrderId> item_order_list) {
        this.item_order_list = item_order_list;
        return this;
    }

    public Integer getDelivery_status() {
        return delivery_status;
    }

    public TradePushDeliveryQuery setDelivery_status(Integer delivery_status) {
        this.delivery_status = delivery_status;
        return this;
    }

    public static TradePushDeliveryQueryBuilder builder() {
        return new TradePushDeliveryQueryBuilder();
    }

    public static final class TradePushDeliveryQueryBuilder {
        private String out_order_no;
        private String poi_info;
        private Boolean use_all;
        private List<PushDeliveryItemOrderId> item_order_list;
        private Integer delivery_status;
        private Integer tenantId;
        private String clientKey;

        private TradePushDeliveryQueryBuilder() {
        }

        public static TradePushDeliveryQueryBuilder aTradePushDeliveryQuery() {
            return new TradePushDeliveryQueryBuilder();
        }

        public TradePushDeliveryQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public TradePushDeliveryQueryBuilder poiInfo(String poiInfo) {
            this.poi_info = poiInfo;
            return this;
        }

        public TradePushDeliveryQueryBuilder useAll(Boolean useAll) {
            this.use_all = useAll;
            return this;
        }

        public TradePushDeliveryQueryBuilder itemOrderList(List<PushDeliveryItemOrderId> itemOrderList) {
            this.item_order_list = itemOrderList;
            return this;
        }

        public TradePushDeliveryQueryBuilder deliveryStatus(Integer deliveryStatus) {
            this.delivery_status = deliveryStatus;
            return this;
        }

        public TradePushDeliveryQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public TradePushDeliveryQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public TradePushDeliveryQuery build() {
            TradePushDeliveryQuery tradePushDeliveryQuery = new TradePushDeliveryQuery();
            tradePushDeliveryQuery.setOut_order_no(out_order_no);
            tradePushDeliveryQuery.setPoi_info(poi_info);
            tradePushDeliveryQuery.setUse_all(use_all);
            tradePushDeliveryQuery.setItem_order_list(item_order_list);
            tradePushDeliveryQuery.setDelivery_status(delivery_status);
            tradePushDeliveryQuery.setTenantId(tenantId);
            tradePushDeliveryQuery.setClientKey(clientKey);
            return tradePushDeliveryQuery;
        }
    }
}
