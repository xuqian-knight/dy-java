package com.dyj.applet.domain.vo;

/**
 * 发起进件返回值
 */
public class CreateSubMerchantDataVo {

    /**
     * 申请id
     */
    private String apply_id;
    /**
     * 二级商户id
     */
    private String merchant_id;


    public String getApply_id() {
        return apply_id;
    }

    public CreateSubMerchantDataVo setApply_id(String apply_id) {
        this.apply_id = apply_id;
        return this;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public CreateSubMerchantDataVo setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
        return this;
    }
}
