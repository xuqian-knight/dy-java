package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-07-17 10:04
 **/
public class ApplyCondition {

    /**
     * 条件描述  必须是上线的小程序
     */
    private String condition_description;
    /**
     * 条件名称 上线小程序
     */
    private String condition_name;
    /**
     * 是否满足
     */
    private Boolean satisfied;

    public String getCondition_description() {
        return condition_description;
    }

    public void setCondition_description(String condition_description) {
        this.condition_description = condition_description;
    }

    public String getCondition_name() {
        return condition_name;
    }

    public void setCondition_name(String condition_name) {
        this.condition_name = condition_name;
    }

    public Boolean getSatisfied() {
        return satisfied;
    }

    public void setSatisfied(Boolean satisfied) {
        this.satisfied = satisfied;
    }
}
