package com.dyj.applet.domain.query;

import com.alibaba.fastjson.JSONObject;
import com.dyj.applet.domain.PlayletBusinessUploadContext;
import com.dyj.applet.domain.PlayletBusinessUploadContextAd;
import com.dyj.applet.domain.PlayletBusinessUploadContextDevice;
import com.dyj.applet.domain.PlayletBusinessUploadProperties;
import com.dyj.common.domain.query.BaseQuery;

import java.util.Objects;

/**
 * @author danmo
 * @date 2024-05-21 10:19
 **/
public class PlayletBusinessUploadQuery extends BaseQuery {

    private PlayletBusinessUploadContext context;

    /**
     * 事件类型
     * premium_member(购买权益事件)
     * coin_change(余额变动事件)
     */
    private String event_type;
    /**
     * 事件参数
     */
    private String properties;

    /**
     * 事件时间戳
     */
    private Long timestamp;

    public static PlayletBusinessUploadQueryBuilder builder() {
        return new PlayletBusinessUploadQueryBuilder();
    }

    public static class PlayletBusinessUploadQueryBuilder {
        private PlayletBusinessUploadContext context;
        /**
         * 事件类型
         * premium_member(购买权益事件)
         * coin_change(余额变动事件)
         */
        private String eventType;
        /**
         * 事件参数
         */
        private PlayletBusinessUploadProperties properties;

        /**
         * 事件时间戳
         */
        private Long timestamp;

        private Integer tenantId;

        private String clientKey;

        public PlayletBusinessUploadQueryBuilder openId(String openId) {
            if(Objects.isNull(context)){
                context = new PlayletBusinessUploadContext();
            }
            context.setDevice(new PlayletBusinessUploadContextDevice(openId));
            return this;
        }

        public PlayletBusinessUploadQueryBuilder callback(String callback) {
            if(Objects.isNull(context)){
                context = new PlayletBusinessUploadContext();
            }
            context.setAd(new PlayletBusinessUploadContextAd(callback));
            return this;
        }

        public PlayletBusinessUploadQueryBuilder eventType(String eventType) {
            this.eventType = eventType;
            return this;
        }

        public PlayletBusinessUploadQueryBuilder properties(PlayletBusinessUploadProperties properties) {
            this.properties = properties;
            return this;
        }

        public PlayletBusinessUploadQueryBuilder timestamp(Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public PlayletBusinessUploadQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public PlayletBusinessUploadQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public PlayletBusinessUploadQuery build() {
            PlayletBusinessUploadQuery playletBusinessUploadQuery = new PlayletBusinessUploadQuery();
            playletBusinessUploadQuery.setContext(context);
            playletBusinessUploadQuery.setEvent_type(eventType);
            playletBusinessUploadQuery.setProperties(JSONObject.toJSONString(properties));
            playletBusinessUploadQuery.setTimestamp(timestamp);
            playletBusinessUploadQuery.setTenantId(tenantId);
            playletBusinessUploadQuery.setClientKey(clientKey);
            return playletBusinessUploadQuery;
        }
    }


    public PlayletBusinessUploadContext getContext() {
        return context;
    }

    public void setContext(PlayletBusinessUploadContext context) {
        this.context = context;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
