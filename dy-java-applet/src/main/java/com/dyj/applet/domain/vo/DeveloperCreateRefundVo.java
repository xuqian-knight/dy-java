package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * 生活服务交易系统->退货退款->开发者发起退款 相应值
 */
public class DeveloperCreateRefundVo extends BaseVo {

    /**
     * 抖音开放平台交易系统内部退款单号
     */
    private String refund_id;

    public String getRefund_id() {
        return refund_id;
    }

    public DeveloperCreateRefundVo setRefund_id(String refund_id) {
        this.refund_id = refund_id;
        return this;
    }
}
