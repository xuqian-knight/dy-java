package com.dyj.applet.domain;

import java.util.Map;
import java.util.Objects;

/**
 * 营销算价->用户身份信息(如会员)
 */
public class MarketingMembershipInfo {

    /**
     * 用户身份信息(如会员)
     */
    private String id;

    /**
     * 用户身份描述
     */
    private String desc;

    /**
     * 营销类别
     * 1：商家
     * 2：平台
     */
    private Integer kind;

    /**
     * 创建人类型
     * 1：抖音平台
     * 2：抖音来客—商家
     * 3：小程序商家
     */
    private Integer creator_type;

    /**
     * 扩展字段
     */
    private Map<Object,Objects> marketing_extend;

    public String getId() {
        return id;
    }

    public MarketingMembershipInfo setId(String id) {
        this.id = id;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public MarketingMembershipInfo setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public Integer getKind() {
        return kind;
    }

    public MarketingMembershipInfo setKind(Integer kind) {
        this.kind = kind;
        return this;
    }

    public Integer getCreator_type() {
        return creator_type;
    }

    public MarketingMembershipInfo setCreator_type(Integer creator_type) {
        this.creator_type = creator_type;
        return this;
    }

    public Map<Object, Objects> getMarketing_extend() {
        return marketing_extend;
    }

    public MarketingMembershipInfo setMarketing_extend(Map<Object, Objects> marketing_extend) {
        this.marketing_extend = marketing_extend;
        return this;
    }
}
