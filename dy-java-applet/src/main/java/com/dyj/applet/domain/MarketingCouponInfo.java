package com.dyj.applet.domain;

import java.util.List;
import java.util.Map;

/**
 * 营销算价->优惠券信息
 */
public class MarketingCouponInfo {

    /**
     * 优惠券id
     */
    private String id;

    /**
     * 优惠券编码
     */
    private String code;

    /**
     * 优惠券类型：
     * 1：立减券
     * 2：满减券
     * 3：折扣券
     */
    private Integer type;

    /**
     * 优惠券名称
     */
    private String name;

    /**
     * 优惠券领取时间戳，单位毫秒 选填
     */
    private Long receive_time;

    /**
     * 有效起始时间戳，单位毫秒
     */
    private Long start_time;

    /**
     * 有效结束时间戳，单位毫秒
     */
    private Long end_time;

    /**
     * 优惠价格，单位分 选填
     */
    private Long discount_amount;

    /**
     * 折扣百分比，填 0~100，折扣抵 30% 则填 30 选填
     */
    private Integer deduct_percentage;

    /**
     * 优惠券详情跳转链接 选填
     */
    private String detail_url;

    /**
     * 使用规则描述，包含适用范围描述，使用条件，退款时的处理说明等
     */
    private String rule;

    /**
     * 营销类别
     * 1：商家
     * 2：平台
     */
    private Integer kind;

    /**
     * 创建人类型
     * 1：抖音平台
     * 2：抖音来客—商家
     * 3：小程序商家
     */
    private Integer creator_type;

    /**
     * 扩展字段
     */
    private Map<Object,Object> marketing_extend;

    /**
     * 不可用原因 选填
     */
    private List<String> deny_reasons;

    public String getId() {
        return id;
    }

    public MarketingCouponInfo setId(String id) {
        this.id = id;
        return this;
    }

    public String getCode() {
        return code;
    }

    public MarketingCouponInfo setCode(String code) {
        this.code = code;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public MarketingCouponInfo setType(Integer type) {
        this.type = type;
        return this;
    }

    public String getName() {
        return name;
    }

    public MarketingCouponInfo setName(String name) {
        this.name = name;
        return this;
    }

    public Long getReceive_time() {
        return receive_time;
    }

    public MarketingCouponInfo setReceive_time(Long receive_time) {
        this.receive_time = receive_time;
        return this;
    }

    public Long getStart_time() {
        return start_time;
    }

    public MarketingCouponInfo setStart_time(Long start_time) {
        this.start_time = start_time;
        return this;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public MarketingCouponInfo setEnd_time(Long end_time) {
        this.end_time = end_time;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public MarketingCouponInfo setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public Integer getDeduct_percentage() {
        return deduct_percentage;
    }

    public MarketingCouponInfo setDeduct_percentage(Integer deduct_percentage) {
        this.deduct_percentage = deduct_percentage;
        return this;
    }

    public String getDetail_url() {
        return detail_url;
    }

    public MarketingCouponInfo setDetail_url(String detail_url) {
        this.detail_url = detail_url;
        return this;
    }

    public String getRule() {
        return rule;
    }

    public MarketingCouponInfo setRule(String rule) {
        this.rule = rule;
        return this;
    }

    public Integer getKind() {
        return kind;
    }

    public MarketingCouponInfo setKind(Integer kind) {
        this.kind = kind;
        return this;
    }

    public Integer getCreator_type() {
        return creator_type;
    }

    public MarketingCouponInfo setCreator_type(Integer creator_type) {
        this.creator_type = creator_type;
        return this;
    }

    public Map<Object, Object> getMarketing_extend() {
        return marketing_extend;
    }

    public MarketingCouponInfo setMarketing_extend(Map<Object, Object> marketing_extend) {
        this.marketing_extend = marketing_extend;
        return this;
    }

    public List<String> getDeny_reasons() {
        return deny_reasons;
    }

    public MarketingCouponInfo setDeny_reasons(List<String> deny_reasons) {
        this.deny_reasons = deny_reasons;
        return this;
    }
}
