package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.UserTaskInfo;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;
import java.util.Map;

/**
 * @author danmo
 * @date 2024-05-27 18:46
 **/
public class UserTaskVo extends BaseVo {

    private List<Map<String, UserTaskInfo>> task_info_list;


    public List<Map<String, UserTaskInfo>> getTask_info_list() {
        return task_info_list;
    }

    public void setTask_info_list(List<Map<String, UserTaskInfo>> task_info_list) {
        this.task_info_list = task_info_list;
    }
}
