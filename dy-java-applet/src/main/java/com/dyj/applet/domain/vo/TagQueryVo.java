package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.TagQueryTagDetail;

import java.util.List;

/**
 * 标签组信息
 */
public class TagQueryVo {

    /**
     * <p>标签组列表</p>
     */
    private List<TagQueryTagDetail> tag_detail_list;

    public List<TagQueryTagDetail> getTag_detail_list() {
        return tag_detail_list;
    }

    public TagQueryVo setTag_detail_list(List<TagQueryTagDetail> tag_detail_list) {
        this.tag_detail_list = tag_detail_list;
        return this;
    }
}
