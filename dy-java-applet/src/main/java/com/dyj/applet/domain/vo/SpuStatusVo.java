package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-29 16:21
 **/
public class SpuStatusVo extends BaseVo {

    /**
     * 状态同步成功的spu_ext_id列表
     */
    private List<String> spu_ext_id_list;

    public List<String> getSpu_ext_id_list() {
        return spu_ext_id_list;
    }

    public void setSpu_ext_id_list(List<String> spu_ext_id_list) {
        this.spu_ext_id_list = spu_ext_id_list;
    }
}
