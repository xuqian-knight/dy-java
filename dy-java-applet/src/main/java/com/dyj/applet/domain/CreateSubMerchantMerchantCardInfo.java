package com.dyj.applet.domain;

/**
 * 结算账户信息信息
 */
public class CreateSubMerchantMerchantCardInfo {

    /**
     * 卡户名
     */
    private String account_name;
    /**
     * 银行卡号
     */
    private String account_no;
    /**
     * <p>支付宝结算账号</p><p>alipay_settle_type 参数为2时必传</p> 选填
     */
    private String alipay_account_no;
    /**
     * <p>支付宝结算类型枚举值：</p><p>1: 卡结算</p><p>2: 支付宝账户结算</p>
     */
    private Integer alipay_settle_type;
    /**
     * 开户支行名
     */
    private String bank_full_name;
    /**
     * <p>银行卡类型枚举值：</p><p>DC: 借记卡</p><p>CC: 信用卡</p>
     */
    private String card_type;
    /**
     * <p>结算类型枚举值：</p><p>1: 对私</p><p>2: 对公</p>
     */
    private Integer settle_type;

    public String getAccount_name() {
        return account_name;
    }

    public CreateSubMerchantMerchantCardInfo setAccount_name(String account_name) {
        this.account_name = account_name;
        return this;
    }

    public String getAccount_no() {
        return account_no;
    }

    public CreateSubMerchantMerchantCardInfo setAccount_no(String account_no) {
        this.account_no = account_no;
        return this;
    }

    public String getAlipay_account_no() {
        return alipay_account_no;
    }

    public CreateSubMerchantMerchantCardInfo setAlipay_account_no(String alipay_account_no) {
        this.alipay_account_no = alipay_account_no;
        return this;
    }

    public Integer getAlipay_settle_type() {
        return alipay_settle_type;
    }

    public CreateSubMerchantMerchantCardInfo setAlipay_settle_type(Integer alipay_settle_type) {
        this.alipay_settle_type = alipay_settle_type;
        return this;
    }

    public String getBank_full_name() {
        return bank_full_name;
    }

    public CreateSubMerchantMerchantCardInfo setBank_full_name(String bank_full_name) {
        this.bank_full_name = bank_full_name;
        return this;
    }

    public String getCard_type() {
        return card_type;
    }

    public CreateSubMerchantMerchantCardInfo setCard_type(String card_type) {
        this.card_type = card_type;
        return this;
    }

    public Integer getSettle_type() {
        return settle_type;
    }

    public CreateSubMerchantMerchantCardInfo setSettle_type(Integer settle_type) {
        this.settle_type = settle_type;
        return this;
    }

    public static CreateSubMerchantMerchantCardInfoBuilder builder(){
        return new CreateSubMerchantMerchantCardInfoBuilder();
    }

    public static final class CreateSubMerchantMerchantCardInfoBuilder {
        private String account_name;
        private String account_no;
        private String alipay_account_no;
        private Integer alipay_settle_type;
        private String bank_full_name;
        private String card_type;
        private Integer settle_type;

        private CreateSubMerchantMerchantCardInfoBuilder() {
        }

        public CreateSubMerchantMerchantCardInfoBuilder accountName(String accountName) {
            this.account_name = accountName;
            return this;
        }

        public CreateSubMerchantMerchantCardInfoBuilder accountNo(String accountNo) {
            this.account_no = accountNo;
            return this;
        }

        public CreateSubMerchantMerchantCardInfoBuilder alipayAccountNo(String alipayAccountNo) {
            this.alipay_account_no = alipayAccountNo;
            return this;
        }

        public CreateSubMerchantMerchantCardInfoBuilder alipaySettleType(Integer alipaySettleType) {
            this.alipay_settle_type = alipaySettleType;
            return this;
        }

        public CreateSubMerchantMerchantCardInfoBuilder bankFullName(String bankFullName) {
            this.bank_full_name = bankFullName;
            return this;
        }

        public CreateSubMerchantMerchantCardInfoBuilder cardType(String cardType) {
            this.card_type = cardType;
            return this;
        }

        public CreateSubMerchantMerchantCardInfoBuilder settleType(Integer settleType) {
            this.settle_type = settleType;
            return this;
        }

        public CreateSubMerchantMerchantCardInfo build() {
            CreateSubMerchantMerchantCardInfo createSubMerchantMerchantCardInfo = new CreateSubMerchantMerchantCardInfo();
            createSubMerchantMerchantCardInfo.setAccount_name(account_name);
            createSubMerchantMerchantCardInfo.setAccount_no(account_no);
            createSubMerchantMerchantCardInfo.setAlipay_account_no(alipay_account_no);
            createSubMerchantMerchantCardInfo.setAlipay_settle_type(alipay_settle_type);
            createSubMerchantMerchantCardInfo.setBank_full_name(bank_full_name);
            createSubMerchantMerchantCardInfo.setCard_type(card_type);
            createSubMerchantMerchantCardInfo.setSettle_type(settle_type);
            return createSubMerchantMerchantCardInfo;
        }
    }
}
