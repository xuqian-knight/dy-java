package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-24 14:15
 **/
public class AptUserComment {

    /**
     * 新增评论
     */
    private Long new_comment;

    /**
     * yyyy-MM-dd日期
     */
    private String date;

    public Long getNew_comment() {
        return new_comment;
    }

    public void setNew_comment(Long new_comment) {
        this.new_comment = new_comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
