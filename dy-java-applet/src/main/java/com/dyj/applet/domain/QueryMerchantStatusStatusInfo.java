package com.dyj.applet.domain;

public class QueryMerchantStatusStatusInfo {
    /**
     * <p>签约链接，进件状态待签约下发，其他状态为空字符串</p>
     */
    private String legal_sign_url;
    /**
     * <p>法人验证链接，进件状态待认证下发，其他状态为空字符串</p>
     */
    private String legal_validation_url;
    /**
     * <p>商户号</p>
     */
    private String merchant_id;
    /**
     * <p>失败原因</p>
     */
    private String reject_reason;
    /**
     * <p><strong>进件状态枚举，以下状态枚举各渠道通用：</strong></p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">SUCCESS：成功</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">FAIL：失败</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">PROCESSING 受理中</li></ul><p><strong>以下状态枚举仅微信渠道包含：</strong></p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">WX_CHECKING 微信：审核中</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">WX_TO_VALID 微信：待认证</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">WX_TO_SIGN 微信：待签约</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">WX_FROZEN 微信：冻结</li></ul>
     */
    private String status;

    public String getLegal_sign_url() {
        return legal_sign_url;
    }

    public QueryMerchantStatusStatusInfo setLegal_sign_url(String legal_sign_url) {
        this.legal_sign_url = legal_sign_url;
        return this;
    }

    public String getLegal_validation_url() {
        return legal_validation_url;
    }

    public QueryMerchantStatusStatusInfo setLegal_validation_url(String legal_validation_url) {
        this.legal_validation_url = legal_validation_url;
        return this;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public QueryMerchantStatusStatusInfo setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
        return this;
    }

    public String getReject_reason() {
        return reject_reason;
    }

    public QueryMerchantStatusStatusInfo setReject_reason(String reject_reason) {
        this.reject_reason = reject_reason;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public QueryMerchantStatusStatusInfo setStatus(String status) {
        this.status = status;
        return this;
    }
}
