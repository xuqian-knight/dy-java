package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class TradeToolkitQueryTextQuery extends BaseQuery {

    /**
     * 文案类型，不传或传0会全部返回
     * 选填
     */
    private Integer text_type;

    public Integer getText_type() {
        return text_type;
    }

    public TradeToolkitQueryTextQuery setText_type(Integer text_type) {
        this.text_type = text_type;
        return this;
    }

    public static TradeToolkitQueryTextQueryBuilder builder() {
        return new TradeToolkitQueryTextQueryBuilder();
    }

    public static final class TradeToolkitQueryTextQueryBuilder {
        private Integer text_type;
        private Integer tenantId;
        private String clientKey;

        private TradeToolkitQueryTextQueryBuilder() {
        }

        public TradeToolkitQueryTextQueryBuilder textType(Integer textType) {
            this.text_type = textType;
            return this;
        }

        public TradeToolkitQueryTextQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public TradeToolkitQueryTextQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public TradeToolkitQueryTextQuery build() {
            TradeToolkitQueryTextQuery tradeToolkitQueryTextQuery = new TradeToolkitQueryTextQuery();
            tradeToolkitQueryTextQuery.setText_type(text_type);
            tradeToolkitQueryTextQuery.setTenantId(tenantId);
            tradeToolkitQueryTextQuery.setClientKey(clientKey);
            return tradeToolkitQueryTextQuery;
        }
    }
}
