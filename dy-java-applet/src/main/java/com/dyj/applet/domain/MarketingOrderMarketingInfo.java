package com.dyj.applet.domain;

import java.util.List;

/**
 * 营销算价->用户所选营销策略信息，仅包含该商品的商品维度优惠信息
 */
public class MarketingOrderMarketingInfo {

    /**
     * 用户所选营销策略信息，仅包含订单维度优惠信息 选填
     */
    private List<MarketingBundle> selected_marketing;

    /**
     * 订单总价，单位分
     */
    private Long total_amount;

    public List<MarketingBundle> getSelected_marketing() {
        return selected_marketing;
    }

    public MarketingOrderMarketingInfo setSelected_marketing(List<MarketingBundle> selected_marketing) {
        this.selected_marketing = selected_marketing;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public MarketingOrderMarketingInfo setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }
}
