package com.dyj.applet.domain.vo;

import java.util.List;

public class OrderDiscountDetail {

    /**
     * 订单维度总优惠金额，单位分
     */
    private Integer order_total_discount_amount;

    /**
     * 商品（SKU）维度总优惠金额，单位分
     */
    private Integer goods_total_discount_amount;

    /**
     * 营销明细
     * 选填
     */
    private List<ItemDiscountDetail> marketing_detail_info;

    public Integer getOrder_total_discount_amount() {
        return order_total_discount_amount;
    }

    public OrderDiscountDetail setOrder_total_discount_amount(Integer order_total_discount_amount) {
        this.order_total_discount_amount = order_total_discount_amount;
        return this;
    }

    public Integer getGoods_total_discount_amount() {
        return goods_total_discount_amount;
    }

    public OrderDiscountDetail setGoods_total_discount_amount(Integer goods_total_discount_amount) {
        this.goods_total_discount_amount = goods_total_discount_amount;
        return this;
    }

    public List<ItemDiscountDetail> getMarketing_detail_info() {
        return marketing_detail_info;
    }

    public OrderDiscountDetail setMarketing_detail_info(List<ItemDiscountDetail> marketing_detail_info) {
        this.marketing_detail_info = marketing_detail_info;
        return this;
    }
}
