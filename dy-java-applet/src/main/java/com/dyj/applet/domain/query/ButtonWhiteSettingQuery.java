package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class ButtonWhiteSettingQuery extends BaseQuery {

    /**
     * 设置的按钮类型1-核销工具,2-外卖到家,3-外卖到店,4-详情页
     */
    private Integer button_type;

    /**
     *
     * uid维度白名单，每次传入都是全量覆盖。
     * •uid_list&open_all 至少传入一个
     * •uid_list数量不能超过100
     * •每次传入uid_list均为全量覆盖
     * ◦比如第一次传入[11,22]
     * ◦第二次传入[33,44]
     * ◦则最后只有[33,44]在白名单列表中
     * 选填
     */
    private List<Integer> uid_list;

    /**
     * 全量用户可见字段
     * •true:代表所有用户都可以看到c端去使用按钮，从而跳转核销工具小程序
     * •False: 关闭
     * 选填
     */
    private Boolean open_all;

    public Integer getButton_type() {
        return button_type;
    }

    public ButtonWhiteSettingQuery setButton_type(Integer button_type) {
        this.button_type = button_type;
        return this;
    }

    public List<Integer> getUid_list() {
        return uid_list;
    }

    public ButtonWhiteSettingQuery setUid_list(List<Integer> uid_list) {
        this.uid_list = uid_list;
        return this;
    }

    public Boolean getOpen_all() {
        return open_all;
    }

    public ButtonWhiteSettingQuery setOpen_all(Boolean open_all) {
        this.open_all = open_all;
        return this;
    }

    public static ButtonWhiteSettingQueryBuilder builder() {
        return new ButtonWhiteSettingQueryBuilder();
    }

    public static final class ButtonWhiteSettingQueryBuilder {
        private Integer button_type;
        private List<Integer> uid_list;
        private Boolean open_all;
        private Integer tenantId;
        private String clientKey;

        private ButtonWhiteSettingQueryBuilder() {
        }

        public ButtonWhiteSettingQueryBuilder buttonType(Integer buttonType) {
            this.button_type = buttonType;
            return this;
        }

        public ButtonWhiteSettingQueryBuilder uidList(List<Integer> uidList) {
            this.uid_list = uidList;
            return this;
        }

        public ButtonWhiteSettingQueryBuilder openAll(Boolean openAll) {
            this.open_all = openAll;
            return this;
        }

        public ButtonWhiteSettingQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ButtonWhiteSettingQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public ButtonWhiteSettingQuery build() {
            ButtonWhiteSettingQuery buttonWhiteSettingQuery = new ButtonWhiteSettingQuery();
            buttonWhiteSettingQuery.setButton_type(button_type);
            buttonWhiteSettingQuery.setUid_list(uid_list);
            buttonWhiteSettingQuery.setOpen_all(open_all);
            buttonWhiteSettingQuery.setTenantId(tenantId);
            buttonWhiteSettingQuery.setClientKey(clientKey);
            return buttonWhiteSettingQuery;
        }
    }
}
