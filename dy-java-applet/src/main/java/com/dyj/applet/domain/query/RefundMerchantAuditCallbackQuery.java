package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 生活服务交易系统->退货退款->同步退款审核结果请求值
 */
public class RefundMerchantAuditCallbackQuery extends BaseQuery {

    /**
     * 不同意退款信息(不同意退款时必填)，长度 <= 512 byte 选填
     */
    private String deny_message;
    /**
     * 开发者侧退款单号，长度 <= 64 byte
     */
    private String out_refund_no;
    /**
     * 审核状态
     */
    private Byte refund_audit_status;

    public String getDeny_message() {
        return deny_message;
    }

    public RefundMerchantAuditCallbackQuery setDeny_message(String deny_message) {
        this.deny_message = deny_message;
        return this;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public RefundMerchantAuditCallbackQuery setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public Byte getRefund_audit_status() {
        return refund_audit_status;
    }

    public RefundMerchantAuditCallbackQuery setRefund_audit_status(Byte refund_audit_status) {
        this.refund_audit_status = refund_audit_status;
        return this;
    }

    public static RefundMerchantAuditCallbackQueryBuilder builder() {
        return new RefundMerchantAuditCallbackQueryBuilder();
    }


    public static final class RefundMerchantAuditCallbackQueryBuilder {
        private String deny_message;
        private String out_refund_no;
        private Byte refund_audit_status;
        private Integer tenantId;
        private String clientKey;

        private RefundMerchantAuditCallbackQueryBuilder() {
        }

        public RefundMerchantAuditCallbackQueryBuilder denyMessage(String denyMessage) {
            this.deny_message = denyMessage;
            return this;
        }

        public RefundMerchantAuditCallbackQueryBuilder outRefundNo(String outRefundNo) {
            this.out_refund_no = outRefundNo;
            return this;
        }

        public RefundMerchantAuditCallbackQueryBuilder refundAuditStatus(Byte refundAuditStatus) {
            this.refund_audit_status = refundAuditStatus;
            return this;
        }

        public RefundMerchantAuditCallbackQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public RefundMerchantAuditCallbackQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public RefundMerchantAuditCallbackQuery build() {
            RefundMerchantAuditCallbackQuery refundMerchantAuditCallbackQuery = new RefundMerchantAuditCallbackQuery();
            refundMerchantAuditCallbackQuery.setDeny_message(deny_message);
            refundMerchantAuditCallbackQuery.setOut_refund_no(out_refund_no);
            refundMerchantAuditCallbackQuery.setRefund_audit_status(refund_audit_status);
            refundMerchantAuditCallbackQuery.setTenantId(tenantId);
            refundMerchantAuditCallbackQuery.setClientKey(clientKey);
            return refundMerchantAuditCallbackQuery;
        }
    }
}
