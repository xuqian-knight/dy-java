package com.dyj.applet.domain.vo;

import java.util.List;

/**
 * 获取资金账单返回值
 */
public class GetFundBillVo {

    /**
     * 包含多个文件链接的数组
     * 链接过期时间为6小时
     */
    private List<String> fund_bill_list;
}
