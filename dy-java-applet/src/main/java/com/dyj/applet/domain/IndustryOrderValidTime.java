package com.dyj.applet.domain;

/**
 * 券的有效期
 */
public class IndustryOrderValidTime {

    /**
     * 券的有效期开始时间，单位毫秒，须大于 0 选填
     */
    private Long valid_start_time;

    /**
     * 券的有效期结束时间，单位毫秒，须大于 0，且须大于 valid_start_time 和当前时间 选填
     */
    private Long valid_end_time;

    /**
     * a.券的相对有效时间，单位毫秒，须大于 0
     * b.与 valid_start_time、valid_end_time 组合，至少回传一个，否则会下单失败
     * c.都合法优先使用 valid_start_time、valid_end_time 组合
     * d.当 valid_duration 有效时，
     * 券的有效期开始时间 S = 订单支付完成时间
     * 券的有效期结束时间 E = 1 天 + 向下按天截断（S + valid_duration)）。
     * 选填
     */
    private Long valid_duration;

    public Long getValid_start_time() {
        return valid_start_time;
    }

    public IndustryOrderValidTime setValid_start_time(Long valid_start_time) {
        this.valid_start_time = valid_start_time;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public IndustryOrderValidTime setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }

    public Long getValid_duration() {
        return valid_duration;
    }

    public IndustryOrderValidTime setValid_duration(Long valid_duration) {
        this.valid_duration = valid_duration;
        return this;
    }
}
