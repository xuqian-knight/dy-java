package com.dyj.applet.domain;

/**
 * 券的验券结果
 */
public class TradeDeliveryVerifyVerifyResult {

    /**
     * 用户券码，核销成功时会将用户券码返回。开发者可用于对账。 选填
     */
    private String certificate_code;
    /**
     * 交易系统里对应的商品单 id
     */
    private String item_order_id;
    /**
     * 验券结果码，0 表示成功
     */
    private Long result_code;
    /**
     * 验券结果 result_code 的说明
     */
    private String result_msg;

    /**
     * 核销时间，13 位毫秒级时间戳
     */
    private Long verify_time;

    public String getCertificate_code() {
        return certificate_code;
    }

    public TradeDeliveryVerifyVerifyResult setCertificate_code(String certificate_code) {
        this.certificate_code = certificate_code;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public TradeDeliveryVerifyVerifyResult setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public Long getResult_code() {
        return result_code;
    }

    public TradeDeliveryVerifyVerifyResult setResult_code(Long result_code) {
        this.result_code = result_code;
        return this;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public TradeDeliveryVerifyVerifyResult setResult_msg(String result_msg) {
        this.result_msg = result_msg;
        return this;
    }

    public Long getVerify_time() {
        return verify_time;
    }

    public TradeDeliveryVerifyVerifyResult setVerify_time(Long verify_time) {
        this.verify_time = verify_time;
        return this;
    }
}
