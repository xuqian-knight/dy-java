package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.PoiBillboard;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 15:55
 **/
public class PoiBillboardVo extends BaseVo {

    private List<PoiBillboard> result_list;

    public List<PoiBillboard> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<PoiBillboard> result_list) {
        this.result_list = result_list;
    }
}
