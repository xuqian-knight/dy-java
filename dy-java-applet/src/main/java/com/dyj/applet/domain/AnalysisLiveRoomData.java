package com.dyj.applet.domain;

public class AnalysisLiveRoomData {

    /**
     * 评论次数，挂载了小程序的主播, 直播开播开始，在直播间发评论的次数
     */
    private Long comment_count;
    /**
     * 累计看播人数，挂载了小程序的主播, 在直播被打开观看的人数
     */
    private Long cumulative_audience_count;
    /**
     * 点赞次数，挂载了小程序的主播, 直播开播开始，点赞的次数
     */
    private Long like_times;
    /**
     * 分享次数，挂载了小程序的主播, 直播开播开始，分享成功的人数
     */
    private Long share_count;
    /**
     * 时间格式为“2006-01-02 15:04:05”
     */
    private String time;

    public Long getComment_count() {
        return comment_count;
    }

    public void setComment_count(Long comment_count) {
        this.comment_count = comment_count;
    }

    public Long getCumulative_audience_count() {
        return cumulative_audience_count;
    }

    public void setCumulative_audience_count(Long cumulative_audience_count) {
        this.cumulative_audience_count = cumulative_audience_count;
    }

    public Long getLike_times() {
        return like_times;
    }

    public void setLike_times(Long like_times) {
        this.like_times = like_times;
    }

    public Long getShare_count() {
        return share_count;
    }

    public void setShare_count(Long share_count) {
        this.share_count = share_count;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
