package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisComponentOverviewData;

import java.util.List;

public class AnalysisComponentWithOverviewVo {

    private AnalysisComponentOverviewData ComponentOverviewData;

    private List<AnalysisComponentOverviewData> ComponentDataInfoList;

    public AnalysisComponentOverviewData getComponentOverviewData() {
        return ComponentOverviewData;
    }

    public void setComponentOverviewData(AnalysisComponentOverviewData componentOverviewData) {
        ComponentOverviewData = componentOverviewData;
    }

    public List<AnalysisComponentOverviewData> getComponentDataInfoList() {
        return ComponentDataInfoList;
    }

    public void setComponentDataInfoList(List<AnalysisComponentOverviewData> componentDataInfoList) {
        ComponentDataInfoList = componentDataInfoList;
    }
}
