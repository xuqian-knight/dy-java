package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-06-12 14:16
 **/
public class DoudianShop {

    /**
     *抖音小程序ID
     */
    private String MaAppId;
    /**
     * 抖店账号ID
     */
    private String ShopId;
    /**
     * 抖店店铺名称
     */
    private String ShopName;
    /**
     * 抖店用户ID
     */
    private String ToutiaoId;
    /**
     * 绑定抖店应用ID
     */
    private String DoudianAppId;

    public String getMaAppId() {
        return MaAppId;
    }

    public void setMaAppId(String maAppId) {
        MaAppId = maAppId;
    }

    public String getShopId() {
        return ShopId;
    }

    public void setShopId(String shopId) {
        ShopId = shopId;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public String getToutiaoId() {
        return ToutiaoId;
    }

    public void setToutiaoId(String toutiaoId) {
        ToutiaoId = toutiaoId;
    }

    public String getDoudianAppId() {
        return DoudianAppId;
    }

    public void setDoudianAppId(String doudianAppId) {
        DoudianAppId = doudianAppId;
    }
}
