package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.vo.BinDoudianAccountVo;
import com.dyj.applet.domain.vo.CreateDoudianAppVo;
import com.dyj.applet.domain.vo.DoudianAppVo;
import com.dyj.applet.domain.vo.DoudianShopVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.UserInfoQuery;
import com.dyj.common.interceptor.AppV2TokenHeaderInterceptor;

import java.util.List;

/**
 * 抖店绑定
 *
 * @author danmo
 * @date 2024-06-12 13:58
 **/
@BaseRequest(baseURL = "${ttDomain}")
public interface AptDouDianClient {

    /**
     * 绑定抖店开放平台账号
     *
     * @param query 用户信息
     * @return DySimpleResult<BinDoudianAccountVo>
     */
    @Post(value = "/api/apps/v1/capacity/bind_doudian_account", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<BinDoudianAccountVo> bindDoudianAccount(@Var("query") UserInfoQuery query);

    /**
     * 查询绑定的抖店开放平台账号信息
     *
     * @param query 用户信息
     * @return DySimpleResult<BinDoudianAccountVo>
     */
    @Get(value = "/api/apps/v1/capacity/query_bind_doudian_account", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<BinDoudianAccountVo> queryBindDoudianAccount(@Var("query") UserInfoQuery query);

    /**
     * 配置抖店开放平台应用
     *
     * @param query          用户信息
     * @param shopName       店铺名称
     * @param note           备注说明
     * @param screenShotList 系统主要功能截图，使用上传素材接口获取到的路径
     * @return DySimpleResult<CreateDoudianAccountVo>
     */
    @Post(value = "/api/apps/v1/capacity/create_doudian_app", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<CreateDoudianAppVo> createDoudianApp(@Var("query") UserInfoQuery query, @JSONBody("shop_name") String shopName, @JSONBody("note") String note, @JSONBody("screen_shot_list") List<String> screenShotList);

    /**
     * 查询配置的抖店开放平台应用信息
     *
     * @param query 用户信息
     * @return DySimpleResult<DoudianAppVo>
     */
    @Get(value = "/api/apps/v1/capacity/query_doudian_app", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<DoudianAppVo> queryDoudianApp(@Var("query") UserInfoQuery query);

    /**
     * 获取绑定抖店账号信息
     *
     * @param query 用户信息
     * @return DySimpleResult<DoudianShopVo>
     */
    @Get(value = "/api/apps/v1/capacity/query_doudian_shop_info", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<DoudianShopVo> queryDoudianShopInfo(@Var("query") UserInfoQuery query);
}
