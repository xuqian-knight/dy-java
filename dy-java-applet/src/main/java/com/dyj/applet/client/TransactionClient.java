package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dtflys.forest.backend.ContentType;
import com.dyj.applet.domain.*;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DataAndExtraVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.domain.vo.BaseVo;
import com.dyj.common.interceptor.ClientTokenInterceptor;
import com.dyj.common.interceptor.TransactionMerchantTokenInterceptor;

import java.io.InputStream;
import java.util.List;

/**
 * 交易系统 通用交易系统 生活服务交易系统(全融合版) 生活服务交易系统(账号融合版)
 */
@BaseRequest(baseURL = "${domain}")
public interface TransactionClient {

    /**
     * 进件图片上传
     * @param imageType 图片格式，支持格式：jpg、jpeg、png
     * @param imageContent 图片二进制字节流，最大为2M
     * @return
     */
    @Post(value = "${merchantImageUpload}", interceptor = TransactionMerchantTokenInterceptor.class,contentType = ContentType.MULTIPART_FORM_DATA)
    DySimpleResult<ImageUploadImageIdVo> merchantImageUpload(@Var("query") BaseTransactionMerchantQuery query, @Body(name = "image_type") String imageType, @DataFile(value = "image_content",fileName = "image_content") InputStream imageContent);


    /**
     * 发起进件
     * @param body 发起进件请求值
     * @return
     */
    @Post(value = "${createSubMerchantV3}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<CreateSubMerchantDataVo> createSubMerchantV3(@JSONBody CreateSubMerchantMerchantQuery body);

    /**
     * 进件查询
     * @param body 进件查询请求值
     * @return
     */
    @Post(value = "${queryMerchantStatusV3}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<QueryMerchantStatus> queryMerchantStatusV3(@JSONBody QueryMerchantStatusMerchantQuery body);

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     * @param body 开发者获取小程序收款商户/合作方进件页面请求值
     * @return
     */
    @Post(value = "${openAppAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openAppAddSubMerchant(@JSONBody GetMerchantUrlMerchantQuery body);

    /**
     * 服务商获取小程序收款商户进件页面
     * @param body 服务商获取小程序收款商户进件页面请求值
     * @return
     */
    @Post(value = "${openAddAppMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openAddAppMerchant(@JSONBody GetMerchantUrlMerchantQuery body);

    /**
     * 服务商获取服务商进件页面
     * @param body 服务商获取服务商进件页面请求值
     * @return
     */
    @Post(value = "${openSaasAddMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openSaasAddMerchant(@JSONBody OpenSaasAddMerchantMerchantQuery body);

    /**
     * 服务商获取合作方进件页面
     * @param body 服务商获取合作方进件页面请求值
     * @return
     */
    @Post(value = "${openSaasAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openSaasAddSubMerchant(@JSONBody OpenSaasAddSubMerchantMerchantQuery body);

    /**
     * 查询标签组信息
     * @param body 查询标签组信息请求值
     * @return
     */
    @Post(value = "${tagQuery}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<TagQueryVo> tagQuery(@JSONBody TagQueryQuery body);

    /**
     * 查询CPS信息
     * @param body 查询CPS信息请求值
     * @return
     */
    @Post(value = "${queryCpsOpenApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryTransactionCps> queryCps(@JSONBody QueryTransactionCpsQuery body);

    /**
     * 查询订单信息
     * @param body 查询订单信息请求值
     * @return
     */
    @Post(value = "${queryOrderOpenApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryTransactionOrder> queryOrder(@JSONBody QueryTransactionOrderQuery body);

    /**
     * 发起退款
     * @param body 发起退款请求值
     * @return
     */
    @Post(value = "${createRefundOpenApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateRefundVo> createRefund(@JSONBody CreateRefundQuery body);

    /**
     * 查询退款
     * @param body 查询退款请求值
     * @return
     */
    @Post(value = "${queryRefundOpenApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryRefundResultVo> queryRefund(@JSONBody QueryRefundQuery body);

    /**
     * 同步退款审核结果
     * @param body 同步退款审核结果请求值
     * @return
     */
    @Post(value = "${merchantAuditCallback}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> merchantAuditCallback(@JSONBody MerchantAuditCallbackQuery body);

    /**
     * 推送履约状态
     * @param body 推送履约状态请求值
     * @return
     */
    @Post(value = "${fulfillPushStatus}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> fulfillPushStatus(@JSONBody FulfillPushStatusQuery body);

    /**
     * 发起分账
     * @param body 发起分账请求值
     * @return
     */
    @Post(value = "${createSettle}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateSettleVo> createSettle(@JSONBody CreateSettleQuery body);

    /**
     * 查询分账
     * @param body 查询分账请求值
     * @return
     */
    @Post(value = "${querySettle}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<List<QuerySettleResult>> querySettle(@JSONBody QuerySettleQuery body);

    /**
     * 商户余额查询
     * @param body 商户余额查询请求值
     * @return
     */
    @Post(value = "${openQueryChannelBalanceAccount}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<QueryChannelBalanceAccountVo> queryChannelBalanceAccount(@JSONBody QueryChannelBalanceAccountQuery body);

    /**
     * 商户提现
     * @param body 商户提现请求值
     * @return
     */
    @Post(value = "${openMerchantWithdraw}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantWithdrawVo> merchantWithdraw(@JSONBody MerchantWithdrawQuery body);

    /**
     * 商户提现结果查询
     * @param body 商户提现结果查询请求值
     * @return
     */
    @Post(value = "${openQueryWithdrawOrder}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<QueryWithdrawOrderVo> queryWithdrawOrder(@JSONBody QueryWithdrawOrderQuery body);

    /**
     * 开发者获取小程序收款商户/合作方提现页面
     * @param body
     * @return
     */
    @Post(value = "${saasAppAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> saasAppAddSubMerchant(@JSONBody SaasAppAddSubMerchantQuery body);

    /**
     * 服务商获取小程序收款商户提现页面
     * @param body
     * @return
     */
    @Post(value = "${saasGetAppMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> saasGetAppMerchant(@JSONBody OpenAddAppMerchantMerchantQuery body);

    /**
     * 服务商获取服务商提现页面
     * @param body
     * @return
     */
    @Post(value = "${saasAddMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> saasAddMerchant(@JSONBody SaasAddMerchantQuery body);

    /**
     * 服务商获取合作方提现页面
     * @param body
     * @return
     */
    @Post(value = "${saasAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> saasAddSubMerchant(@JSONBody OpenSaasAddSubMerchantMerchantQuery body);

    /**
     * 获取资金账单
     * @param body 获取资金账单请求值
     * @return
     */
    @Post(value = "${getFundBill}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<GetFundBillVo> getFundBill(@JSONBody GetFundBillQuery body);

    /**
     * 获取交易账单
     * @param body 获取交易账单请求值
     * @return
     */
    @Post(value = "${getBill}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<GetBillVo> getBill(@JSONBody GetBillQuery body);




    /**
     * 查询订单基本信息。
     * @param body 查询订单基本信息。请求值
     * @return
     */
    @Post(value = "${queryIndustryOrder}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<QueryIndustryOrderVo> queryIndustryOrder(@JSONBody QueryIndustryOrderQuery body);

    /**
     * 查询券状态信息
     * @param body 查询券状态信息请求值
     * @return
     */
    @Post(value = "${queryIndustryItemOrderInfo}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<QueryIndustryItemOrderInfoVo> queryIndustryItemOrderInfo(@JSONBody QueryIndustryItemOrderInfoQuery body);

    /**
     * 生活服务交易系统->查询 CPS 信息
     * @param body 查询 CPS 信息请求值
     * @return
     */
    @Post(value = "${queryIndustryOrderCps}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<QueryIndustryCpsVo> queryIndustryOrderCps(@JSONBody QueryIndustryOrderCpsQuery body);

    /**
     * 生活服务交易系统->预下单->开发者发起下单
     * @param body
     * @return
     */
    @Post(value = "${preCreateIndustryOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<PreCreateIndustryOrderVo> preCreateIndustryOrder(@JSONBody PreCreateIndustryOrderQuery body);

    /**
     * 生活服务交易系统->营销算价->查询营销算价
     * @param body
     */
    @Post(value = "${queryAndCalculateMarketing}",interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryAndCalculateMarketingResult> queryAndCalculateMarketing(@JSONBody QueryAndCalculateMarketingQuery body);

    /**
     * 生活服务交易系统->核销->抖音码->验券准备
     * @param body 验券准备请求值
     * @return
     */
    @Post(value = "${deliveryPrepare}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<DeliveryPrepareVo> deliveryPrepare(@JSONBody DeliveryPrepareQuery body);


    /**
     * 生活服务交易系统->核销->抖音码->验券
     * @param body 验券请求值
     * @return
     */
    @Post(value = "${deliveryVerify}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<DeliveryVerifyVo> deliveryVerify(@JSONBody DeliveryVerifyQuery body);

    /**
     * 生活服务交易系统->核销->抖音码->
     * 生活服务交易系统->核销->三方码->撤销核销
     * @param body 撤销核销请求值
     * @return
     */
    @Post(value = "${verifyCancel}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<BaseVo> verifyCancel(@JSONBody VerifyCancelQuery body);

    /**
     * 生活服务交易系统->核销->三方码->推送核销状态
     * @param body 推送核销状态请求值
     * @return
     */
    @Post(value = "${pushDelivery}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<PushDeliveryVo> pushDelivery(@JSONBody PushDeliveryQuery body);

    /**
     * 生活服务交易系统->分账->查询分账
     * @param body 查询分账请求值
     * @return
     */
    @Post(value = "${industryQuerySettle}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<List<IndustryQuerySettleVo>> industryQuerySettle(@JSONBody IndustryQuerySettleQuery body);

    /**
     * 生活服务交易系统->退货退款->开发者发起退款
     * @param body
     * @return
     */
    @Post(value = "${developerCreateRefund}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<DeveloperCreateRefundVo> developerCreateRefund(@JSONBody DeveloperCreateRefundQuery body);

    /**
     * 生活服务交易系统->退货退款->同步退款审核结果
     * @param body 同步退款审核结果请求值
     * @return
     */
    @Post(value = "${refundMerchantAuditCallback}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<BaseVo> refundMerchantAuditCallback(@JSONBody RefundMerchantAuditCallbackQuery body);

    /**
     * 生活服务交易系统->退货退款->查询退款
     * @param body
     * @return
     */
    @Post(value = "${queryRefund}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<IndustryQueryRefundVo> industryQueryRefund(@JSONBody QueryRefundQuery body);

    /**
     * 生活服务交易系统->预约->创建预约单
     * @param body
     * @return
     */
    @Post(value = "createBook", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<CreateBookVo> createBook(@JSONBody CreateBookQuery body);

    /**
     * 生活服务交易系统->预约->预约接单结果回调
     * @param body
     */
    @Post(value = "bookResultCallback", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<Void> bookResultCallback(@JSONBody BookResultCallbackQuery body);

    /**
     * 生活服务交易系统->预约->商家取消预约
     * @param body
     */
    @Post(value = "merchantCancelBook", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<Void> merchantCancelBook(@JSONBody MerchantCancelBookQuery body);

    /**
     * 生活服务交易系统->预约->用户取消预约
     * @param body
     */
    @Post(value = "userCancelBook", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<Void> userCancelBook(@JSONBody UserCancelBookQuery body);

    /**
     * 生活服务交易系统->预约->查询预约单信息
     */
    @Post(value = "queryBook", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<List<QueryBookVo>> queryBook(@JSONBody QueryBookQuery body);

    /**
     * 生活服务交易系统->预下单->关闭订单
     * @return
     */
    @Post(value = "closeOrder", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<Void> closeOrder(@JSONBody CloseOrderQuery body);

    /**
     * 生活服务交易系统->核销->核销工具->查询用户券列表
     * @param body
     */
    @Post(value = "queryUserCertificates", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<QueryUserCertificatesVo> queryUserCertificates(@JSONBody QueryUserCertificatesQuery body);

    /**
     * 生活服务交易系统->核销->核销工具->查询订单可用门店
     * @param body
     */
    @Post(value = "orderCanUse", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<QueryCertificatesOrderInfo> orderCanUse(@JSONBody OrderCanUseQuery body);

    /**
     * 生活服务交易系统->核销->核销工具->设置商家展示信息
     * @param body
     */
    @Post(value = "updateMerchantConf", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<Void> updateMerchantConf(@JSONBody UpdateMerchantConfQuery body);

    /**
     * 生活服务交易系统->核销->核销工具->查询商家配置文案
     * @param body
     */
    @Post(value = "tradeToolkitQueryText", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<TradeToolkitQueryTextVo> tradeToolkitQueryText(@JSONBody TradeToolkitQueryTextQuery body);

    /**
     * 生活服务交易系统->核销->核销工具->设置订单详情页按钮白名单接口
     * @param body
     */
    @Post(value = "buttonWhiteSetting", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<Void> buttonWhiteSetting(@JSONBody ButtonWhiteSettingQuery body);

    /**
     * 生活服务交易系统->核销->核销工具->设置小程序跳转path
     * @param body
     */
    @Post(value = "updateMerchantPath", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<Void> updateMerchantPath(@JSONBody UpdateMerchantPathQuery body);


    /**
     * 生活服务交易系统->分账->发起分账
     * @param body 发起分账请求值
     * @return
     */
    @Post(value = "${createSettleV2}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<CreateSettleV2Vo> createSettleV2(@JSONBody CreateSettleV2Query body);
}
