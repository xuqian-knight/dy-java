package com.dyj.applet.handler;

import com.dyj.applet.domain.query.CreateUserTaskQuery;
import com.dyj.applet.domain.vo.CreateTaskVo;
import com.dyj.applet.domain.vo.UserShareTaskVo;
import com.dyj.applet.domain.vo.UserTaskVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-28 14:50
 **/
public class AptShareTaskHandler extends AbstractAppletHandler {
    public AptShareTaskHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 创建用户任务
     *
     * @param query 入参
     * @return DySimpleResult<CreateTaskVo>
     */
    public DySimpleResult<CreateTaskVo> createUserTask(CreateUserTaskQuery query) {
        baseQuery(query);
        return getShareTaskClient().createUserTask(query);
    }

    /**
     * 查询用户任务进度
     *
     * @param appId  小程序ID
     * @param openId 开启任务的用户openID
     * @param taskId 任务 ID 列表
     * @return DySimpleResult<UserTaskVo>
     */
    public DySimpleResult<UserTaskVo> queryUserTask(String appId, String openId, List<String> taskId) {
        return getShareTaskClient().queryUserTask(baseQuery(), appId, openId, taskId);
    }

    /**
     * 创建互动任务
     *
     * @param query 入参
     * @return DySimpleResult<CreateTaskVo>
     */
    public DySimpleResult<CreateTaskVo> createInteractTask(CreateUserTaskQuery query) {
        baseQuery(query);
        return getShareTaskClient().createInteractTask(query);
    }

    /**
     * 查询互动任务进度
     *
     * @param appId  小程序ID
     * @param openId 开启任务的用户openID
     * @param taskId 任务 ID 列表
     * @return DySimpleResult<UserTaskVo>
     */
    public DySimpleResult<UserTaskVo> queryUserInteractTask(String appId, String openId, List<String> taskId) {
        return getShareTaskClient().queryUserInteractTask(baseQuery(), appId, openId, taskId);
    }

    /**
     * 创建分享任务
     *
     * @param targetCount 目标分享次数，分享给非重复用户算作成功分享
     * @param taskType    任务类型 1：分享任务
     * @param startTime   开始时间戳，秒级
     * @param endTime     结束时间戳，秒级，任务持续时间最长一年
     * @return DySimpleResult<CreateTaskVo>
     */
    public DySimpleResult<CreateTaskVo> createShareTask(Long targetCount, Integer taskType, Long startTime, Long endTime) {
        return getShareTaskClient().createShareTask(baseQuery(), targetCount, taskType, startTime, endTime);
    }

    /**
     * 查询分享任务进度
     *
     * @param openId 开启任务的用户openid
     * @param taskId 任务id
     * @return DySimpleResult<UserShareTaskVo>
     */
    public DySimpleResult<UserShareTaskVo> queryUserShareTask(String openId, List<String> taskId) {
        return getShareTaskClient().queryUserShareTask(baseQuery(), openId, taskId);
    }

}
