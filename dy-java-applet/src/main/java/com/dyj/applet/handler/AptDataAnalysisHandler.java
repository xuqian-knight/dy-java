package com.dyj.applet.handler;

import com.dtflys.forest.annotation.Query;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

import java.util.List;

public class AptDataAnalysisHandler extends AbstractAppletHandler {
    public AptDataAnalysisHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    //****************用户分析******************************

    /**
     * 行为分析
     *
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param os          操作系统，ios或者android
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserBehaviorDataVo>
     */
    public DySimpleResult<AnalysisUserBehaviorDataVo> queryBehaviorData(Long startTime, Long endTime, String hostName, String os, String versionType) {
        return getDataAnalysisClient().queryBehaviorData(baseQuery(), startTime, endTime, hostName, os, versionType);
    }

    /**
     * 实时用户分析
     *
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserBehaviorDataVo>
     */
    public DySimpleResult<AnalysisUserBehaviorDataVo> queryRealTimeUserData(String hostName, String versionType) {
        return getDataAnalysisClient().queryRealTimeUserData(baseQuery(), hostName, versionType);
    }

    /**
     * 留存分析
     *
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param os          操作系统，ios或者android
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserRetentionDataVo>
     */
    public DySimpleResult<AnalysisUserRetentionDataVo> queryRetentionData(Long startTime, Long endTime, String hostName, String os, String versionType) {
        return getDataAnalysisClient().queryRetentionData(baseQuery(), startTime, endTime, hostName, os, versionType);
    }

    /**
     * 来源分析
     *
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return
     */
    public DySimpleResult<AnalysisUserSceneDataVo> querySceneData(Long startTime, Long endTime, String hostName, String versionType) {
        return getDataAnalysisClient().querySceneData(baseQuery(), startTime, endTime, hostName, versionType);
    }

    /**
     * 用户画像分析
     *
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param userType    用户类型，active_user-活跃用户；new_user-新增用户
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserPortraitDataVo>
     */
    public DySimpleResult<AnalysisUserPortraitDataVo> queryUserPortraitData(Long startTime, Long endTime, String userType, String hostName, String versionType) {
        return getDataAnalysisClient().queryUserPortraitData(baseQuery(), startTime, endTime, userType, hostName, versionType);
    }

    /**
     * 终端分析
     *
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param userType    用户类型，active_user-活跃用户；new_user-新增用户
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserClientDataVo>
     */
    public DySimpleResult<AnalysisUserClientDataVo> queryClientData(Long startTime, Long endTime, String userType, String hostName, String versionType) {
        return getDataAnalysisClient().queryClientData(baseQuery(), startTime, endTime, userType, hostName, versionType);
    }

    /**
     * 页面分析
     *
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param os          操作系统，ios或者android
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserPageDataVo>
     */
    public DySimpleResult<AnalysisUserPageDataVo> queryPageData(Long startTime, @Query("end_time") Long endTime, @Query("host_name") String hostName, @Query("os") String os, @Query("version_type") String versionType) {
        return getDataAnalysisClient().queryPageData(baseQuery(), startTime, endTime, hostName, os, versionType);
    }

    /**
     * 总览分析
     *
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return
     */
    public DySimpleResult<AnalysisDealOverviewDataVo> queryDealOverviewData(Long startTime, Long endTime, String hostName, String versionType) {
        return getDataAnalysisClient().queryDealOverviewData(baseQuery(), startTime, endTime, hostName, versionType);
    }

    /**
     * 流量转化
     *
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param scenes_list 流量来源渠道
     * @return DySimpleResult<AnalysisDealDataWithConversionVo>
     */
    public DySimpleResult<AnalysisDealDataWithConversionVo> queryDealDataWithConversion(Long startTime, Long endTime, String hostName, List<String> scenes_list) {
        return getDataAnalysisClient().queryDealDataWithConversion(baseQuery(), startTime, endTime, hostName, scenes_list);
    }

    /**
     * 短视频交易分析
     *
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime   结束时间，必须是昨天以前的某个时间戳
     * @param hostName  宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     * @return DySimpleResult<AnalysisVideoDealDataVo>
     */
    public DySimpleResult<AnalysisVideoDealDataVo> queryVideoDealData(Long startTime, Long endTime, String hostName) {
        return getDataAnalysisClient().queryVideoDealData(baseQuery(), startTime, endTime, hostName);
    }

    /**
     * 获取直播房间数据
     *
     * @param anchorName 主播昵称
     * @return DySimpleResult<AnalysisLiveRoomDataVo>
     */
    public DySimpleResult<LiveRoomDataVo> queryLiveRoomData(String anchorName) {
        return getDataAnalysisClient().queryLiveRoomData(baseQuery(), anchorName);
    }

    /**
     * 直播数据分析
     *
     * @param liveRoomId 直播间ID，可通过【获取直播房间数据】接口获取该字段通过 /api/platform/v2/data_analysis/query_live_room/
     *                   接口返回的 data.current_live_room.0.live_room_id 字段获取
     * @return
     */
    public DySimpleResult<AnalysisLiveRoomDataVo> queryLiveRoomData(Long liveRoomId) {
        return getDataAnalysisClient().queryLiveRoomData(baseQuery(), liveRoomId);
    }

    /**
     * 直播交易分析
     *
     * @param liveRoomId 直播间ID，可通过【获取直播房间数据】接口获取该字段通过 /api/platform/v2/data_analysis/query_live_room/
     *                   接口返回的 data.history_live_room.0.live_room_id 字段获取
     * @return
     */
    public DySimpleResult<AnalysisLiveDealDataVo> queryLiveDealData(Long liveRoomId) {
        return getDataAnalysisClient().queryLiveDealData(baseQuery(), liveRoomId);
    }

    /**
     * 商品分析
     *
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime   结束时间，必须是昨天以前的某个时间戳
     * @param pageNum   页码，默认为1
     * @param pageSize  每页数量，默认为20，最大为50
     * @param hostName  宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     * @return DySimpleResult<AnalysisProductDealDataVo>
     */
    public DySimpleResult<AnalysisProductDealDataVo> queryProductDealData(Long startTime, Long endTime, Long pageNum, Long pageSize, String hostName) {
        return getDataAnalysisClient().queryProductDealData(baseQuery(), startTime, endTime, pageNum, pageSize, hostName);
    }

    /**
     * 短视频投稿数据
     *
     * @param query 短视频投稿数据参数
     * @return DySimpleResult<ShortLiveIdWithAwemeIdVo>
     */
    public DySimpleResult<ShortLiveIdWithAwemeIdVo> queryShortIdWithAwemeId(ShortLiveIdWithAwemeIdQuery query) {
        baseQuery(query);
        return getDataAnalysisClient().queryShortLiveIdWithAwemeId(query);
    }


    /**
     * 短视频总览数据
     *
     * @param query 视频数据查询参数
     * @return DySimpleResult<AnalysisVideoDataVo>
     */
    public DySimpleResult<AnalysisVideoDataVo> queryVideoData(AnalysisVideoDataQuery query) {
        baseQuery(query);
        return getDataAnalysisClient().queryVideoData(query);
    }

    /**
     * 短视频详细数据
     *
     * @param query 视频数据查询参数
     * @return DySimpleResult<ShortLiveDataWithIdVo>
     */
    public DySimpleResult<ShortLiveDataWithIdVo> queryShortDataWithId(ShortLiveDataWithIdQuery query) {
        baseQuery(query);
        return getDataAnalysisClient().queryShortLiveDataWithId(query);
    }

    /**
     * 流量来源
     *
     * @param query 流量来源查询参数
     * @return DySimpleResult<AnalysisVideoSourceDataVo>
     */
    public DySimpleResult<AnalysisVideoSourceDataVo> queryVideoWithSource(AnalysisVideoSourceQuery query) {
        baseQuery(query);
        return getDataAnalysisClient().queryVideoWithSource(query);
    }


    /**
     * 直播投稿数据
     *
     * @param query 直播投稿数据参数
     * @return DySimpleResult<ShortLiveIdWithAwemeIdVo>
     */
    public DySimpleResult<ShortLiveIdWithAwemeIdVo> queryLiveWithAwemeId(ShortLiveIdWithAwemeIdQuery query) {
        baseQuery(query);
        return getDataAnalysisClient().queryShortLiveIdWithAwemeId(query);
    }


    /**
     * 直播间详细数据
     *
     * @param query 直播间详细数据查询参数
     * @return DySimpleResult<ShortLiveDataWithIdVo>
     */
    public DySimpleResult<ShortLiveDataWithIdVo> queryLiveDataWithId(ShortLiveDataWithIdQuery query) {
        baseQuery(query);
        return getDataAnalysisClient().queryShortLiveDataWithId(query);
    }

    /**
     * 主播分析
     *
     * @param query 主播分析参数
     * @return DySimpleResult<LiveWithShortIdVo>
     */
    public DySimpleResult<LiveWithShortIdVo> queryLiveWithShortId(LiveWithShortIdQuery query) {
        baseQuery(query);
        return getDataAnalysisClient().queryLiveWithShortId(query);
    }


    /**
     * 小房子直播间总览数据
     *
     * @param startTime 开始时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     * @param endTime   结束时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
     * @return DySimpleResult<SmallHomeOverviewDataVo>
     */
    public DySimpleResult<SmallHomeOverviewDataVo> querySmallHomeOverviewData(Long startTime, Long endTime) {
        return getDataAnalysisClient().querySmallHomeOverviewData(baseQuery(), startTime, endTime);
    }

    /**
     * 小房子直播间详细数据
     *
     * @param startTime 开始时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     * @param endTime   结束时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
     * @return DySimpleResult<SmallHomeRoomDataVo>
     */
    public DySimpleResult<SmallHomeRoomDataVo> querySmallHomeRoomData(Long startTime, Long endTime) {
        return getDataAnalysisClient().querySmallHomeRoomData(baseQuery(), startTime, endTime);
    }

    /**
     * 小房子直播间订单数据
     *
     * @param startTime 开始时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     * @param endTime   结束时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
     * @param pageNum   分页编号，从 0 开始
     * @param pageSize  分页大小，小于等于 1000，大于1000的默认取最大值1000
     * @return DySimpleResult<SmallHomeOrderDataVo>
     */
    public DySimpleResult<SmallHomeOrderDataVo> querySmallHomeOrderData(Long startTime, Long endTime, Integer pageNum, Integer pageSize) {
        return getDataAnalysisClient().querySmallHomeOrderData(baseQuery(), startTime, endTime, pageNum, pageSize);
    }


    /**
     * 组件使用数据
     *
     * @param startTime       结束时间，必须是昨天以前的某个时间戳
     * @param endTime         开始时间，必须是昨天以前的某个时间戳
     * @param componentIdList 组件配置id（配置的唯一标识）。可以通过查询已创建的线索组件接口获得
     * @return DySimpleResult<AnalysisComponentWithOverviewVo>
     */
    public DySimpleResult<AnalysisComponentWithOverviewVo> queryComponentWithOverview(Long startTime, Long endTime, List<String> componentIdList) {
        return getDataAnalysisClient().queryComponentWithOverview(baseQuery(), startTime, endTime, componentIdList);
    }

    /**
     * 流量来源
     *
     * @param startTime       开始时间，必须是昨天以前的某个时间戳
     * @param endTime         结束时间，必须是昨天以前的某个时间戳
     * @param componentIdList 组件配置id（配置的唯一标识）
     * @return DySimpleResult<AnalysisComponentWithSourceVo>
     */
    public DySimpleResult<AnalysisComponentWithSourceVo> queryComponentWithSource(Long startTime, Long endTime, List<String> componentIdList) {
        return getDataAnalysisClient().queryComponentWithSource(baseQuery(), startTime, endTime, componentIdList);
    }

    /**
     * 组件详细数据
     *
     * @param startTime       开始时间，必须是昨天以前的某个时间戳
     * @param endTime         结束时间，必须是昨天以前的某个时间戳
     * @param pageNo          页号
     * @param pageSize        页面大小，最大50
     * @param isQueryLive     是否查询直播维度
     * @param isQueryVideo    是否查询短视频维度
     * @param componentIdList 组件配置id（配置的唯一标识）
     * @return DySimpleResult<AnalysisComponentWithDetailVo>
     */
    public DySimpleResult<AnalysisComponentWithDetailVo> queryComponentWithDetail(Long startTime, Long endTime, Integer pageNo, Integer pageSize, Boolean isQueryLive, Boolean isQueryVideo, List<String> componentIdList) {
        return getDataAnalysisClient().queryComponentWithDetail(baseQuery(), startTime, endTime, pageNo, pageSize, isQueryLive, isQueryVideo, componentIdList);
    }

    /**
     * 组件使用对比
     *
     * @param startTime       开始时间，必须是昨天以前的某个时间戳
     * @param endTime         结束时间，必须是昨天以前的某个时间戳
     * @param pageNo          页号
     * @param pageSize        页面大小，最大50
     * @param componentIdList 组件配置id（配置的唯一标识）
     * @return DySimpleResult<AnalysisComponentWithDataVo>
     */
    public DySimpleResult<AnalysisComponentWithDataVo> queryComponentWithData(Long startTime, Long endTime, Integer pageNo, Integer pageSize, List<String> componentIdList) {
        return getDataAnalysisClient().queryComponentWithData(baseQuery(), startTime, endTime, pageNo, pageSize, componentIdList);
    }

}
