package com.dyj.applet.handler;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.ApplyCapacityQuery;
import com.dyj.applet.domain.query.BindAwemeRelaionQuery;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.AppV2TokenHeaderInterceptor;
import com.dyj.common.interceptor.TpV2AuthTokenHeaderInterceptor;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-26 18:28
 **/
public class AptCapacityBindHandler extends AbstractAppletHandler {
    public AptCapacityBindHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 获取抖音号绑定所需的资质模版列表
     *
     * @param awemeId      抖音号
     * @param type         绑定类型
     *                     brand：品牌号绑定
     *                     cooperation：合作号绑定
     *                     employee：员工号绑定
     * @param capacityList 绑定能力列表
     *                     video_self_mount：短视频自主挂载
     *                     live_self_mount：直播自主挂载
     *                     ma.im.life_im: 本地生活
     *                     imma.component.instant_messaging：私信组件
     *                     ma.jsapi.authorizePrivateMessage：主动授权私信组件
     * @return DySimpleResult<AwemeBindTemplateInfoVo>
     */
    public DySimpleResult<AwemeBindTemplateListVo> getAwemeBindTemplateList(String awemeId, String type, List<Long> capacityList) {
        return getCapacityBindClient().getAwemeBindTemplateList(baseQuery(), awemeId, type, capacityList);
    }

    /**
     * 获取抖音号绑定所需的资质模版信息
     *
     * @param awemeId    抖音号
     * @param type       绑定类型  brand：品牌号绑定  cooperation：合作号绑定  employee：员工号绑定
     * @param templateId 资质模版ID列表，通过获取抖音号绑定所需的资质模版列表接口获取
     * @return DySimpleResult<AwemeBindTemplateInfoVo>
     */
    public DySimpleResult<AwemeBindTemplateInfoVo> getAwemeBindTemplateInfo(String awemeId, String type, List<Long> templateId) {
        return getCapacityBindClient().getAwemeBindTemplateInfo(baseQuery(), awemeId, type, templateId);
    }

    /**
     * 输入抖音号绑定
     *
     * @param query 请求参数
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> bindAwemeRelation(BindAwemeRelaionQuery query) {
        baseQuery(query);
        return getCapacityBindClient().bindAwemeRelation(query);
    }

    /**
     * 获取抖音号绑定二维码
     *
     * @param capacityList 绑定能力列表
     *                     video_self_mount：短视频自主挂载
     *                     live_self_mount：直播自主挂载
     *                     ma.im.life_im: 本地生活im
     * @param type         绑定类型，当前仅品牌号支持获取绑定二维码   brand：品牌号绑定
     * @param coSubject    抖音号和小程序是否是相同的主体，品牌号绑定时必传
     * @return
     */
    public DySimpleResult<AwemeRelationBindQrcodeVo> getAwemeRelationBindQrcode(List<String> capacityList, String type, Boolean coSubject) {
        return getCapacityBindClient().getAwemeRelationBindQrcode(baseQuery(), capacityList, type, coSubject);
    }

    /**
     * 查询抖音号绑定列表及状态
     *
     * @param pageNum  分页编号，从1开始
     * @param pageSize 分页大小
     * @param type     绑定类型 brand：品牌号绑定 cooperation：合作号绑定 employee：员工号绑定
     * @return DySimpleResult<AwemeRelationListVo>
     */
    public DySimpleResult<AwemeRelationListVo> queryAwemeRelationList(Long pageNum, Long pageSize, String type) {
        return getCapacityBindClient().queryAwemeRelationList(baseQuery(), pageNum, pageSize, type);
    }

    /**
     * 解除抖音号绑定
     *
     * @param awemeId 抖音号
     * @param type    绑定类型brand：品牌号绑定cooperation：合作号绑定employee：员工号绑定
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> unbindAwemeRelation(BaseQuery query, String awemeId, String type) {
        return getCapacityBindClient().unbindAwemeRelation(query, awemeId, type);
    }


    /**
     * 查询页面结构自定义能力申请状态
     *
     * @param capacityKey 应用信息
     * @return DySimpleResult<String>
     */
    public DySimpleResult<CapacityApplyStatusVo> queryApplyStatus(String capacityKey){
        return getCapacityBindClient().queryApplyStatus(baseQuery(), capacityKey);
    }


    /**
     * 查询页面结构自定义能力申请状态(服务商代调用)
     *
     * @param capacityKey 应用信息
     * @return DySimpleResult<String>
     */
    public DySimpleResult<CapacityApplyStatusVo> queryTpApplyStatus(String capacityKey){
        return getCapacityBindClient().queryTpApplyStatus(baseQuery(), capacityKey);
    }

    /**
     * 申请页面结构自定义能力
     *
     * @param query 请求参数
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> applyCapacity(ApplyCapacityQuery query){
        baseQuery(query);
        return getCapacityBindClient().applyCapacity(query);
    }

    /**
     * 申请页面结构自定义能力(服务商代调用)
     *
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> tpApplyCapacity(ApplyCapacityQuery query){
        baseQuery(query);
        return getCapacityBindClient().tpApplyCapacity(query);
    }
}
