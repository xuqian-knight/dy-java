package com.dyj.applet.handler;

import com.dtflys.forest.annotation.Var;
import com.dyj.applet.domain.vo.ApplyCapacityListVo;
import com.dyj.applet.domain.vo.AuditCategoryListVo;
import com.dyj.applet.domain.vo.CapacityApplyStatusVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;
import java.util.Map;

/**
 * @author danmo
 * @date 2024-07-17 10:06
 **/
public class AptCapacityApplyHandler extends AbstractAppletHandler {

    public AptCapacityApplyHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 能力申请  接口频率限制：10 QPS
     *
     * @param capacityKey 能力标识
     * @param applyReason 申请理由
     * @param applyInfo   申请能力填写的字段
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> applyCapacity(String capacityKey, String applyReason, Map<String, List<String>> applyInfo) {
        return getCapacityApplyClient().applyCapacity(baseQuery(), capacityKey, applyReason, applyInfo);
    }

    /**
     * 能力申请状态查询 接口频率限制：10 QPS
     *
     * @param capacityKey 能力key，能力唯一标识
     * @return DySimpleResult<ApplyCapacityStatusVo>
     */
    public DySimpleResult<CapacityApplyStatusVo> queryApplyStatus(String capacityKey) {
        return getCapacityApplyClient().queryApplyStatus(baseQuery(), capacityKey);
    }

    /**
     * 能力列表查询 接口频率限制：10 QPS
     *
     * @return DySimpleResult<ApplyCapacityListVo>
     */
    public DySimpleResult<ApplyCapacityListVo> queryCapacityList() {
        return getCapacityApplyClient().queryCapacityList(baseQuery());
    }

    /**
     * 获取已设置的服务类目
     * @return  DySimpleResult<AuditCategoryListVo>
     */
    public DySimpleResult<AuditCategoryListVo> getAuditCategories(){
        return getCapacityApplyClient().getAuditCategories(baseQuery());
    }


}
